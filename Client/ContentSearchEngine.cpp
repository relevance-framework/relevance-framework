/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "ContentSearchEngine.h"
#ifdef QT_DBUS_LIB
#include "../Daemon/RelevanceDaemonIf.h"
typedef QVector<QVariantMap> SearchResult_t;
#else
#include "ClientSocket.h"
typedef SocketReply * SearchResult_t;
#endif
#include "RelevanceItemModel.h"

/**
 * Private data class for ContentSearchEngine.
 *
 * This class contains private data for the ContentSearchEngine class.
 **/
class ContentSearchEnginePriv {

public:

    ContentSearchEnginePriv() : 
#ifdef QT_DBUS_LIB
        m_interface("com.nokia.RelevanceDaemon", "/", QDBusConnection::sessionBus()) 
#else
        m_interface (ClientSocket::getInstance ())
#endif
    {
        // do nothing
    }
    
    ~ContentSearchEnginePriv()
    {
        // do nothing
    }
#ifdef QT_DBUS_LIB
    RelevanceDaemonIf m_interface;
#else
    ClientSocket &m_interface;
#endif
};

ContentSearchEngine::ContentSearchEngine (QObject *parent) : QObject(parent), m_dp(new ContentSearchEnginePriv) {
#ifdef QT_DBUS_LIB
    // Register Types
    qRegisterMetaType<QVector<QVariantMap> >();
    qDBusRegisterMetaType<QVector<QVariantMap> >();
#endif
}

ContentSearchEngine::~ContentSearchEngine () {
    delete m_dp;
    m_dp = 0;
}

QAbstractItemModel * ContentSearchEngine::doSearch (const QString& text, const QStringList& content_types, const QStringList& content_sources) {
    SearchResult_t results = m_dp->m_interface.doSearch (text, content_types, content_sources, true);
    return new RelevanceItemModel (results); 
}

QAbstractItemModel * ContentSearchEngine::doSearchWithoutGrouping (const QString& text, const QStringList& content_types, const QStringList& content_sources) {
    SearchResult_t results = m_dp->m_interface.doSearch (text, content_types, content_sources, false);

    return new RelevanceItemModel (results) ;
}

QAbstractItemModel * ContentSearchEngine::doSearchGroup (const QString& groupIdentifier, const QString& text, const QStringList& content_types, 
                                                         const QStringList& content_sources) {
    SearchResult_t results = m_dp->m_interface.doSearch (groupIdentifier, text, content_types, content_sources, true);

    return new RelevanceItemModel (results) ;
}

QAbstractItemModel * ContentSearchEngine::doSearchGroupWithoutGrouping (const QString& groupIdentifier, const QString& text, const QStringList& content_types,
                                                                       const QStringList& content_sources) {
    SearchResult_t results = m_dp->m_interface.doSearch (groupIdentifier, text, content_types, content_sources, false);

    return new RelevanceItemModel (results);
}

QAbstractItemModel * ContentSearchEngine::doSearchItems (const QStringList & uris, const QStringList& content_types,
                                                         const QStringList& content_sources) {
    SearchResult_t results = m_dp->m_interface.doSearchItems (uris, content_types, content_sources);
 
    return new RelevanceItemModel (results) ;
}
