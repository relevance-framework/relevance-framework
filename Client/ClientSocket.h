/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _CLIENT_SOCKET_H
#define _CLIENT_SOCKET_H

#include <QLocalSocket>

#include <QString>
#include <QVariant>
#include <QVector>

Q_DECLARE_METATYPE(QVector<QVariantMap>)
class ClientSocket ;

class SocketReply : public QObject
{
    Q_OBJECT
public:
    SocketReply(quint16 id, QObject *parent);
    ~SocketReply();

    const QVariant & results() ;
    void  setResults (const QVariant &r) ; 
    bool isValid() ;

signals:
    void ready();

private:
    quint16 m_id;
    QVariant m_results;
};
                    

class ClientSocket : public QObject 
{
    Q_OBJECT 
private:
    ClientSocket (const QString &address, QObject *parent = 0);
    void request(const QString &request, const QList<QVariant> &arguments);
    QVector<QStringList> requestActions (const QString &request, const QList<QVariant> &arguments);

public slots:
    void readReady();
    void onSocketError(QLocalSocket::LocalSocketError err);

public:
    static ClientSocket & getInstance () { 
        static ClientSocket socket ("com.nokia.RelevanceDaemon");
        return socket ;
    }

    SocketReply * doSearch(const QString &text, const QStringList &content_types, const QStringList &content_sources, bool grouping_requried);
    SocketReply * doSearch(const QString &group_uri, const QString &text, const QStringList &content_types, const QStringList &content_sources, bool grouping_required);
    SocketReply * doSearchItems(const QStringList &item_uris, const QStringList &content_types, const QStringList &content_sources);
    
    QVector<QStringList> actionsForItem(const QString &identifier);
    QVector<QStringList> actionsForItems(const QStringList &identifiers);
    QVector<QStringList> actionsForMime(const QString &mimetype);
    
    bool executeItem(const QString &identifier, const QString &action);
    bool executeItems(const QStringList &identifiers, const QString &action);

    QVariantList activityCount(const QString &subjectUri);
    QVariantList activityCounts();
    void addActivity(const QString &subjectUri);
    bool addActivitySynchronously(const QString &subjectUri);
    
    ~ClientSocket ();
private:
    QLocalSocket *m_socket ;
    quint32 m_blockSize ;
    QList<uint> *m_requestIds ;
    QHash<quint16, SocketReply *> m_requests;
    bool m_blocked ; // true for blokcing APIs 
};

#endif // _CLIENT_SOCKET_H
