/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CONTENT_ACTIVITY_ENGINE_H
#define CONTENT_ACTIVITY_ENGINE_H

#include <QObject>
#include <QVariantList>
#include <QString>

class ContentActivityEnginePrivate;

/**
 * @brief API for tracking content activity.
 * @ingroup RelevanceAPI
 *
 * This class is used to reporting and querying activity related to content items.
 * The reported information is mainly used by the relevance calculation algorithm,
 * which stores the results in tracker in the maemo:relevance property.
 **/
class ContentActivityEngine : public QObject {

    Q_OBJECT

public:

    /**
     * Creates the content activity engine with the given @p parent.
     *
     * @param parent optional parent object.
     **/
    explicit ContentActivityEngine (QObject *parent = 0);

    /**
     * @brief Destructor
     **/
    ~ContentActivityEngine();

    /**
     * @brief Count how many times an uri has been accessed.
     *
     * The access count decays over time in order to give more weight to recent actions,
     * so the returned number is not a straight access frequency.
     *
     * @param subjectUri the uri for which access should be counted
     * @return a QVariantList containing the following: { uri, count, "Total", totalcount }
     * where uri and "Total" are the type QString and count and totalCount are of type double.
     **/
    QVariantList activityCount( const QString &subjectUri );
    
    /**
     * @brief Count how many times each uri in the database has been accessed.
     *
     * The access count decays over time in order to give more weight to recent actions,
     * so, the returned number is not a streight access frequency.
     *
     * @return A QVariantList containing the following: { uri1, count1, ..., uriN, countN, "Total", totalCount }
     * where uriX and "Total" are of type QString and countX and totalCount are of type double.
     **/
    QVariantList activityCounts(  );

    /**
     * @brief Report a user activity that access a content item.
     *
     * If the item is stored in tracker, the reported subjectUri should be the same as the
     * tracker URI. The method is asynchronous, so the control will return to the client
     * immediately. There is no possibility to detect failures. 
     * Please note that if the back end server is not currently running, and the client
     * application exists immediately after invoking this method, the invocation will silently
     * fail due to the way DBus operates. In this case, you must use the synchronous method
     * instead to ensure that the activity is stored to the database.
     *
     * @param subjectUri uri of the item that the user accessed (may not be empty)
     **/
    void addActivity( const QString &subjectUri );

    /**
     * @brief Report a user activity that access a content item.
     *
     * If the item is stored in tracker, the reported subjectUri should be the same as the
     * tracker URI. The method is asynchronous, so the control will return to the client
     * immediately. There is no possibility to detect failures. 
     * Please note that if the back end server is not currently running, and the client
     * application exists immediately after invoking this method, the invocation will silently
     * fail due to the way DBus operates. In this case, you must use the synchronous method
     * instead to ensure that the activity is stored to the database.
     *
     * @param subjectUri uri of the item that the user accessed (may not be empty)
     * @param searchQuery current text in search box
     * @param contentTypes list of allowed content types in search
     * @param actionName invoked action
     **/
    void addSearchActivity( const QString &subjectUri, const QString &searchQuery, const QStringList &contentTypes, const QString &actionName );

    /**
     * @brief Synchronously report a user activity that accessed a content item.
     *
     * If the item is stored in tracker, the reported subjectUri should be the same as the
     * tracker URI. The method is synchronous, so the control will return to the client
     * only after the data has been written to disk. This method should be used only if
     * it is critical that the report is recorded, or if the client application is going
     * to exit immediately after the call.
     *
     * @param subjectUri uri of the item that the user accessed (may not be empty)
     **/
    bool addActivitySynchronously( const QString &subjectUri );

Q_SIGNALS:
    /**
     * @brief A signal that is emitted whenever a new activity has been reported.
     *
     * @param subjectUri subject of the activity
     **/
    void activityAdded(const QString &subjectUri);

private:

    //! private implementation and data
    ContentActivityEnginePrivate* m_dptr;
    
    Q_DISABLE_COPY(ContentActivityEngine)
};

#endif // CONTENT_ACTIVITY_ENGINE_H
