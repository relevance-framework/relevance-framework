/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CONTENT_SEARCH_ENGINE_H
#define CONTENT_SEARCH_ENGINE_H

#include <QObject>
#include <QStringList>
#include <QDateTime>

class ContentSearchEnginePriv;
class QAbstractItemModel;

/**
 * @brief API for performing searches.
 * @ingroup RelevanceAPI
 *
 * This class is used to perform searches using all the known data providers and by taking given
 * search criteria into account. Searches can also be done without any criteria and then the most
 * relevant data is returned.
 **/
class ContentSearchEngine : public QObject {

    Q_OBJECT

public:

    /**
     * Creates the content search engine with the given @p parent. Does not perform any searches yet.
     *
     * @param parent optional parent object.
     **/
    explicit ContentSearchEngine (QObject *parent = 0);

    /**
     * Destructor.
     **/
    ~ContentSearchEngine ();

    /**
     * All roles return a QString.
     * About images:
     *
     * @li file URL to a file in file system, e.g. "/home/user/.thumbnails/aaa.png"
     * @li theme icon to be fetched from the theme system, e.g. "[icon_id]"
     * @li plain text to be rendered as image, e.g. "12:30"
     * @li empty string "" means no image for that kind
     **/
    enum ResultItemDataRole {
        DefaultLabel = Qt::DisplayRole, //<! the main text label for the item
        Identifier = Qt::UserRole,      //<! the uri/url of the item
        IsGroup,                        //<! bool value, is item a group
        ContentType,                    //<! uri of a content type for the item
        ContentSource,                  //<! the harvester name
        MimeType,                       //<! a mime type for the item
        Details,                        //<! an optional mapping of item details
        RelevanceValue                  //<! a global relevance value for the item
    };

    /**
     * Performs a search query
     *
     * This method supports returning groups.
     *
     * @param text a search string that is used to perform an initial search.
     * @param content_types a list of fully qualified or short content type uris that narrow down the available content.
     * @param content_sources a list of fully qualified or short content source uris that narrow down the available content.
     *
     * @return a newly allocated model whose ownership is moved to the caller or 0 on an critical error.
     **/
    QAbstractItemModel* doSearch (const QString& text="", const QStringList& content_types=QStringList(),
                                  const QStringList& content_sources=QStringList());

    /**
     * Performs a search query
     *
     * This method does not return groups.
     *
     * @param text a search string that is used to perform an initial search.
     * @param content_types a list of fully qualified or short content type uris that narrow down the available content.
     * @param content_sources a list of fully qualified or short content source uris that narrow down the available content.
     *
     * @return a newly allocated model whose ownership is moved to the caller or 0 on an critical error.
     **/
    QAbstractItemModel* doSearchWithoutGrouping (const QString& text="", const QStringList& content_types=QStringList(),
                                                 const QStringList& content_sources=QStringList());

    /**
     * Performs a group browse query
     *
     * This method supports returning groups.
     *
     * @param groupIdentifier a group identifier.
     * @param text a search string that is used to perform an initial search.
     * @param content_types a list of fully qualified or short content type uris that narrow down the available content.
     * @param content_sources a list of fully qualified or short content source uris that narrow down the available content.
     *
     * @return a newly allocated model whose ownership is moved to the caller or 0 on an critical error.
     **/
    QAbstractItemModel* doSearchGroup (const QString& groupIdentifier, const QString& text="", const QStringList& content_types=QStringList(),
                                       const QStringList& content_sources=QStringList());

    /**
     * Performs a group browser query
     *
     * This method does not return groups.
     *
     * @param groupIdentifier a group identifier.
     * @param text a search string that is used to perform an initial search.
     * @param content_types a list of fully qualified or short content type uris that narrow down the available content.
     * @param content_sources a list of fully qualified or short content source uris that narrow down the available content.
     *
     * @return a newly allocated model whose ownership is moved to the caller or 0 on an critical error.
     **/
    QAbstractItemModel* doSearchGroupWithoutGrouping (const QString& groupIdentifier, const QString& text="", const QStringList& content_types=QStringList(),
                                                      const QStringList& content_sources=QStringList());

    /**
     * Performs a search query
     *
     * @param identifiers a list of URIs to be searched.
     * @param content_types a list of fully qualified or short content type uris that narrow down the available content.
     * @param content_sources a list of fully qualified or short content source uris that narrow down the available content.
     *
     * @return a newly allocated model whose ownership is moved to the caller or 0 on an critical error.
     **/
    QAbstractItemModel* doSearchItems (const QStringList& identifiers, const QStringList& content_types=QStringList(),
                                       const QStringList& content_sources=QStringList());

private:

    //! private implementation and data
    ContentSearchEnginePriv * m_dp;
};

#endif // CONTENT_SEARCH_ENGINE_H
