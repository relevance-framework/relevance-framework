/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "RelevanceItemModel.h"

#include "ContentSearchEngine.h" // for Roles
#ifdef QT_DBUS_LIB
#include <QTimer>
#else
#include "ClientSocket.h"
#include <QMetaType>
#endif

#include <QByteArray>

#ifdef QT_DBUS_LIB
RelevanceItemModel::RelevanceItemModel (const QVector<QVariantMap> &results, QObject *parent) : QAbstractItemModel(parent), m_results (results)
#else
RelevanceItemModel::RelevanceItemModel (SocketReply *results, QObject *parent) : QAbstractItemModel(parent)
#endif
{
    QHash<int, QByteArray> role_names;
    role_names.insert (ContentSearchEngine::DefaultLabel,            "defaultLabel");
    role_names.insert (ContentSearchEngine::Identifier,              "identifier");
    role_names.insert (ContentSearchEngine::IsGroup,                 "isGroup");
    role_names.insert (ContentSearchEngine::ContentType,             "contentType");
    role_names.insert (ContentSearchEngine::ContentSource,           "contentSource");
    role_names.insert (ContentSearchEngine::MimeType,                "mimeType");
    role_names.insert (ContentSearchEngine::Details,                 "details");
    role_names.insert (ContentSearchEngine::RelevanceValue,          "relevanceValue");
    setRoleNames (role_names);
#ifdef QT_DBUS_LIB
    /* Just for Backword Comapatibility emiting the dataChanged() signal 
     * in gotReply() handler, so that the clients know about the data 
     */
    QTimer::singleShot(0, this, SLOT(gotReply()));
#else
    qRegisterMetaType<QVector<QVariantMap> >();
    if(results) connect (results, SIGNAL(ready()), this, SLOT(gotReply()));
#endif
}

RelevanceItemModel::~RelevanceItemModel ()
{
    // do nothing
}

int RelevanceItemModel::columnCount (const QModelIndex & parent) const 
{
    Q_UNUSED(parent);

    // always 1
    return 1;
}

QVariant RelevanceItemModel::fetchRole (const QVariantMap &row_data, const int &role) const
{
    switch (role) {
    case ContentSearchEngine::DefaultLabel :
        return row_data.value ("defaultLabel");
        
    case ContentSearchEngine::Identifier:
        return row_data.value ("uri");

    case ContentSearchEngine::IsGroup:
        return row_data.value("isGroup");
 
    case ContentSearchEngine::ContentType:
        return row_data.value("rdfs:type");
        
    case ContentSearchEngine::ContentSource:
        return row_data.value("defaultHarvester");
        
    case ContentSearchEngine::MimeType:
        return row_data.value("mimeType");
        
    case ContentSearchEngine::Details:
        return row_data ;
        
    case ContentSearchEngine::RelevanceValue:
        return row_data.value("relevanceValue");
    }

    return QVariant();
}

QVariant RelevanceItemModel::data (const QModelIndex &index, int role) const
{
    if (!index.isValid()) return QVariant() ;

    return fetchRole (m_results.at (index.row()), role);
}

QModelIndex RelevanceItemModel::index(int row, int col, const QModelIndex &parent) const
{
    Q_UNUSED (parent);

    return createIndex (row, col); 
}

QModelIndex RelevanceItemModel::parent(const QModelIndex &index) const
{
    Q_UNUSED (index);

    return QModelIndex();
}

int RelevanceItemModel::rowCount(const QModelIndex & parent) const
{
    Q_UNUSED (parent);

    return m_results.size();
}

void RelevanceItemModel::gotReply ()
{
#ifndef QT_DBUS_LIB
    SocketReply *reply = qobject_cast<SocketReply *>(sender());

    m_results = reply->results().value<QVector<QVariantMap> >();

#endif

    emit dataChanged (createIndex(0, 0), createIndex( m_results.size() , 1)) ;
}
