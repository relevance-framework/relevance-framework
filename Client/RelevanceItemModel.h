/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _RELEVANCE_ITEM_MODEL_H_
#define _RELEVANCE_ITEM_MODEL_H_

#include <QVector>
#include <QStringList>
#include <QAbstractItemModel>

#ifndef QT_DBUS_LIB
class SocketReply;
//Q_DECLARE_METATYPE(QVector<QVariantMap>)
#endif

class RelevanceItemModel : public QAbstractItemModel
{
    Q_OBJECT 

private :
    QVector<QVariantMap> m_results;

    QVariant fetchRole (const QVariantMap &row_data, const int &row) const ;

public :
#ifdef QT_DBUS_LIB
	RelevanceItemModel (const QVector<QVariantMap> &results = QVector<QVariantMap>(), QObject *parent = 0) ;
#else
	RelevanceItemModel (SocketReply *results = 0, QObject *parent = 0) ;
#endif

    ~RelevanceItemModel () ;

    virtual QVariant data ( const QModelIndex & item, int role = Qt::DisplayRole ) const;
    virtual QModelIndex index(int, int, const QModelIndex&) const ;
    virtual QModelIndex parent(const QModelIndex & parent = QModelIndex()) const;
    virtual int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual int columnCount (const QModelIndex & parent = QModelIndex()) const;

public slots :
    void gotReply();
};

#endif // _RELEVANCE_ITEM_MODEL_H_
