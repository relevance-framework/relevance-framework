/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "ContentActivityEngine.h"
#ifdef QT_DBUS_LIB
#include "../Daemon/RelevanceDaemonIf.h"
#else
#include "ClientSocket.h"
#endif

class ContentActivityEnginePrivate {

public:

    ContentActivityEnginePrivate() :
#ifdef QT_DBUS_LIB
        m_interface("com.nokia.RelevanceDaemon", "/", QDBusConnection::sessionBus()) 
#else
        m_interface(ClientSocket::getInstance())
#endif
    {
        // do nothing
    }
#ifdef QT_DBUS_LIB
    RelevanceDaemonIf m_interface;
#else
    ClientSocket &m_interface;
#endif
};

ContentActivityEngine::ContentActivityEngine (QObject *parent) : QObject(parent), m_dptr(new ContentActivityEnginePrivate()) {
    //    connect(&(m_dptr->m_interface), SIGNAL(activityAdded(const QString &)), this, SIGNAL(activityAdded(const QString &)));
}

ContentActivityEngine::~ContentActivityEngine() {
    delete m_dptr;
}

QVariantList ContentActivityEngine::activityCount( const QString &subjectUri ) {
    return m_dptr->m_interface.activityCount(subjectUri);
}

QVariantList ContentActivityEngine::activityCounts(  ) {
    return m_dptr->m_interface.activityCounts();
}

void ContentActivityEngine::addActivity( const QString &subjectUri ) {
    m_dptr->m_interface.addActivity(subjectUri);
}

void ContentActivityEngine::addSearchActivity( const QString &subjectUri, const QString &searchQuery, const QStringList &contentTypes, const QString &actionName ) {
    Q_UNUSED(searchQuery);
    Q_UNUSED(contentTypes);
    Q_UNUSED(actionName);

    addActivity(subjectUri);
}

bool ContentActivityEngine::addActivitySynchronously( const QString &subjectUri ) {
    return m_dptr->m_interface.addActivitySynchronously(subjectUri);
}
