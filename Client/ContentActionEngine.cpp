/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <QStandardItemModel>
#include <QStandardItem>

#include "ContentActionEngine.h"
#ifdef QT_DBUS_LIB
#include "../Daemon/RelevanceDaemonIf.h"
#else
#include "ClientSocket.h"
#endif

/**
 * Private data class for ContentActionEngine.
 *
 * This class contains private data for the ContentActionEngine class.
 **/
class ContentActionEnginePrivate {

public:

    ContentActionEnginePrivate() : 
#ifdef QT_DBUS_LIB
        m_daemonIf ("com.nokia.RelevanceDaemon", "/", QDBusConnection::sessionBus()) 
#else
        m_daemonIf(ClientSocket::getInstance())
#endif
    {
        // do nothing
    }

    QAbstractItemModel* makeModel(QVector<QStringList> actions) {
        QStandardItemModel* model = new QStandardItemModel();

        for(int i=0;i<actions.size();i++) {
            QString identifier = actions.at(i).at(0);
            QString name = actions.at(i).at(1);
            QString localizedName = actions.at(i).at(2);
            QStandardItem* item = new QStandardItem();
            
            item->setData(identifier, ContentActionEngine::Identifier);
            item->setData(name, ContentActionEngine::Name);
            item->setData(localizedName, ContentActionEngine::LocalizedName);
           
            model->appendRow(item);
        }

        return model;
    }
#ifdef QT_DBUS_LIB
    RelevanceDaemonIf m_daemonIf ;
#else
    ClientSocket &m_daemonIf;
#endif
};

ContentActionEngine::ContentActionEngine (QObject *parent) : QObject(parent), m_dp(new ContentActionEnginePrivate) {
    // do nothing
}

ContentActionEngine::~ContentActionEngine () {
    delete m_dp;
    m_dp = 0;
}

QAbstractItemModel* ContentActionEngine::actionsForItem(const QString& identifier) {
    return m_dp->makeModel (m_dp->m_daemonIf.actionsForItem (identifier));
}

QAbstractItemModel* ContentActionEngine::actionsForItems(const QStringList& identifiers) {
    return m_dp->makeModel (m_dp->m_daemonIf.actionsForItems (identifiers));
}

QAbstractItemModel* ContentActionEngine::actionsForMimeType(const QString& mimetype) {
    return m_dp->makeModel (m_dp->m_daemonIf.actionsForMime (mimetype));
}

bool ContentActionEngine::executeItem( const QString& itemIdentifier, const QString& actionIdentifier ) {
    if (itemIdentifier.isEmpty()) return false;

    return m_dp->m_daemonIf.executeItem (itemIdentifier, actionIdentifier);
}

bool ContentActionEngine::executeItems(const QStringList &itemIdentifiers, const QString& actionIdentifier) {
    if (itemIdentifiers.size() == 0) return false;

    return m_dp->m_daemonIf.executeItems (itemIdentifiers, actionIdentifier);
}
