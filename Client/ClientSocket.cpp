/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "ClientSocket.h"

#include <QLocalSocket>
#include <QDataStream>
#include <QStringList>
#include <QDateTime>
#include <QDebug>

//
//GLocal Variables
//
static int g_socketTimeout = 20000 ; //2 seconds


SocketReply::SocketReply(quint16 id, QObject *parent): QObject(parent), m_id(id) 
{ 
}

SocketReply::~SocketReply() 
{
    //do nothing
}
const QVariant & SocketReply::results() { 
    return m_results ; 
}

void  SocketReply::setResults (const QVariant &r) { 
    m_results = r ;

    emit ready();
}

bool SocketReply::isValid() 
{ 
    return !m_results.isNull(); 
}


ClientSocket::ClientSocket (const QString &address, QObject *parent) 
    : QObject(parent), m_socket (new QLocalSocket(this)), m_blockSize((quint32)0),
      m_requestIds(new QList<unsigned int>)
{
    m_socket->connectToServer(address);

    connect(m_socket, SIGNAL(readyRead()), this, SLOT(readReady()));
    connect(m_socket, SIGNAL(error(QLocalSocket::LocalSocketError)), this, SLOT(onSocketError(QLocalSocket::LocalSocketError)));

    qRegisterMetaType<QVector<QVariantMap> >();
    qRegisterMetaTypeStreamOperators<QVector<QVariantMap> >("QVector<QVariantMap>");
}

ClientSocket::~ClientSocket()
{
    delete m_socket;
    delete m_requestIds;
}

//
//Private Members
//

void ClientSocket::request(const QString &request, const QList<QVariant> &arguments)
{
    QByteArray data;
    QDataStream out(&data, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);
    quint32 blockSize = (quint32)0;
    quint16 requestId = QDateTime::currentDateTime().toTime_t();

    out << blockSize ;
    out << request ;
    out << requestId;

    QList<QVariant>::const_iterator i;
    for (i = arguments.begin(); i != arguments.end(); ++i)
        out << *i;

    blockSize = (quint32) data.size() - sizeof(blockSize);
    out.device()->seek(0);
    out << blockSize ;

    m_blocked = true;
    if (m_socket->write(data) == -1){
        qCritical () << "Client Socket : Write data failure : " << m_socket->errorString();
    }
    m_socket->flush();
    bool state = m_socket->waitForReadyRead(g_socketTimeout);
    // if any reply from server, just ignore
    if (state) {
        m_socket->readAll();
    }
    m_blocked = false;
}

QVector<QStringList> ClientSocket::requestActions(const QString &request, const QList<QVariant> &arguments)
{
    QByteArray data;
    QDataStream out(&data, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);
    quint32 blockSize = (quint32)0;
    QVector<QStringList> reply ;
    quint16 requestId = QDateTime::currentDateTime().toTime_t();

    out << blockSize ;
    out << request ;
    out << requestId ;

    QList<QVariant>::const_iterator i;
    for (i = arguments.begin(); i != arguments.end(); ++i)
        out << *i;

    blockSize = (quint32) data.size() - sizeof(blockSize);
    out.device()->seek(0);
    out << blockSize ;

    if (m_socket->write(data) == -1){
        qCritical () << "Client Socket : Write data failure : " << m_socket->errorString();
        return reply;
    }
    m_socket->flush();

    blockSize = 0;
    do {
        bool state = false;
        QDataStream in(m_socket);
        in.setVersion(QDataStream::Qt_4_0);

        m_blocked = true;
        state = m_socket->waitForReadyRead(g_socketTimeout);
        m_blocked = false;

        // If timeout
        if (!state) return reply ;

        if (!blockSize) {
            // Error case
            if ((quint32)m_socket->bytesAvailable() < sizeof(blockSize))
                return reply;

            in >> blockSize;
        }
        if ((quint32)m_socket->bytesAvailable() >= blockSize) {
            in >> requestId ;
            in >> reply ;

            blockSize = 0;
        }
    } while (blockSize != 0);

    // flush if any leftovers on socket
    m_socket->readAll();

    return reply;
}

//
//Public Methods
//
SocketReply * ClientSocket::doSearch(const QString &text, const QStringList &content_types, const QStringList &content_sources, bool grouping_requried)
{
    QByteArray data;
    QDataStream out(&data, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);
    quint32 blockSize = (quint32)0;

    quint16 requestId = QDateTime::currentDateTime().toTime_t();

    out << blockSize;
    out << QString("doSearch") ;
    out << requestId ;

    out << text ;
    out << content_types ;
    out << content_sources ;
    out << grouping_requried ;

    blockSize = (quint32) data.size() - sizeof(blockSize);
    out.device()->seek(0);
    out << blockSize ;

    if (m_socket->write(data) == -1){
        qCritical () << "Client Socket : Write data failure : " << m_socket->errorString();
        return 0;
    }
    m_socket->flush();
 
    SocketReply *reply = new SocketReply(requestId, this);

    m_requests.insert (requestId, reply);

    return reply;
}

SocketReply *ClientSocket::doSearch(const QString &groupUri, const QString &text, 
                                            const QStringList &content_types, const QStringList &content_sources, bool grouping_flag)
{
    QByteArray data;
    QDataStream out(&data, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);
    quint32 blockSize = (quint32)0;
    quint16 requestId = QDateTime::currentDateTime().toTime_t();

    qCritical () << "ClientSocket::doSearchGroup : " << groupUri ;

    out << blockSize;
    out << QString("doSearchGroup") ;
    out << requestId ;
    
    out << groupUri ;
    out << text ;
    out << content_types ;
    out << content_sources ;
    out << grouping_flag;

    blockSize = (quint32) data.size() - sizeof(blockSize);
    out.device()->seek(0);
    out << blockSize ;

    if (m_socket->write(data) == -1){
        qCritical () << "Client Socket : Write data failure : " << m_socket->errorString();
        return 0;
    }
    m_socket->flush();
 
    SocketReply *reply = new SocketReply(requestId, this);

    m_requests.insert (requestId, reply);

    return reply ;
}


SocketReply * ClientSocket::doSearchItems(const QStringList &uris, const QStringList &content_types, const QStringList &content_sources)
{
    QByteArray data;
    QDataStream out(&data, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);
    quint32 blockSize = (quint32)0;
    quint16 requestId = QDateTime::currentDateTime().toTime_t();
    
    qCritical () << "ClientSocket::doSearchItems : " << uris;

    out << blockSize;
    out << QString("doSearchItems") ;
    out << requestId ;
    
    out << uris ;
    out << content_types ;
    out << content_sources ;

    blockSize = (quint32) data.size() - sizeof(blockSize);
    out.device()->seek(0);
    out << blockSize ;

    if (m_socket->write(data) == -1){
        qCritical () << "Client Socket : Write data failure : " << m_socket->errorString();
        return 0;
    }
    m_socket->flush();

    SocketReply *reply = new SocketReply(requestId, this);

    m_requests.insert (requestId, reply);

    return reply ;
}

QVector<QStringList> ClientSocket::actionsForItem(const QString &identifier)
{
    return requestActions ("actionsForItem", QList<QVariant>() << qVariantFromValue(identifier));
}

QVector<QStringList> ClientSocket::actionsForItems(const QStringList &uris)
{
    return requestActions ("actionsForItems", QList<QVariant>() << qVariantFromValue(uris));
}

QVector<QStringList> ClientSocket::actionsForMime(const QString &mimeType)
{
    return requestActions ("atcionsForMime", QList<QVariant>() << qVariantFromValue(mimeType)) ;
}

bool ClientSocket::executeItem(const QString &uri, const QString &action)
{
    QList<QVariant> arguments ;

    arguments << qVariantFromValue(uri) << qVariantFromValue(action) ;
    request ("executeItem", arguments);
    
    return true;
}

bool ClientSocket::executeItems(const QStringList &uris, const QString &action)
{
    QList<QVariant> arguments ;

    arguments << qVariantFromValue(uris) << qVariantFromValue(action) ;
    request ("executeItems", arguments);

    return true;
}

QVariantList ClientSocket::activityCount(const QString &)
{
    return QVariantList() ;
}

QVariantList ClientSocket::activityCounts()
{
    return QVariantList();
}

void ClientSocket::addActivity(const QString &)
{
}

bool ClientSocket::addActivitySynchronously(const QString &)
{
    return TRUE;
}


//
// Slots
//
void ClientSocket::readReady()
{
    QString replyFor ;
    QDataStream in(m_socket);
    in.setVersion(QDataStream::Qt_4_0);

    qDebug() << "ClientSocket::readRead() : bytesAvailable :" << m_socket->bytesAvailable();

    // Dont handle blocked requests,
    if (m_blocked == true) return ;
    
    if (m_blockSize == 0) {
        // Safe check
        if ((quint32)m_socket->bytesAvailable() < sizeof(m_blockSize)) {
            qCritical () << "Currpted reply ";
            return;
        }

        // Read the size of the reply
        in >> m_blockSize ;
    }

    // If the reply is not completely available, do nothing
    // waiting till whole reply available
    if ((quint32)m_socket->bytesAvailable() < m_blockSize) {
        return ;
    }
    
    // Reset blocksize for next request
    m_blockSize = 0 ;

    quint16 requestId ;
    in >> requestId;

    SocketReply *info = m_requests.value (requestId, 0);

    QVariant results ;
    in >> results ;

    if (info) {
        // save results
        info->setResults(results);

        // remove from request table
        m_requests.remove (requestId);
        
        delete info ;
    }
    else 
        // Something wrong
        qCritical () << " Request ID not Found :" << requestId ;

    // flush leftover
    m_socket->readAll();
}


void ClientSocket::onSocketError(QLocalSocket::LocalSocketError err)
{
    qCritical () << "ServerSocket::onSocketError :" << err ;

    switch(err)
    {
        case QLocalSocket::ServerNotFoundError:
            qCritical () << "QLocalSocket::ServerNotFoundError" ;
            break;
        case QLocalSocket::ConnectionRefusedError:
            qCritical () << "QLocalSocket::ConnectionRefusedError" ;
            break;
        case QLocalSocket::PeerClosedError:
            qCritical () << "QLocalSocket::PeerClosedError" ;
            break;
        default:
            break;
    }
}

