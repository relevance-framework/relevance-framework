/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <QTextStream>
#include <QCoreApplication>
#include <QStringList>
#include <QDebug>
#include <QVariant>
#include <QDateTime>
#include <errno.h>
#include "../../Client/ContentActivityEngine.h"


ContentActivityEngine* interface;

/**
 * Prints application usage on standard out.
 *
 * @param arguments the application arguments
 **/

int usage (const QStringList & arguments) {
    QTextStream cout(stdout);
    cout << "Usage:" << endl;

    cout << arguments[0] << " -retrieve all" << endl;
    cout << "    Prints <uri> <count for uri> for all <uri> in database, and then \"Total\" and the total count on last line." << endl;
    cout << arguments[0] << " -retrieve <uri>" << endl;
    cout << "    Prints <uri> <count for uri> on first line, and then \"Total\" and the total count on second line." << endl;
    cout << arguments[0] << " -store <uri>" << endl;
    cout << "    Adds a new activity for <uri>" << endl;
    cout << arguments[0] << " -help (or -h)" << endl;
    cout << "    Prints this page." << endl;

    cout << endl << "Tabulated output is always tab-separated." << endl;

    return 0;
}


int storeToDB(QString uri) {
    interface->addActivity(uri);
    return 0;
}

int retrieveAll() {
    QTextStream cout(stdout);
    QVariantList result = interface->activityCounts();
    int i=0;
    while(i < result.size()) {
        cout << result.at(i).toString() << "\t" << result.at(i+1).toString() << endl;
        i+=2;
    }
    return 0;
}

int retrieveUri(QString uri) {
    QTextStream cout(stdout);
    QVariantList result = interface->activityCount(uri);
    int i=0;
    while(i < result.size()) {
        cout << result.at(i).toString() << "\t" << result.at(i+1).toString() << endl;
        i+=2;
    }
    return 0;
}

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    QStringList arguments = QCoreApplication::arguments();
    QTextStream cout(stdout);

    // check commandline args
    if ( arguments.contains("--help") ||  arguments.contains("-h") || arguments.length() < 2) {
        return usage(arguments);
    }

    interface = new ContentActivityEngine ();

    if (arguments[1] == "-store") {
        return storeToDB(arguments[2]);
    } else if (arguments[1] == "-retrieve") {
        if (arguments[2] == "all") {
            return retrieveAll();
        } else {
            return retrieveUri(arguments[2]);
        }
    } else {
        return usage(arguments);
    }
}
