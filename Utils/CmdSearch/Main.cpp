/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <QCoreApplication>
#include <QStringList>
#include <QDebug>

#include "Search.h"

int main(int argc, char *argv[]) {
    QCoreApplication a(argc, argv);

    Search search;
    if (QCoreApplication::arguments().size() > 1) {
        if (QCoreApplication::arguments()[1] == "-h" || QCoreApplication::arguments()[1] == "--help") {
            qDebug () << QString("Usage : \n\t%1 [-items <uri(s)>] [-types <content type(s)>] [-h|--help]").arg(QCoreApplication::arguments()[0]);
            return 0;
        }
        else if (QCoreApplication::arguments()[1] == "-items") {
            QStringList uris ;
            for (int i=2; i<QCoreApplication::arguments().size(); i++)
                uris << QCoreApplication::arguments().at(i);
            qDebug() << "Searching for Items" ;
            search.search(uris);
        }
        else if (QCoreApplication::arguments()[1] == "-types") {
            QStringList content_types ;
            for (int i=2; i<QCoreApplication::arguments().size(); i++)
                content_types << QCoreApplication::arguments().at(i);
            qDebug() << "Searching for Types" ;
            search.search(QString(""), content_types);
        }
        else if (QCoreApplication::arguments()[1] == "-group") {
            QString groupUri, text;
            if (QCoreApplication::arguments().size() >= 3)
                groupUri = QCoreApplication::arguments().at(2);
            if (QCoreApplication::arguments().size() >= 4)
                text = QCoreApplication::arguments().at(3);
            
            qDebug() << "search In Group"  << groupUri << " ," << text;
            search.searchGroup(groupUri, text);
        }
        else {
            qDebug() << "Searching for Text" ;
            search.search (QCoreApplication::arguments()[1]);
        }
    } else {
        qDebug () << "Searching for All" ;
        search.search ();
    }
    
    return a.exec ();
}
