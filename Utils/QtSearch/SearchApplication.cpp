/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <QLayout>
#include <QLineEdit>
#include <QListView>

#include <QDebug>

#include "SearchApplication.h"
#include "ContentSearchEngine.h"

/** 
    PRIVATE
**/


/**
   PUBLIC
**/

SearchApplication::SearchApplication () 
    : m_layout(0),
      m_edit(0),
      m_list(0),
      m_model(0)
{
    // do nothing
}

SearchApplication::~SearchApplication () 
{
    delete m_layout;
    m_layout = 0;

    delete m_model;
    m_model = 0;
}

void SearchApplication::createUi () 
{
    m_edit = new QLineEdit ();
    connect( m_edit, SIGNAL(returnPressed()), this, SLOT(searchClicked()) );
    connect( m_edit, SIGNAL(textChanged(const QString&)), this, SLOT(searchClicked()) );

    m_list = new QListView();
    
    m_layout = new QVBoxLayout ();
    m_layout->addWidget (m_edit);
    m_layout->addWidget (m_list);
    
    this->setLayout (m_layout);
}

/**
   SLOTS 
**/
void SearchApplication::searchClicked () 
{
    QString text = m_edit->text ();

    QAbstractItemModel* old_model = m_model;
    
    m_model = ContentSearchEngine().doSearch (text);
    
    QObject::connect (m_model, SIGNAL(dataChanged(QModelIndex,QModelIndex)),
                      this, SLOT (showResult()));

    if (old_model) {
        old_model->disconnect(this);
        old_model->deleteLater();
    }
}

void SearchApplication::showResult () 
{
    m_list->setModel(m_model);
}
