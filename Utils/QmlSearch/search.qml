/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import Qt 4.7
import com.nokia.relevance 1.0

Rectangle {
    id: page
    width: 640; height: 480

    // the relevance model
    ItemListModel { id: relevance }
    ActionModel { id: relevanceAction }
    ActionEngine { id: relevanceActionEngine }
    ActivityModel { id: relevanceActivity }
    ActivityEngine { id: relevanceActivityEngine }

    /////////////////
    // searchBox
    /////////////////
    Rectangle {
        x: 5; y: 5 ; width: parent.width - 10 ; height: 40 //parent.height - 5
        color: "white"
        border.width: 4; border.color: "orange"; radius: 6
        TextInput {
            id: query
            text: relevance.searchQuery
            x: 5 ; y: 5 ; width: parent.width ; height: 35
            font.pointSize: 14; color: "darkGray"

            onTextChanged: {
                console.log("search text: " + query.text )
                relevance.search( query.text )
            }
        }
    }

    ///////////////////////////////
    // popUp ( Object Menu )
    //////////////////////////////
    Rectangle {
        id: popup
        opacity: 0
       // state: "popUpHide"
        Component {
            id: objectMenuDelegate
            Item {
                height: 50 ; width: listObjectMenu.width
                Rectangle {
                    height: 50; width: listObjectMenu.width
                    color: "darkGray"; border.width: 1; border.color: "green"; radius: 5
                    Text {
                        id: actionMenu
                        text:"   " + localizedName
                        font.pointSize: 14 ; color: "black"
                    }
                    MouseArea {
                        anchors.fill: parent;
                        onClicked: {
                            console.log("item clicked: " + index);
                            parent.border.color = "blue"
                            //popup.state = "popUpHide"
                        }
                    }
                }
            }
        }
        ListView {
            id: listObjectMenu
            x: 150 ; y: 150 ; width: 200 ; height: 150 ; clip: true
            model: relevanceAction
            delegate: objectMenuDelegate
        }
    }
    ////////////////
    // End of popUp
    ///////////////

    ///////////////
    // Main List
    ///////////////
    Component {
        id: delegate
        Item {
            height: 102 ; width: list.width

            function splitDate(strDate, dateFormat) {
                var dateArray = strDate.split("T");
                var yymmddArray = dateArray[0].split("-");
                return yymmddArray[dateFormat];
                }

            Rectangle {
                height: 100 ; width: parent.width - 2 ; color: "white" ; border.width: 2; border.color: "green"; radius: 6

                // default text
                Text {
                    id: defaultText
                    x: 300; y: 5;
                    text: defaultLabel
                    font.pointSize: 20 ; font.bold: true; color: "black"
                }

                MouseArea {
                    anchors.fill: parent;
                    onClicked: {
                        console.log("item clicked: " + index);
                        parent.border.color = "blue"                        
                        //popup.state = "popUpHide"
                        // Qt.formatDateTime( details.value("date"), Qt.DefaultLocaleLongDate )
                        console.log("search.qml: " + details["date"]);

                    }
                    onPressAndHold: {
                        console.log("press and hold ..Index=" + index + " identifier: " + identifier);
                        parent.border.color = "red"
                        relevanceAction.actionsForItem(identifier)
                        popup.state = "popUpShow"
                        //popup.opacity = 1
                    }
                }
            }
        }
    }

    ListView {
        id: list
        x: 4; y: 50; width: page.width - 10; height: 430; clip: true
        model: relevance
        delegate: delegate
    }

    states: [
        State {
            name: "popUpShow"
            PropertyChanges { target: popup; opacity:1 }
        },
        State {
            name: "popUpHide"
            PropertyChanges { target: popup; opacity:0 }
        }
    ]

    transitions: [
        Transition {
            from: "*" ; to: "popUpShow"
            PropertyAnimation { target: popup; properties: "opacity"; easing.type: Easing.OutBounce; duration: 1000 }
        },
        Transition {
            from: "*" ; to: "popUpHide"
            PropertyAnimation { target: popup; properties: "opacity"; easing.type: Easing.OutBounce; duration: 1000 }
        }
    ]

    Component.onCompleted: relevance.search("")
}
