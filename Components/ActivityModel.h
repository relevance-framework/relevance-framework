/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ActivityModel_H
#define ActivityModel_H

#include <QObject>
#include <QVariantList>
#include <QString>
#include <QAbstractItemModel>
#include <qdeclarative.h>

class QAbstractItemModel;
class ContentActivityEngine;

class ActivityModel: public QAbstractItemModel {

    Q_OBJECT

public:

    ActivityModel(QObject * parent=0);

    /**
     * Destructor.
     **/
    ~ActivityModel();

    /**
     * @brief Count how many times an uri has been accessed.
     *
     * The access count decays over time in order to give more weight to recent actions,
     * so the returned number is not a straight access frequency.
     *
     * @param subjectUri the uri for which access should be counted
     * @return a QVariantList containing the following: { uri, count, "Total", totalcount }
     * where uri and "Total" are the type QString and count and totalCount are of type double.
     **/
    Q_INVOKABLE void activityCount( const QString &subjectUri );

    /**
     * @brief Count how many times each uri in the database has been accessed.
     *
     * The access count decays over time in order to give more weight to recent actions,
     * so, the returned number is not a streight access frequency.
     *
     * @return A QVariantList containing the following: { uri1, count1, ..., uriN, countN, "Total", totalCount }
     * where uriX and "Total" are of type QString and countX and totalCount are of type double.
     **/
    Q_INVOKABLE void activityCounts(  );

    /**
     * Returns the number of rows in the model.
     *
     * @param parent not used.
     *
     * @return number of rows.
     **/
    virtual int rowCount (const QModelIndex & parent=QModelIndex()) const;

    /**
     * Returns the column count. Currently returns a hardcoded 1, as this model is not used in a real Qt view. This
     * should be changed if the model should be used in such a context.
     *
     * @param parent not used.
     *
     * @return always 1.
     **/
    virtual int columnCount (const QModelIndex & parent=QModelIndex()) const;

    /**
     * Returns the visual data for the item at the given @p index. The row and column of @p index are used to
     * find the item and then to select the proper column of data. The supported roles are in ContentSearchEngine::ResultItemDataRole.
     * Only Qt::DisplayRole is used of the default roles and it corresponds to ContentSearchEngine::PrimaryLabel.
     *
     * @param index the index representing the item and the data of the item.
     * @param role a role from ContentSearchEngine::ResultItemDataRole
     *
     * @return a variant with the data or an empty QVariant if there is no data for the @p role.
     **/
    virtual QVariant data (const QModelIndex & index, int role=Qt::DisplayRole) const;

    /**
     * Returns a model index that matches the given @p row, @p column and @p parent.
     *
     * @param row the wanted row index.
     * @param column the wanted column index.
     * @param parent not used.
     *
     * @return a new model index.
     **/
    virtual QModelIndex index (int row, int column, const QModelIndex & parent=QModelIndex()) const;

    /**
     * Returns the parent model index for the given @p index.
     *
     * @param index the index whose parent is wanted.
     *
     * @return an index, possibly invalid.
     **/
    virtual QModelIndex parent (const QModelIndex & index) const;

private:

    ContentActivityEngine *m_activity;

    //! the current proxied model
    QAbstractItemModel * m_activity_model;

    QAbstractItemModel* makeModel(QVariantList);

};

// allows the class to be exported to QML
QML_DECLARE_TYPE(ActivityModel);

#endif // ActivityModel_H
