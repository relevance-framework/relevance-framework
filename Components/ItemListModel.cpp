#include "ItemListModel.h"
#include "ContentSearchEngine.h"
#include "RelevanceItemModel.h"

#include <QDeclarativeView>
#include <QDeclarativeContext>

ItemListModel::ItemListModel (QObject * parent)
    : QAbstractItemModel(parent),
      m_groupsSupported (true),
      state(false),
      m_engine(new ContentSearchEngine(this)),
      m_model()
{
    //nothing
}

ItemListModel::ItemListModel (const ItemListModel &m):QAbstractItemModel(0) 
{
    Q_UNUSED(m);
}

ItemListModel::~ItemListModel () {
    delete m_engine;
    m_engine = 0;

    if (m_model) {
        delete m_model;
        m_model = 0;
    }
}

const QString & ItemListModel::text () const {
    return m_query;
}

void ItemListModel::setText (const QString &newText){
    // Do nothing if no change in query
    if (m_query == newText) return ;

    m_query = newText ;
    if (state) updateQuery() ;
    emit textUpdated();
}

const QVariant & ItemListModel::selectedItems() const {
    return (QVariant)m_selectedItems ;
}

void ItemListModel::setSelectedItems (const QVariant &s) {

    QStringList strList = s.toStringList();

    m_selectedItems = strList ;
    if (state) updateQuery ();
    emit selectedItemsUpdated();

}

const QStringList & ItemListModel::types () const {
    return m_contentTypes;
}

void ItemListModel::setTypes (const QStringList &types) {
    if (m_contentTypes != types) {
        m_contentTypes = types ;

        if (state) updateQuery ();
        emit typesUpdated ();
    }
}

const QStringList & ItemListModel::sources () const {
    return m_contentSources ;
}

void ItemListModel::setSources (const QStringList &sources) {
    if (m_contentSources != sources) {
        m_contentSources = sources ;

        if (state) updateQuery ();

        emit sourcesUpdated();
    }
}

const QString & ItemListModel::groupId () const
{
    return m_groupId ;
}

void ItemListModel::setGroupId (const QString &group) {
    if (m_groupId != group) {
        m_groupId = group ;

        if (state) updateQuery ();

        emit groupUpdated();
    }
}

const bool & ItemListModel::groupsSupported () const
{
    return m_groupsSupported;
}

void ItemListModel::setGroupsSupported (const bool &supported)
{
    if (m_groupsSupported == supported) return;

    m_groupsSupported = supported;

    if (state)
        updateQuery ();

    emit groupUpdated();
}

const bool & ItemListModel::live() const {
    return state ;
}

void ItemListModel::setLive (const bool state) {
    if (this->state == state) return ;

    this->state = state ;

    if (state) updateQuery();

    emit liveUpdated ();
}

int ItemListModel::rowCount (const QModelIndex & parent) const {
    Q_UNUSED( parent );
    return m_model ? m_model->rowCount() : 0;
}


int ItemListModel::columnCount (const QModelIndex & parent) const {
    Q_UNUSED( parent );
    return m_model ? m_model->columnCount() : 0;
}


QVariant ItemListModel::data (const QModelIndex & index, int role) const {
    if ( ! m_model ) {
        return QVariant();
    }

    QVariant value = m_model->data( index, role );

    return value;
}

QModelIndex ItemListModel::index (int row, int column, const QModelIndex & parent) const {
    Q_UNUSED( parent );
    return m_model ? createIndex( row, column ) : QModelIndex();
}

QModelIndex ItemListModel::parent (const QModelIndex & index) const {
    return m_model ? m_model->parent( m_model->index( index.row(), index.column()) ) : QModelIndex();
}

void ItemListModel::updateQuery () {
    QAbstractItemModel * old_model = m_model;

    if (old_model) {
        disconnect (old_model, SIGNAL (dataChanged (const QModelIndex &, const QModelIndex &)),
                    this, SIGNAL (modelReset ()));
    }

    beginResetModel();

    if(m_selectedItems.size() > 0) {
        m_model = m_engine->doSearchItems(m_selectedItems);
    }
    else {
        if (m_groupId.isEmpty()) {
            if (m_groupsSupported) {
                m_model = m_engine->doSearch(m_query, m_contentTypes, m_contentSources);
            }
            else {
                m_model = m_engine->doSearchWithoutGrouping(m_query, m_contentTypes, m_contentSources);
            }
        }
        // Search in a group
        else {
            if (m_groupsSupported) {
                m_model = m_engine->doSearchGroup(m_groupId, m_query, m_contentSources, m_contentSources);
            }
            else {
                m_model = m_engine->doSearchGroupWithoutGrouping(m_groupId, m_query, m_contentSources, m_contentSources);
            }
        }
    }

        if (m_model) {
            // Set roles form new model
            setRoleNames(m_model->roleNames());
            connect (m_model, SIGNAL(dataChanged(const QModelIndex &, const QModelIndex &)), this, SIGNAL(modelReset ()));
        }
        endResetModel();

        if (old_model) {
            delete old_model;
        }

}
