/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <QVariantList>
#include <QString>
#include <QAbstractItemModel>
#include <QStandardItemModel>

#include "ActivityModel.h"
#include "ContentActivityEngine.h"

ActivityModel::ActivityModel (QObject *parent) :  QAbstractItemModel(parent), m_activity(0), m_activity_model(0) {
    m_activity = new ContentActivityEngine;
}

ActivityModel::~ActivityModel () {
    delete m_activity;
    m_activity = 0;
    
    delete m_activity_model;
    m_activity_model = 0;
}

QAbstractItemModel* ActivityModel::makeModel(QVariantList activities) {
    QStandardItemModel* model = new QStandardItemModel();

    for(int i=0; i<activities.size()-2 ; i+=2) {
        QString uri = activities.at(i).toString();
        double count = activities.at(i+1).toDouble();
        QStandardItem* item = new QStandardItem();
        item->setData(uri);
        item->setData(count);
        model->appendRow(item);
    }

    QString Total = activities.at(activities.size()-2).toString();
    double totalCount = activities.at(activities.size()-1).toDouble();
    QStandardItem* item = new QStandardItem();
    item->setData(Total);
    item->setData(totalCount);
    model->appendRow(item);

    return model;
}

void ActivityModel::activityCount( const QString &subjectUri ) {
    beginResetModel();
    m_activity_model = makeModel(m_activity->activityCount(subjectUri));
    endResetModel();
}

void ActivityModel::activityCounts( ) {
    beginResetModel();
    m_activity_model = makeModel(m_activity->activityCounts());
    endResetModel();
}

int ActivityModel::rowCount (const QModelIndex & parent) const {
    Q_UNUSED( parent );
    return m_activity_model ? m_activity_model->rowCount() : 0;
}

int ActivityModel::columnCount (const QModelIndex & parent) const {
    Q_UNUSED( parent );
    return m_activity_model ? m_activity_model->columnCount() : 0;
}

QVariant ActivityModel::data (const QModelIndex & index, int role) const {
    if ( ! m_activity_model ) {
        return QVariant();
    }
    QVariant value = m_activity_model->data( m_activity_model->index( index.row(), index.column()), role );
    return value;
}

QModelIndex ActivityModel::index (int row, int column, const QModelIndex & parent) const {
    Q_UNUSED( parent );
    return m_activity_model ? createIndex( row, column ) : QModelIndex();
}

QModelIndex ActivityModel::parent (const QModelIndex & index) const {
    return m_activity_model ? m_activity_model->parent( m_activity_model->index( index.row(), index.column()) ) : QModelIndex();
}
