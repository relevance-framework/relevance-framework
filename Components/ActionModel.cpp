/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <QStandardItemModel>
#include <QStandardItem>

#include "ActionModel.h"
#include "ContentActionEngine.h"

ActionModel::ActionModel (QObject *parent) : QAbstractItemModel(parent), m_action(0), m_action_model(0) {
    m_action = new ContentActionEngine;

    QHash<int, QByteArray> role_names;
    role_names.insert( ContentActionEngine::Identifier, "identifier" );
    role_names.insert( ContentActionEngine::Name, "name" );
    role_names.insert( ContentActionEngine::LocalizedName, "localizedName" );
    setRoleNames( role_names );
}

ActionModel::~ActionModel () {
    delete m_action;
    m_action = 0;

    delete m_action_model;
    m_action_model = 0;
}

int ActionModel::rowCount (const QModelIndex & parent) const {
    Q_UNUSED( parent );

    return m_action_model ? m_action_model->rowCount() : 0;
}

int ActionModel::columnCount (const QModelIndex & parent) const {
    Q_UNUSED( parent );

    return m_action_model ? m_action_model->columnCount() : 0;
}

QVariant ActionModel::data (const QModelIndex & index, int role) const {
    if ( ! m_action_model ) {
        return QVariant();
    }
    QVariant value = m_action_model->data( m_action_model->index( index.row(), index.column()), role );
    return value;
}

QModelIndex ActionModel::index (int row, int column, const QModelIndex & parent) const {
    Q_UNUSED( parent );

    return m_action_model ? createIndex( row, column ) : QModelIndex();
}

QModelIndex ActionModel::parent (const QModelIndex & index) const {
    return m_action_model ? m_action_model->parent( m_action_model->index( index.row(), index.column()) ) : QModelIndex();
}

void ActionModel::actionsForItem(const QString& identifier) 
{
    beginResetModel();
    m_action_model = m_action->actionsForItem(identifier);
    endResetModel();
}

void ActionModel::actionsForItems(const QStringList& identifiers) 
{
    beginResetModel();
    m_action_model = m_action->actionsForItems(identifiers);
    endResetModel();    
}

void ActionModel::actionsForMimeType(const QString& mimetype) 
{
    beginResetModel();
    m_action_model = m_action->actionsForMimeType(mimetype);
    endResetModel();
}
