/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ACTIVITYENGINE_H
#define ACTIVITYENGINE_H

#include <QAbstractItemModel>
#include <qdeclarative.h>

class ContentActivityEngine;

class ActivityEngine:  public QObject  {

    Q_OBJECT

public:

    /**
     * Creates the proxy with the given @p parent. Sets up the content action engine.
     *
     * @param parent optional parent object.
     **/
    ActivityEngine(QObject * parent=0);

    /**
     * Destructor.
     **/
    ~ActivityEngine();

     /**
     * @brief Report a user activity that access a content item.
     *
     * If the item is stored in tracker, the reported subjectUri should be the same as the
     * tracker URI. The method is asynchronous, so the control will return to the client
     * immediately. There is no possibility to detect failures.
     * Please note that if the back end server is not currently running, and the client
     * application exists immediately after invoking this method, the invocation will silently
     * fail due to the way DBus operates. In this case, you must use the synchronous method
     * instead to ensure that the activity is stored to the database.
     *
     * @param subjectUri uri of the item that the user accessed (may not be empty)
     **/
    Q_INVOKABLE void addActivity( const QString &subjectUri );

    /**
     * @brief Report a user activity that access a content item.
     *
     * If the item is stored in tracker, the reported subjectUri should be the same as the
     * tracker URI. The method is asynchronous, so the control will return to the client
     * immediately. There is no possibility to detect failures.
     * Please note that if the back end server is not currently running, and the client
     * application exists immediately after invoking this method, the invocation will silently
     * fail due to the way DBus operates. In this case, you must use the synchronous method
     * instead to ensure that the activity is stored to the database.
     *
     * @param subjectUri uri of the item that the user accessed (may not be empty)
     * @param searchQuery current text in search box
     * @param contentTypes list of allowed content types in search
     * @param actionName invoked action
     **/
    Q_INVOKABLE void addSearchActivity( const QString &subjectUri, const QString &searchQuery, const QStringList &contentTypes, const QString &actionName );

private:

    ContentActivityEngine * m_activity;

};

// allows the class to be exported to QML
QML_DECLARE_TYPE(ActivityEngine);

#endif // ACTIVITYENGINE_H
