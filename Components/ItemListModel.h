#ifndef RELEVANCE_PROXY_H
#define RELEVANCE_PROXY_H

#include <QAbstractItemModel>
#include <QStringList>

#include <qdeclarative.h>
#include <QDeclarativeListProperty>

class ContentSearchEngine;

/**
 * Proxy class for the content search engine and result model for QML usage.
 *
 * This class is used to make the content search engine available to the QML side. It allows searches to be performed and
 * it also manages the result models from the engine. The QML side always sees an instance of this class, even if new
 * searches are performed and the model behind the scenes change and get deleted.
 *
 * Of the normal Qt data roles only Qt::DisplayRole is used, all other data is through custom roles.
 * The role names that are supported on the QML side can be seen from ContentSearchEngine::ResultItemDataRole. The names of
 * the roles on the QML side is simply the role enum with the first letter lower cased, e.g. @e primaryLabel.
 **/
class ItemListModel : public QAbstractItemModel {

    Q_OBJECT

    Q_PROPERTY( QString text READ text WRITE setText NOTIFY textUpdated );
    Q_PROPERTY( QStringList types READ types WRITE setTypes NOTIFY typesUpdated );
    Q_PROPERTY( QStringList sources READ sources WRITE setSources NOTIFY sourcesUpdated );
    Q_PROPERTY( QString groupId READ groupId WRITE setGroupId NOTIFY groupUpdated );
    Q_PROPERTY( bool groupsSupported READ groupsSupported WRITE setGroupsSupported NOTIFY groupUpdated );
    Q_PROPERTY( bool live READ live WRITE setLive NOTIFY liveUpdated );
    Q_PROPERTY( QVariant selectedItems READ selectedItems WRITE setSelectedItems NOTIFY selectedItemsUpdated );

public:

    /**
     * Creates the proxy with the given @p parent. Sets up the content search engine and all role names.
     *
     * @param parent optional parent object.
     **/
    ItemListModel (QObject * parent=0);

    ItemListModel (const ItemListModel &m) ;
    /**
     * Destructor.
     **/
    ~ItemListModel ();

    /**
     * Returns the number of rows in the model.
     *
     * @param parent not used.
     *
     * @return number of rows.
     **/
    virtual int rowCount (const QModelIndex & parent=QModelIndex()) const;

    /**
     * Returns the column count. Currently returns a hardcoded 1, as this model is not used in a real Qt view. This
     * should be changed if the model should be used in such a context.
     *
     * @param parent not used.
     *
     * @return always 1.
     **/
    virtual int columnCount (const QModelIndex & parent=QModelIndex()) const;

    /**
     * Returns the visual data for the item at the given @p index. The row and column of @p index are used to
     * find the item and then to select the proper column of data. The supported roles are in ContentSearchEngine::ResultItemDataRole.
     * Only Qt::DisplayRole is used of the default roles and it corresponds to ContentSearchEngine::PrimaryLabel.
     *
     * @param index the index representing the item and the data of the item.
     * @param role a role from ContentSearchEngine::ResultItemDataRole
     *
     * @return a variant with the data or an empty QVariant if there is no data for the @p role.
     **/
    virtual QVariant data (const QModelIndex & index, int role=Qt::DisplayRole) const;

    /**
     * Returns a model index that matches the given @p row, @p column and @p parent.
     *
     * @param row the wanted row index.
     * @param column the wanted column index.
     * @param parent not used.
     *
     * @return a new model index.
     **/
    virtual QModelIndex index (int row, int column, const QModelIndex & parent=QModelIndex()) const;

    /**
     * Returns the parent model index for the given @p index.
     *
     * @param index the index whose parent is wanted.
     *
     * @return an index, possibly invalid.
     **/
    virtual QModelIndex parent (const QModelIndex & index) const;

    // Property Functions
    const QString & text () const ;
    void setText (const QString &newText) ;

    const QStringList & types() const;
    void setTypes (const QStringList &t) ;

    const QStringList  & sources () const ;
    void setSources (const QStringList &s) ;

    const QString & groupId () const ;
    void setGroupId (const QString &group);

    const bool & groupsSupported () const;
    void setGroupsSupported (const bool &supported);

    const bool & live() const ;
    void setLive (const bool state) ;

    const QVariant & selectedItems () const ;
    void setSelectedItems (const QVariant &s);

signals:
    void textUpdated();
    void typesUpdated();
    void sourcesUpdated();
    void groupUpdated();
    void liveUpdated();
    void selectedItemsUpdated();

private:

    //static void appendType (QDeclarativeListProperty<QString> *list, QString *type);
    //static void appendSource (QDeclarativeListProperty<QString> *list, QString *source);
    void updateQuery ();

    //! current search query
    QString m_query;

    QStringList m_contentTypes ;
    QStringList m_contentSources ;
    QString m_groupId;
    bool m_groupsSupported;
    bool state;
    QStringList m_selectedItems ;

    QStringList m_defaultTypes ;

    //! the search engine
    ContentSearchEngine * m_engine;

    //! the current proxied model
    QAbstractItemModel * m_model;
};

// allows the class to be exported to QML
Q_DECLARE_METATYPE(ItemListModel)

#endif // RELEVANCE_PROXY_H
