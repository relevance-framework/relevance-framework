/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ACTIONENGINE_H
#define ACTIONENGINE_H

#include <QObject>
#include <QString>
#include <QStringList>

#include <QAbstractItemModel>
#include <qdeclarative.h>

class ContentActionEngine;

class ActionEngine:  public QObject  {

    Q_OBJECT

public:

    /**
     * Creates the proxy with the given @p parent. Sets up the content action engine.
     *
     * @param parent optional parent object.
     **/
    ActionEngine(QObject * parent=0);

    /**
     * Destructor.
     **/
    ~ActionEngine();

    /**
     * Execute action for a item
     *
     * @param itemIdentifier item
     * @param action, default on empty action
     **/
    Q_INVOKABLE bool executeItem(const QString& itemIdentifier, const QString& action = QString(""));

    /**
     * Execute action for items
     *
     * @param itemlist to execute
     * @param action, default action in case of empty
     **/
    Q_INVOKABLE bool executeItems(const QStringList& itemIdentifiers, const QString& action = QString(""));

private:

    ContentActionEngine * m_action;
};

// allows the class to be exported to QML
QML_DECLARE_TYPE(ActionEngine);

#endif // ACTIONENGINE_H
