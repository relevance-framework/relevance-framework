/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SimpleHarvester_H
#define SimpleHarvester_H

#include <QObject>
#include <QHash>
#include <QDateTime>
#include <QTimer>
#include <QSet>
#include <QVector>
#include <QStringList>
#include <QVariant>
#include <QDir>

#include "../../Daemon/AbstractHarvester.h"

enum PrivateItemClass {
    DOCUMENT,
    UNDEFINED
};

class SimpleHarvester 
  : public AbstractHarvester
{
    Q_OBJECT

    public:

        SimpleHarvester();
        ~SimpleHarvester();

    public:

        // Functions from AbstractHarvester interface.
        bool initialize();
        QStringList getItemClass(const QString &uri);
        QMap<QString,QVariant> getItemProperties(const QString &uri, const QString &classUri);
        QStringList getRecentChanges( const QString &lastUpdate );
        QStringList getRelatedItems(const QString &uri);
        QVector<QStringList> getSupportedClasses();

        QVector<QStringList> convertActions(const QStringList &);
        QVector<QStringList> convertActionsForMime(const QString &);
        bool executeItems(const QStringList &items, const QString &action);

    private:

        /**
         * Utility function for traversing user home directory and listing files
         * with correct types.
         **/
        QSet<QString> findFiles( QDateTime lastUpdate );

        /**
         * Utility function for determining item class from file name suffix.
         **/
        PrivateItemClass determineItemClass(QString uri);

};

#endif // SimpleHarvester_H
