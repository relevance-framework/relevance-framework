/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <QDebug>
#include <QDirIterator>

#include "SimpleHarvester.h"

static const QString DocumentType("http://www.semanticdesktop.org/ontologies/2007/03/22/nfo#Document");

SimpleHarvester::SimpleHarvester() 
{
    // do nothing
}

SimpleHarvester::~SimpleHarvester()
{
}

bool SimpleHarvester::initialize()
{
    return true;
}

QStringList SimpleHarvester::getItemClass(const QString &uri) 
{
    QStringList result;

    switch( determineItemClass( uri )) {
        case DOCUMENT:
            result << DocumentType;
            result << ""; // timestamp
            break;
        case UNDEFINED:
        default:
            break;
    }

    return result;
}

QStringList SimpleHarvester::getRelatedItems(const QString &uri)
{
    QStringList result;

    // Prepare to obtain files from the same directory
    QFileInfo file(uri);
    QDir dir = file.dir();
    dir.setFilter(QDir::Files | QDir::NoSymLinks);
    QFileInfoList list = dir.entryInfoList();

    switch( determineItemClass( uri )) {

        // Document type is used to demonstrate how the related items behave.
        // The definition of relation is simple, items are related if they
        // are located in the same directory.
        case DOCUMENT:
            for (int i = 0; i < list.size(); ++i) {
                QFileInfo fileInfo = list.at(i);
                if( file != fileInfo &&
                    determineItemClass( fileInfo.absoluteFilePath() ) != UNDEFINED )
                    result << fileInfo.absoluteFilePath();
            }
            break;

        case UNDEFINED:
        default:
            break;
    }

    return result;
}

QVector<QStringList> SimpleHarvester::getSupportedClasses()
{
    // One entry in classes vector contains class name and class parent.
    QVector<QStringList> classes;

    QStringList list;

    // case DOCUMENT
    list << DocumentType << "";
    classes << list;
    list.clear();

    return classes;
}

QStringList SimpleHarvester::getRecentChanges( const QString &lastUpdate )
{
    // Parameters are for added, removed and changed files.
    // Currently this SimpleHarvester can not detect file removals or
    // make the distiction between file change and file adding.
    QSet<QString> added = findFiles( QDateTime::fromString( lastUpdate,Qt::ISODate ) );
    QSet<QString> empty;

    // This Harvester function is called after signals are connected so
    // we can report recent changes using the itemsUpdated signal.
    emit itemsUpdated(added, empty, empty, QDateTime::currentDateTime());

    // Even though function signature hints that recent changes should
    // be returned in QStringList those should be sent using itemsUpdated
    // signal. The return value is obsolete.
    QStringList result;
    return result;
}

QMap<QString,QVariant> SimpleHarvester::getItemProperties(const QString &uri, const QString &/*classUri*/)
{
    QMap<QString,QVariant> itemProperties;
    QFileInfo file(uri);

    // In SimpleHarvester we do not parse files for meta data so just 
    // use file name as default label.
    itemProperties.insert( "defaultLabel", file.fileName() );

    return itemProperties;
}

QVector<QStringList> SimpleHarvester::convertActions(const QStringList &identifiers)
{
    qDebug() << "[SimpleHarvester] convertActions" << identifiers;
    QVector<QStringList> actionsList ;
    return actionsList ;
}

QVector<QStringList> SimpleHarvester::convertActionsForMime(const QString &identifier)
{
    qDebug() << "[SimpleHarvester] convertActionsForMime" << identifier;
    QVector<QStringList> actionsList ;
    return actionsList ;
}

bool SimpleHarvester::executeItems(const QStringList &items, const QString &action)
{
    Q_UNUSED(items);
    qDebug() << "[SimpleHarvester] executeItems" << action;
    return false;
}

QSet<QString> SimpleHarvester::findFiles( QDateTime lastUpdate )
{
    QSet<QString> files;
    QDir home = QDir::home();
    home.setFilter(QDir::Files | QDir::NoSymLinks);
    QDirIterator it(home, QDirIterator::Subdirectories);
    while (it.hasNext()) {
        // Only list files that have been modified recently and that
        // have supported class type.
        if( it.fileInfo().lastModified() > lastUpdate &&
            determineItemClass(it.fileInfo().filePath()) != UNDEFINED )
        {
            files << it.filePath();
        }
        it.next();
    }
    return files;
}

PrivateItemClass SimpleHarvester::determineItemClass(QString uri)
{
    QFileInfo file(uri);
    if( file.suffix().compare( "txt", Qt::CaseInsensitive ) == 0 )
        return DOCUMENT;
    else 
        return UNDEFINED;
}


