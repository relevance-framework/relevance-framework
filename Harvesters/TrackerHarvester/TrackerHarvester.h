/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef TRACKERHARVESTER_H
#define TRACKERHARVESTER_H

#include <QObject>
#include <QHash>
#include <QDateTime>
#include <QTimer>
#include <QSet>
#include <QVector>
#include <QStringList>

#include <contentaction/contentaction.h>

#include "../../Daemon/AbstractHarvester.h"

class TrackerDBus;

typedef enum {
    GRAPH =0,
    SUBJECT,
    PREDICATE,
    OBJECT
} TrackerUpdateTypes ;

class TrackerHarvester 
  : public AbstractHarvester
{
    Q_OBJECT
    public:
        TrackerHarvester();
        ~TrackerHarvester();

        bool initialize();
        QStringList getItemClass(const QString &uri);
        QMap<QString,QVariant> getItemProperties(const QString &uri, const QString &classUri);
        QString trackerIdToURI(const int &id);
        int uriToTrackerId (const QString &uri);
        QStringList getRecentChanges( const QString &lastUpdate );
        QStringList getRelatedItems(const QString &uri);
        QVector<QStringList> getSupportedClasses();

        QVector<QStringList> convertActions(const QStringList &);
        QVector<QStringList> convertActionsForMime(const QString &);
        bool executeItems(const QStringList &items, const QString &action = QString(""));

    private:
        QStringList getProperties();
        void resetTimer();
        //! Time when we got last update from tracker
        QDateTime m_lastUpdate;
        //! Timer for handling queues
        QTimer* m_queueTimer;
        //! The properties we watch for changes. Currently only contentAccessed.
        //TODO: Would be better to read from configuration file, but is currently hardcoded
        QHash<QString, bool> m_watchedProperties;
        void setWatchedProperties();
        //! tracker client object
        TrackerDBus* m_tracker;
        //! Queued additions
        QSet<QString> m_addQueue;
        //! Queued removals
        QSet<QString> m_removeQueue;
        //! Queued changes
        QSet<QString> m_changeQueue;
        //! Cache of properties
        QStringList m_properties;

    private slots:
        void processQueues();
        void retryInit();
        void graphUpdated(const QString &, const QVector<QList<int> > deletes, const QVector<QList<int> > inserts);
};

#endif // TRACKERHARVESTER_H
