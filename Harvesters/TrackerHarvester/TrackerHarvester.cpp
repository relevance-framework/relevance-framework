/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <QDebug>
#include <QTimer>
#include <QUrl>

#include "TrackerDBus.h"
#include "TrackerHarvester.h"


#define TYPE_PREDICATE "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"

static const QString s_favorite_tag_query(" (SELECT COUNT(?tag) > 0 { <%1> nao:hasTag ?tag FILTER (?tag = <http://www.semanticdesktop.org/ontologies/2007/08/15/nao#predefined-tag-favorite>) }) ");

static const QString InformationElement("http://www.semanticdesktop.org/ontologies/2007/01/19/nie#InformationElement");
static const QString PersonContactType("http://www.semanticdesktop.org/ontologies/2007/03/22/nco#PersonContact");
static const QString DocumentType("http://www.semanticdesktop.org/ontologies/2007/03/22/nfo#Document");
static const QString PlainTextDocumentType("http://www.semanticdesktop.org/ontologies/2007/03/22/nfo#PlainTextDocument");

static const QString PhotoType("http://www.tracker-project.org/temp/nmm#Photo"); // FIXME: Defining new type, instead need an alternate solution

static const QString SoftwareApplicationType("http://www.semanticdesktop.org/ontologies/2007/03/22/nfo#SoftwareApplication");
static const QString WebHistoryType("http://www.semanticdesktop.org/ontologies/2007/03/22/nfo#WebHistory");
static const QString BookmarkType("http://www.semanticdesktop.org/ontologies/2007/03/22/nfo#Bookmark");
static const QString EmailType("http://www.semanticdesktop.org/ontologies/2007/03/22/nmo#Email");
static const QString IMMessageType("http://www.semanticdesktop.org/ontologies/2007/03/22/nmo#IMMessage");
static const QString SMSMessageType("http://www.semanticdesktop.org/ontologies/2007/03/22/nmo#SMSMessage");
static const QString EventType("http://www.semanticdesktop.org/ontologies/2007/04/02/ncal#Event");
static const QString TodoType("http://www.semanticdesktop.org/ontologies/2007/04/02/ncal#Todo");
static const QString JournalType("http://www.semanticdesktop.org/ontologies/2007/04/02/ncal#Journal");
static const QString FeedMessageType("http://www.tracker-project.org/temp/mfo#FeedMessage");
static const QString MusicAlbumType("http://www.tracker-project.org/temp/nmm#MusicAlbum");
static const QString MusicPieceType("http://www.tracker-project.org/temp/nmm#MusicPiece");
static const QString ArtistType("http://www.tracker-project.org/temp/nmm#Artist");
static const QString VideoType("http://www.tracker-project.org/temp/nmm#Video");
static const QString RadioStationType("http://www.tracker-project.org/temp/nmm#RadioStation");
static const QString FileDataObjectType("http://www.semanticdesktop.org/ontologies/2007/03/22/nfo#FileDataObject"); // --- Which is this provider ??
static const QString FolderType ("http://www.semanticdesktop.org/ontologies/2007/03/22/nfo#Folder");
// FIXME: Ensure SQL parameters are always in quotes (sqlstring method).
// Also, recheck SQL parameters API

TrackerHarvester::TrackerHarvester() 
    : m_queueTimer(0),
      m_tracker(0)
{
    // do nothing
}

TrackerHarvester::~TrackerHarvester()
{
    if (m_tracker) { delete m_tracker ; m_tracker = 0; }
    if (m_queueTimer) { delete m_queueTimer ; m_queueTimer = 0 ; }
}

bool TrackerHarvester::initialize()
{
    qDebug() << "[Tracker Plugin] initializing";
    if (m_tracker == NULL) {
        m_tracker = new TrackerDBus(QString("QTRACKER_DIRECT"), InformationElement, this); 
        QObject::connect(m_tracker, SIGNAL(graphUpdated(const QString &, const QVector<QList<int> > , const QVector<QList<int> > )), 
                         this, SLOT(graphUpdated(const QString &, const QVector<QList<int> >, const QVector<QList<int> >)));
    }

    if (m_queueTimer == NULL) {
        m_queueTimer = new QTimer(this);
        m_queueTimer->setInterval(10000);
        m_queueTimer->setSingleShot(true);
        connect ( m_queueTimer, SIGNAL(timeout()), this, SLOT(processQueues()));
    }

    setWatchedProperties();

    return true;
}

void TrackerHarvester::setWatchedProperties() {
    m_watchedProperties.insert("http://www.semanticdesktop.org/ontologies/2007/01/19/nie#contentAccessed", true);
}

void TrackerHarvester::retryInit() {
    initialize();
}

void TrackerHarvester::graphUpdated(const QString &class_uri, const QVector<QList<int> > deletes, const QVector<QList<int> > inserts)
{
    Q_UNUSED(class_uri);
    bool change = false;
    qDebug() << "[Tracker Plugin] graphUpdated() {" ;
    //As we are intrested in both #type and #contentAccessed predicates
    //cash the trackerids of both uris for further comparision 
    int typePredicate = uriToTrackerId(TYPE_PREDICATE);
    QVector<int> watchablePredicates; 

    QHashIterator<QString, bool> iter(m_watchedProperties);
    while (iter.hasNext()){
        iter.next();
        watchablePredicates.append(uriToTrackerId(iter.key()));
    }
    

    for (int i=0; i<deletes.size(); i++) {
        if (deletes.at(i).at(PREDICATE) == typePredicate) {
            m_removeQueue << trackerIdToURI(deletes.at(i).at(SUBJECT));
            change = true;
        }
        else if(watchablePredicates.contains(deletes.at(i).at(PREDICATE))){
            m_changeQueue << trackerIdToURI(deletes.at(i).at(SUBJECT));
            change = true;
        }
    }

    for (int i=0; i<inserts.size(); i++) {
        if (inserts.at(i).at(PREDICATE) == typePredicate){
            m_addQueue << trackerIdToURI (inserts.at(i).at(SUBJECT));
            change = true;
        }
        else if(watchablePredicates.contains(inserts.at(i).at(PREDICATE))){
            m_changeQueue << trackerIdToURI(inserts.at(i).at(SUBJECT));
            change = true;
        }
    }
    
    // If anything intresting happened, touch the timer
    if (change) {
        qDebug () << "Touching Process Que Timer..." ;
        m_lastUpdate = QDateTime::currentDateTime();
        processQueues() ;
        //resetTimer();
    }
    qDebug() << "[Tracker Plugin] graphUpdated() }" ;
}

QString TrackerHarvester::trackerIdToURI(const int &id)
{
    QString query = "SELECT tracker:uri(%1) {}" ;

    QVector<QVariantList> result = m_tracker->rawSparqlQuerySync (query.arg(QString::number(id))) ;
    if (result.size() == 0 || result.at(0).size() == 0) return QString() ;
    
    return result.at(0).at(0).toUrl().toEncoded();
}

int TrackerHarvester::uriToTrackerId (const QString &uri)
{
    QString query = "SELECT tracker:id(<%1>) {}" ;

    QVector<QVariantList> result = m_tracker->rawSparqlQuerySync (query.arg(uri)) ;
    if (result.size() == 0 || result.at(0).size() == 0) return -1;
    
    return result.at(0).at(0).toInt();
}

void TrackerHarvester::processQueues() 
{
    qDebug() << "[Tracker Plugin] processing queues.";

    emit itemsUpdated(m_addQueue, m_removeQueue, m_changeQueue, m_lastUpdate);

    m_addQueue.clear();
    m_removeQueue.clear();
    m_changeQueue.clear();
}

QStringList TrackerHarvester::getItemClass(const QString &uri) {
    QString query = "SELECT ?c ?t WHERE {<%1> rdf:type ?c. <%1> rdf:type nie:InformationElement. <%1> tracker:added ?t. }";

    QVector<QVariantList> result = m_tracker->rawSparqlQuerySync(query.arg(uri));

    // only return the last item as it seems to be the correct one
    QVariantList list = result.size() ? result.at(result.size()-1) : QVariantList();

	QStringList objectClasses;
	foreach(QVariant item, list)
		objectClasses << item.toUrl().toEncoded();

	return objectClasses;
}

QStringList TrackerHarvester::getRelatedItems(const QString &uri)
{
    QStringList trackerQueries;
    QVector<QVariantList> result;
    QStringList returnValue;
    // currently we have undirected graph, so we query to both directions
    trackerQueries << "SELECT ?x ?p WHERE {<%1> ?p ?x.}";
    trackerQueries << "SELECT ?x ?p WHERE {?x ?p <%1>.}";

    for (int k = 0; k < trackerQueries.size(); k++) {
        QString trackerQuery = trackerQueries.at(k).arg(uri);
        result += m_tracker->rawSparqlQuerySync(trackerQuery);
    }

    for(int i=0; i < result.size(); i++) {
        QVariantList strList = result.at(i);
        QString propStr = strList.at(1).toUrl().toEncoded();
        if (!m_properties.contains(propStr)) {
            continue;
        }
        returnValue << strList.at(0).toUrl().toEncoded();
    }

    return returnValue;
}

QVector<QStringList> TrackerHarvester::getSupportedClasses()
{
    QVector<QVariantList> result = m_tracker->rawSparqlQuerySync("SELECT ?c ?s WHERE {?c a rdfs:Class. OPTIONAL {?c rdfs:subClassOf ?s.}}");

    QVector<QStringList> classes;

    for (int i=0; i<result.size(); i++) {
        QVariantList row = result.at(i) ;
        classes.append (QStringList() << row.at(0).toUrl().toEncoded() << row.at(1).toUrl().toEncoded());
    }
    return classes;
}

QStringList TrackerHarvester::getProperties()
{
    if (m_properties.size() == 0)
    {
        QString query ("SELECT ?p WHERE {" 
                       "{?p a rdf:Property; <http://www.w3.org/2000/01/rdf-schema#range> ?r. "
                       "?r <http://www.w3.org/2000/01/rdf-schema#subClassOf> <http://www.semanticdesktop.org/ontologies/2007/01/19/nie#InformationElement>.} "
                       "UNION "
                       "{?p a rdf:Property; <http://www.w3.org/2000/01/rdf-schema#range> <http://www.semanticdesktop.org/ontologies/2007/01/19/nie#InformationElement>}"
                       "}");
        QVector<QVariantList> result = m_tracker->rawSparqlQuerySync (query);

        for (int i=0; i<result.size(); i++) {
            QVariantList row = result.at(i);

            m_properties << row.at(0).toUrl().toEncoded();
        }
    }

    return m_properties;
}

QStringList TrackerHarvester::getRecentChanges( const QString &lastUpdate )
{
    qDebug ()  << "TrackerHarvester::getRecentChanges :" << lastUpdate;
    QSet<QString> recentlyChanged;
    // TODO: remove ?c and ?t from the query, we only need to look for ?o
    QString query = "SELECT ?o ?c ?t WHERE {?o rdf:type ?c. ?o rdf:type nie:InformationElement. ?o tracker:added ?t. "
                    "FILTER (?t > '%1') "
                    "} LIMIT %2 OFFSET %3";

    int offset = 0;
    int count;
    // Each request takes time, so fewer is better, but tracker will fail if there are too many results
    const int batchSize = 100000;
    do {
        QVector<QVariantList> objectClasses = m_tracker->rawSparqlQuerySync(query.arg(lastUpdate, QString::number(batchSize), QString::number(offset)));
        // qDebug() << "Update query:" << query.arg(lastUpdate(), QString::number(batchSize), QString::number(offset));

        count = objectClasses.size();

        for(int i=0; i<count; i++) {
            QString objectName = objectClasses.at(i).at(0).toUrl().toEncoded();
            recentlyChanged << objectName;
        }
        offset += count;
    } while (count == batchSize);

    return QStringList(recentlyChanged.toList());
}

QMap<QString,QVariant> TrackerHarvester::getItemProperties(const QString &uri, const QString &classUri)
{
    bool grouping = false; // TRUE if group information available
    bool isGroup = false; // TRUE if this is a group, ex: Album, Artist, etc.,
    QStringList groupUris; // list of group uris in witch this is part of.
    QStringList groupPrimaryLabels ;

    QMap<QString,QVariant> itemProperties;

    QString primaryText(""), secondaryText(""), tertiaryText("");
    QString primaryImage(""), secondaryImage(""), tertiaryImage("") ;
    QString ftsText ("");

    int fts_field_index = 0;
    QVector<QVariantList> results ;
    grouping = false;
    /* get default label and other information based on class type of classUri */
    if (classUri == PersonContactType)
    {
        
        if (uri == "http://www.semanticdesktop.org/ontologies/2007/03/22/nco#default-contact-me") primaryText = "Me";
        else if (uri == "http://www.semanticdesktop.org/ontologies/2007/03/22/nco#default-contact-emergency") primaryText = "Emergency";
        else {
            QString moreInfoQuery = QString ("SELECT nco:fullname(<%1>)"
                                             " tracker:coalesce (nco:imStatusMessage(?im_addr), nco:locality(?addr))"
                                             " tracker:coalesce(nie:url(nco:photo(<%1>)), nie:url(nco:imAvatar(?im_addr)))"
                                             " nco:imPresence(?im_addr) "
                                             " nco:nameFamily(<%1>) nco:nameGiven(<%1>) nco:imNickname(?im_addr) "
                                             " nco:emailAddress(?email_1) GROUP_CONCAT (nco:emailAddress(?email), \" \")"
                                             " nco:phoneNumber(?phone_1) GROUP_CONCAT (nco:phoneNumber(?phone), \" \")" 
                                             " nco:region(?addr) nco:country(?addr) nco:streetAddress(?addr) nco:postalcode(?addr) nco:locality(?addr)"
                                             " nco:pobox(?addr) nco:addressLocation(?addr) nco:extendedAddress(?addr)"
                                             " WHERE { OPTIONAL { <%1> nco:hasIMAddress ?im_addr }."
                                             " OPTIONAL { <%1> nco:hasEmailAddress ?email_1 }."
                                             " OPTIONAL { <%1> nco:hasPhoneNumber ?phone_1 }."
                                             " OPTIONAL { <%1> nco:hasPostalAddress ?addr }." 
                                             " OPTIONAL { <%1> nco:hasAffiliation ?aff. ?aff nco:hasPhoneNumber ?phone. }."
                                             " OPTIONAL { <%1> nco:hasAffiliation ?aff. ?aff nco:hasEmailAddress ?email.}."
                                             " OPTIONAL { <%1> nco:hasAffiliation ?aff. ?aff nco:hasPostalAddress ?addr.}." 
                                             " OPTIONAL { <%1> nco:hasAffiliation ?aff. ?aff nco:hasIMAddress ?im_addr.}. }") ; 

            moreInfoQuery = moreInfoQuery.arg (uri) ;
            results = m_tracker->rawSparqlQuerySync (moreInfoQuery);

            if (results.size () != 1) {
                qCritical() << "Unable to get contact infromation for uri : "<< uri ;
            }
            else
            {
                const QString &full = results.at(0).at(0).toString();
                const QString &family = results.at(0).at(4).toString(); // nco:nameFamily(nmo:from())
                const QString &given = results.at(0).at(5).toString(); // nco:nameGiven(nmo:from())
                const QString &nick = results.at(0).at(6).toString(); // nco:imNickname(nco:hasIMAddress) 

                if (!full.isEmpty()) primaryText = full ;
                else if (!family.isEmpty() || !given.isEmpty()) primaryText = QString (given + " " + family).trimmed();
                else primaryText = nick ;

                fts_field_index = 4 ;

                // Details ;
                if (results.at(0).at(0).isValid()) itemProperties.insert ("nco:fullname", results.at(0).at(0));
                if (results.at(0).at(4).isValid()) itemProperties.insert ("nco:nameFamily", results.at(0).at(4));
                if (results.at(0).at(5).isValid()) itemProperties.insert ("nco:nameGiven", results.at(0).at(5));
                if (results.at(0).at(6).isValid()) itemProperties.insert ("nco:nickname", results.at(0).at(6));
                if (results.at(0).at(1).isValid()) itemProperties.insert ("nco:imStatusMessage", results.at(0).at(1));
                if (results.at(0).at(2).isValid()) itemProperties.insert ("nco:photo", results.at(0).at(2));
                if (results.at(0).at(3).isValid()) itemProperties.insert ("nco:imPresence", results.at(0).at(3));
                if (results.at(0).at(7).isValid()|| results.at(0).at(8).isValid()) 
                    itemProperties.insert ("nco:emailAddress", QString (results.at(0).at(7).toString() + " " + results.at(0).at(8).toString()).trimmed());
                if (results.at(0).at(9).isValid() || results.at(0).at(10).isValid())
                    itemProperties.insert ("nco:phoneNumber", QString (results.at(0).at(9).toString() + " " + results.at(0).at(10).toString()).trimmed());
                if (results.at(0).at(11).isValid()) itemProperties.insert ("nco:region", results.at(0).at(11));
                if (results.at(0).at(12).isValid()) itemProperties.insert ("nco:country", results.at(0).at(12));
                if (results.at(0).at(13).isValid()) itemProperties.insert ("nco:streetAddress", results.at(0).at(13));
                if (results.at(0).at(14).isValid()) itemProperties.insert ("nco:postalcode", results.at(0).at(14));
                if (results.at(0).at(15).isValid()) itemProperties.insert ("nco:locality", results.at(0).at(15));
            }
        }
    }
    else if (classUri == DocumentType or classUri == PlainTextDocumentType)
    {
        QString moreInfoQuery = QString ("SELECT nfo:fileName(<%1>) nie:mimeType(<%1>) nfo:fileSize(<%1>) nfo:tableOfContents(<%1>) WHERE { }").arg (uri);

        results = m_tracker->rawSparqlQuerySync (moreInfoQuery);

        if (results.size () != 1) {
            qCritical() << "Unable to get Document infromation for uri : "<< uri ;
        }
        else
        {
            const QString &filename = results.at(0).at(0).toString();
            int lastDot = filename.lastIndexOf('.') ;

            primaryText = filename.left (lastDot-1) ; // nfo:fileName with out extension

            if (results.at(0).at(0).isValid()) itemProperties.insert ("nfo:fileName", filename);
            if (results.at(0).at(1).isValid()) itemProperties.insert ("nie:mimeType", results.at(0).at(1));
            if (results.at(0).at(2).isValid()) itemProperties.insert ("nfo:fileSize", results.at(0).at(2));
            fts_field_index = 3;
        }
    }
    else if (classUri == PhotoType)
    {
        QString moreInfoQuery = QString ("SELECT tracker:coalesce(nie:title(<%1>), nfo:fileName(<%1>))" 
                                         " GROUP_CONCAT(nao:prefLabel(?tag), \",\") nie:mimeType(<%1>) nie:url(<%1>) nie:contentCreated(<%1>)"
                                         " nfo:genre(<%1>)"
                                         " WHERE { OPTIONAL { <%1> nao:hasTag ?tag }. }").arg(uri) ;

        results = m_tracker->rawSparqlQuerySync (moreInfoQuery);

        if (results.size () != 1) {
            qCritical() << "Unable to get Photo infromation for uri : "<< uri ;
        }
        else
        {
            primaryText = results.at(0).at(0).toString() ; // nie:title (or) nfo:fileName
            if (results.at(0).at(4).isValid()) {
                QDate date = results.at(0).at(4).toDateTime().date();
                grouping = true;
				// Group Label : mon YYYY
                groupPrimaryLabels.append (QDate::shortMonthName(date.month(), QDate::StandaloneFormat) + " " + QString::number(date.year()));
                // Group URI : "urn:uuid:mm-yyyy"
                groupUris.append (QString("urn:uuid:") + QString::number(date.year()) + "-" + QString::number(date.month()));
            }
            if (results.at(0).at(0).isValid ()) itemProperties.insert ("nie.title",  results.at(0).at(0));
            if (results.at(0).at(1).isValid ()) itemProperties.insert ("nao.hasTag", results.at(0).at(1));
            if (results.at(0).at(2).isValid ()) itemProperties.insert ("nie:mimeType", results.at(0).at(2));
            if (results.at(0).at(3).isValid ()) itemProperties.insert ("nie:url", results.at(0).at(3).toUrl().toEncoded());
            if (results.at(0).at(4).isValid ()) itemProperties.insert ("nie:contentCreated", results.at(0).at(4));
            fts_field_index = 5 ; 
        }
    }
    else if (classUri == SoftwareApplicationType) // nfo:Software
    {
        QString moreInfoQuery = QString ("SELECT nie:title(<%1>) nfo:softwareIcon(<%1>)"
                                         " GROUP_CONCAT(nao:prefLabel(?tag), \",\") nfo:softwareCmdLine(<%1>) WHERE {}").arg(uri) ;
        results = m_tracker->rawSparqlQuerySync (moreInfoQuery);

        if (results.size () != 1) {
            qCritical() << "Unable to get Software Applicaiton infromation for uri : "<< uri ;
        }
        else
        {
            primaryText = results.at(0).at(0).toString() ; // nie:title

            itemProperties.insert ("nie:title", results.at(0).at(0));
            if(results.at(0).at(1).isValid()) itemProperties.insert ("nfo:softwareIcon", results.at(0).at(1).toUrl().toEncoded());

            fts_field_index = 2;
        }

    }
    else if (classUri == WebHistoryType) // nfo:WebHistory
    {
        QString moreInfoQuery = QString ("SELECT nie:title(<%1>) nfo:uri(<%1>)"
                                         " nfo:domain(<%1>) nfo:uri(<%1>) GROUP_CONCAT(nao:prefLabel(?tag), \",\") WHERE {}").arg(uri) ;
        results = m_tracker->rawSparqlQuerySync (moreInfoQuery);

        if (results.size () != 1) {
            qCritical() << "Unable to get Web history infromation for uri : "<< uri ;
        }
        else
        {
            primaryText = results.at(0).at(0).toString() ; // nie:title

            if (results.at(0).at(0).isValid()) itemProperties.insert ("nie:title", results.at(0).at(0));
            if (results.at(0).at(1).isValid()) itemProperties.insert ("nie:uri", results.at(0).at(1));

            fts_field_index = 2;
        }
    }
    else if (classUri == BookmarkType)
    {
        //FIXME : range of nfo:bookmarks is rdfs:DataObject, we need to get exact fields of DataObject rather
        //than nfo:bookmarks itself
        QString moreInfoQuery = QString ("SELECT nie:title(<%1>) nfo:bookmarks(<%1>)"
                                         " nfo:description(<%1> GROUP_CONCAT(nao:prefLabel(?tag), \",\") WHERE {}").arg(uri) ;
        results = m_tracker->rawSparqlQuerySync (moreInfoQuery);

        if (results.size () != 1) {
            qCritical() << "Unable to get Bookmark infromation for uri : "<< uri ;
        }
        else
        {
            primaryText = results.at(0).at(0).toString() ; // nie:title
            if (primaryText.isEmpty()) primaryText =  results.at(0).at(1).toString() ; // if not title url of the bookmark

            if (results.at(0).at(0).isValid()) itemProperties.insert ("nie:title", results.at(0).at(0));
            if (results.at(0).at(1).isValid()) itemProperties.insert ("nfo:bookmarks", results.at(0).at(1));
            fts_field_index = 2;
        }
    }

    else if (classUri == EmailType)
    {
        QString moreInfoQuery = QString ("SELECT nmo:isSent(<%1>) tracker:coalesce(nco:nameFamily(nmo:from(<%1>), nco:nameFamily(nmo:to(<%1>))"
                                         " tracker:coalesce(nco:nameGiven(nmo:from(<%1>), nco:nameGiven(nmo:to(<%1>))"
                                         " tracker:coalesce(nco:emailAddress(nco:hasEmailAddress(nmo:from(<%1>))), nco:emailAddress(nco:hasEmailAddress(nmo:to(<%1>))))"
                                         " nmo:messageSubject(<%1>) tracker:coalesce(nmo:sentDate(<%1>), nmo:receivedDate(<%1>))"
                                         " nmo:hasAttachment(<%1>) WHERE { }").arg(uri) ;
        results = m_tracker->rawSparqlQuerySync (moreInfoQuery);

        if (results.size () != 1) {
            qCritical() << "Unable to get Email infromation for uri : "<< uri ;
        }
        else
        {
            bool isSent = results.at(0).at(0).toBool() ;
            QString family= results.at(0).at(1).toString() ; // nco:nameFamily(nmo:from())
            QString given= results.at(0).at(2).toString() ; // nco:nameGiven(nmo:from())
            primaryText = family.isEmpty() && given.isEmpty() ? results.at(0).at(3).toString() : (given + " " + family).trimmed(); // "givenName familyName" 

            if (results.at(0).at(0).isValid()) itemProperties.insert ("nmo:isSent", results.at(0).at(0));
            if (isSent) {
                if (results.at(0).at(1).isValid() || results.at(0).at(2).isValid())
                    itemProperties.insert ("nmo:to", QString(results.at(0).at(1).toString() + " " + results.at(0).at(2).toString()).trimmed()); 
                if (results.at(0).at(5).isValid()) itemProperties.insert ("nmo:sentDate", results.at(0).at(5));
            }
            else {
                if (results.at(0).at(1).isValid() || results.at(0).at(2).isValid())
                    itemProperties.insert ("nmo:from", QString(results.at(0).at(1).toString() + " " + results.at(0).at(2).toString()).trimmed()); 
                if (results.at(0).at(5).isValid()) itemProperties.insert ("nmo:receivedDate", results.at(0).at(5));
            }
            if (results.at(0).at(3).isValid()) itemProperties.insert ("nco:emailAddress", results.at(0).at(3));
            if (results.at(0).at(4).isValid()) itemProperties.insert ("nmo:messageSubject", results.at(0).at(4));
        }
    }
    else if (classUri == IMMessageType)
    {
        QString moreInfoQuery = QString ("SELECT nmo:isSent(<%1>)"
                                         " tracker:coalesce(tracker:coalesce(nco:imNickname(?from), nco:imID(?from)), tracker:coalesce(nco:imNickname(?to), nco:imID(?to)))"
                                         " nie:plainTextContent(<%1>)" 
                                         " tracker:coalesce(nmo:receivedDate(<%1>), nmo:sentDate(<%1>))"
                                         " WHERE { OPTIONAL { <%1> nmo:from ?fr. ?fr nco:hasIMAddress ?from }."
                                         " OPTIONAL { <%1> nmo:to ?t. ?t nco:hasIMAddress ?to}. }").arg(uri);
        results = m_tracker->rawSparqlQuerySync (moreInfoQuery);

        if (results.size () != 1) {
            qCritical() << "Unable to get IM message infromation for uri : "<< uri ;
        }
        else
        {
            bool isSent = results.at(0).at(0).toBool() ; // nmo:isSent
            primaryText = results.at(0).at(1).toString(); // ? nco:imNickname (nco:hasIMAddres(nmo:from|nmo:to))

            if (results.at(0).at(0).isValid()) itemProperties.insert ("nmo:isSent", results.at(0).at(0));
            if (results.at(0).at(1).isValid()) itemProperties.insert (isSent ? "nmo:to" : "nmo:from", results.at(0).at(1));
            if (results.at(0).at(3).isValid()) itemProperties.insert (isSent ? "nmo:sentDate" : "nmo:receivedDate", results.at(0).at(3));
            if (results.at(0).at(2).isValid()) itemProperties.insert ("nie:plainTextContent", results.at(0).at(2));
        }
    }
    else if (classUri == SMSMessageType)
    {
        QString moreInfoQuery = QString ("SELECT nmo:isSent(<%1>) nco:nameFamily(?from) nco:nameGiven(?from) maemo:localPhoneNumber(?from_phone)" 
                                         " nco:nameFamily(?to) nco:nameGiven(?to) maemo:localPhoneNumber(?to_phone)"
                                         " rdfs:label(?vcard) nie:plainTextContent(<%1>) nmo:receivedDate(<%1>)"
                                         " WHERE { OPTIONAL {<%1> nmo:from ?from. ?from nco:hasPhoneNumber ?from_phone }."
                                         " OPTIONAL {<%1> nmo:to ?to. ?to nco:hasPhoneNumber ?to_phone }."
                                         " OPTIONAL {<%1> nmo:fromVCard ?vcard }. }").arg(uri);
        results = m_tracker->rawSparqlQuerySync (moreInfoQuery);

        if (results.size () != 1) {
            qCritical() << "Unable to get SMS message infromation for uri : "<< uri ;
        }
        else
        {
            QString family, given, phone ;
            bool isSent = results.at(0).at(0).toBool() ; // nmo:isSent
            if (!isSent)
            {
                family = results.at(0).at(1).toString()  ; // nco:nameFamily(nmo:from)
                given = results.at(0).at(2).toString() ; // nco:nameGiven(nmo:from)
                phone = family.isEmpty() && given.isEmpty() ? results.at(0).at(3).toString() : ""; // maemo:localPhoneNumber(nco:hasPhoneNumber(nmo:from))
            }
            else
            {
                family = results.at(0).at(4).toString()  ; // nco:nameFamily(nmo:to)
                given = results.at(0).at(5).toString() ; // nco:nameGiven(nmo:to)
                phone = family.isEmpty() && given.isEmpty() ? results.at(0).at(6).toString() : ""; // maemo:localPhoneNumber(nco:hasPhoneNumber(nmo:to))
            }

            primaryText = !phone.isEmpty() ? phone : (given + " " + family).trimmed() /*+ " | " + time */;

            if (results.at(0).at(0).isValid()) itemProperties.insert ("nmo:isSent", results.at(0).at(0));
            if (isSent){
                itemProperties.insert ("nmo:to", (given + " " + family).trimmed());
            }
            else{
                itemProperties.insert ("nmo:from", (given + " " + family).trimmed());
                if (results.at(0).at(9).isValid()) itemProperties.insert ("nmo:receivedDate", results.at(0).at(9));
            }
            if (!phone.isEmpty()) itemProperties.insert ("maemo:localPhoneNumber", phone);
            if (results.at(0).at(7).isValid()) itemProperties.insert ("nmo:fromVCard", results.at(0).at(7));
            if (results.at(0).at(8).isValid()) itemProperties.insert ("nie:plainTextContent", results.at(0).at(8));

        }
    }
    else if (classUri == EventType)
    {
        QString moreInfoQuery = QString ("SELECT ncal:summary(<%1>) ncal:dateTime(ncal:dtstart(<%1>)) ncal:dateTime(ncal:dtend(<%1>)) ncal:hasAlarm(<%1>)"
                                         " ncal:comment(<%1>) ncal:location(<%1>) ncal:description(<%1>) ncal:contact(<%1>) "
                                         " nco:fullname(ncal:involvedContact(ncal:organizer(<%1>))) nco:fullname(ncal:involvedContact(ncal:attendee(<%1>)))"
                                         " WHERE { }").arg(uri) ;
        results = m_tracker->rawSparqlQuerySync (moreInfoQuery);

        if (results.size () != 1) {
            qCritical() << "Unable to get Event infromation for uri : "<< uri ;
        }
        else
        {
            primaryText = results.at(0).at(0).toString() ; //ncal:summary

            if (results.at(0).at(0).isValid()) itemProperties.insert ("ncal:summary", results.at(0).at(0));
            if (results.at(0).at(1).isValid()) itemProperties.insert ("ncal:dtstart", results.at(0).at(1));
            if (results.at(0).at(2).isValid()) itemProperties.insert ("ncal:dtend", results.at(0).at(2));
            if (results.at(0).at(4).isValid()) itemProperties.insert ("ncal:hasAlarm", results.at(0).at(4));

            fts_field_index = 4;
        }
    }
    else if (classUri == TodoType)
    {
        QString moreInfoQuery = QString ("SELECT ncal:summary(<%1>) ncal:dateTime(ncal:due)" 
                                         " ncal:comment(<%1>) ncal:location(<%1>) ncal:description(<%1>)"
                                         " nco:fullname(ncal:involvedContact(ncal:organizer(<%1>))) nco:fullname(ncal:involvedContact(ncal:attendee(<%1>)))"
                                         " WHERE {  }").arg(uri) ;
        results = m_tracker->rawSparqlQuerySync (moreInfoQuery);

        if (results.size () != 1) {
            qCritical() << "Unable to get Todo infromation for uri : "<< uri ;
        }
        else
        {
            primaryText = results.at(0).at(0).toString() ; //ncal:summary

            if (results.at(0).at(0).isValid()) itemProperties.insert ("ncal:summary", results.at(0).at(0));
            if (results.at(0).at(1).isValid()) itemProperties.insert ("ncal:due", results.at(0).at(1));
            fts_field_index = 2;
        }
    }
    else if (classUri == JournalType)
    {
        QString moreInfoQuery = QString ("SELECT ncal:description(<%1>) ncal:lastModified(<%1>)"
                                         " ncal:comment(<%1>) ncal:location(<%1>) ncal:description(<%1>) ncal:contact(<%1>)"
                                         " nco:fullname(ncal:involvedContact(ncal:organizer(<%1>))) nco:fullname(ncal:involvedContact(ncal:attendee(<%1>)))"
                                         " WHERE { }").arg(uri) ;
        results = m_tracker->rawSparqlQuerySync (moreInfoQuery);

        if (results.size () != 1) {
            qCritical() << "Unable to get Journal infromation for uri : "<< uri ;
        }
        else
        {
            primaryText = results.at(0).at(0).toString() ; //ncal:description

            if (results.at(0).at(0).isValid()) itemProperties.insert ("ncal:description", results.at(0).at(0));
            if (results.at(0).at(1).isValid()) itemProperties.insert ("ncal:lastModified", results.at(0).at(1));

            fts_field_index = 2;
        }
    } 
    else if (classUri == FeedMessageType)
    {
        QString moreInfoQuery = QString ("SELECT nie:title(<%1>) nie:title(nie:isLogicalPartOf(<%1>)) nie:contentCreated(<%1>) \
                WHERE { }").arg(uri);
        results = m_tracker->rawSparqlQuerySync (moreInfoQuery);

        if (results.size () != 1) {
            qCritical() << "Unable to get Feed message infromation for uri : "<< uri ;
        }
        else
        {
            primaryText = results.at(0).at(0).toString() ; //nie:title

            if (results.at(0).at(0).isValid()) itemProperties.insert ("nie:title", results.at(0).at(0));
            if (results.at(0).at(2).isValid()) itemProperties.insert ("nie:isLogicalPartOf", results.at(0).at(2));
            if (results.at(0).at(2).isValid()) itemProperties.insert ("nie:contentCreated", results.at(0).at(2));
        }
    }
    else if (classUri == MusicAlbumType)
    {
        QString moreInfoQuery = QString ("SELECT nmm:albumTitle(<%1>) tracker:id(<%1>) nmm:albumArtist(<%1>) nmm:genre(<%1>)"
                                         " GROUP_CONCAT(nao:prefLabel(nao:hasTag(<%1>)), \" \") WHERE { }").arg (uri) ;
        results = m_tracker->rawSparqlQuerySync (moreInfoQuery);

        if (results.size () != 1) {
            qCritical() << "Unable to get Album infromation for uri : "<< uri ;
        }
        else
        {
            primaryText = results.at(0).at(0).toString() ; // nmm:albumTitle
            //NOTE : Experimental code for grouping the abums, 
            //needs to review the grouping logic.
            grouping = isGroup = true;
            groupUris.append (uri);
            groupPrimaryLabels.append (primaryText) ;

            if (results.at(0).at(0).isValid()) itemProperties.insert ("nmm:albumTitle", results.at(0).at(0));
            if (results.at(0).at(1).isValid()) itemProperties.insert ("nmm:albumArtist", results.at(0).at(1));

            fts_field_index = 2;
        }
    }
    else if (classUri == MusicPieceType)
    {
        QString moreInfoQuery = QString ("SELECT ") //+ s_favorite_tag_query
            + " nie:title(<%1>) nmm:artistName(nmm:performer(<%1>)) nmm:albumTitle(nmm:musicAlbum(<%1>))"
            + " nfo:duration(<%1>) nmm:runTime(<%1>) nfo:fileName(<%1>) nfo:genre(<%1>)"
            + " GROUP_CONCAT(nao:prefLabel(nao:hasTag(<%1>)), \",\") nmm:musicAlbum(<%1>) nmm:performer(<%1>) WHERE { }" ;

        moreInfoQuery = moreInfoQuery.arg (uri) ;

        results = m_tracker->rawSparqlQuerySync (moreInfoQuery);

        if (results.size () != 1) {
            qCritical() << "Unable to get music piece information for uri : "<< uri << endl << moreInfoQuery;
        }
        else
        {
            primaryText = results.at(0).at(0).toString() ; // Title of the Song - nie:title
            if (primaryText.isEmpty()) primaryText = results.at(0).at(5).toString() ; //nfo:fileName
			QString artistName = results.at(0).at(1).toString();
            QString albumTitle = results.at(0).at(2).toString();

            // run time in seconds
            int duration = results.at(0).at(3).toInt();
            int runTime =  results.at(0).at(4).toInt();

            // if it's partially watched, show the remaing times
            if (runTime < duration) duration -= runTime;

            if (results.at(0).at(0).isValid()) itemProperties.insert ("nie:title", results.at(0).at(0));
            if (results.at(0).at(1).isValid()) itemProperties.insert ("nmm:artistName", results.at(0).at(1));
            if (results.at(0).at(2).isValid()) itemProperties.insert ("nmm:albumTitle", results.at(0).at(2));
            if (results.at(0).at(5).isValid()) itemProperties.insert ("nfo:fileName", results.at(0).at(5));
            itemProperties.insert ("nfo:duration", duration);
            itemProperties.insert ("nmm:runTime", runTime);

            if (!results.at(0).at(8).toUrl().isEmpty()){
                groupUris.append (results.at(0).at(8).toUrl().toEncoded()); //Album id
                groupPrimaryLabels.append (albumTitle); // Album name
            }
            if (!results.at(0).at(9).toUrl().isEmpty()) {
                groupUris.append (results.at(0).at(9).toUrl().toEncoded()); //Artist id
                groupPrimaryLabels.append (artistName); // Artist name
            }

            grouping = groupUris.size();

            fts_field_index = 5;
        }
    }
    else if (classUri == ArtistType)
    {
        QString moreInfoQuery = QString ("SELECT nmm:artistName(<%1>) WHERE { }").arg(uri);
        results = m_tracker->rawSparqlQuerySync (moreInfoQuery);

        if (results.size () != 1) {
            qCritical() << "Unable to get artist information for uri : "<< uri ;
        }
        else
        {
            primaryText = results.at(0).at(0).toString() ; // nmm:artistName
            if (results.at(0).at(0).isValid()) itemProperties.insert ("nmm:artistName", results.at(0).at(0));

			grouping = isGroup = true;
			groupPrimaryLabels.append (results.at(0).at(0).toString()) ; // nmm:artistName

        }
    }
    else if (classUri == VideoType)
    {
        QString moreInfoQuery = QString ("SELECT  tracker:coalesce (nie:title(<%1>), nfo:fileName(<%1>)) nie:description(<%1>)"
                                         " nmm:runTime(<%1>) nfo:duration(<%1>) nie:url(<%1>)"
                                         " nfo:genre(<%1>) GROUP_CONCAT(nao:prefLabel(nao:hasTag(<%1>)), \",\") WHERE { }").arg(uri);
        results = m_tracker->rawSparqlQuerySync (moreInfoQuery);

        if (results.size () != 1) {
            qCritical() << "Unable to get video information for uri : "<< uri << moreInfoQuery;
        }
        else
        {
            primaryText = results.at(0).at(0).toString() ; // nfo:fileName

            int runTime = results.at(0).at(2).toInt();
            int duration = results.at(0).at(3).toInt();
            if (runTime && (runTime < duration)) duration -= runTime;

            if (results.at(0).at(0).isValid()) itemProperties.insert ("nie:title", results.at(0).at(0));
            if (results.at(0).at(1).isValid()) itemProperties.insert ("nie:description", results.at(0).at(1));
            if (results.at(0).at(4).isValid()) itemProperties.insert ("nie:url", results.at(0).at(4).toUrl().toEncoded());
            itemProperties.insert ("nmm:runTime", runTime);
            itemProperties.insert ("nfo:duration", duration);
        }
        fts_field_index = 5;
    }
    else if (classUri == RadioStationType)
    {
        QString moreInfoQuery = QString ("SELECT nie:title(<%1>) nmm:frequency(<%1>) WHERE { }").arg(uri);
        results = m_tracker->rawSparqlQuerySync (moreInfoQuery);

        if (results.size () != 1) {
            qCritical() << "Unable to get radio station information for uri : "<< uri ;
        }
        else
        {
            primaryText = results.at(0).at(0).toString() ; // nie:title

            if (results.at(0).at(0).isValid()) itemProperties.insert ("nie:title", results.at(0).at(0));
            if (results.at(0).at(1).isValid()) itemProperties.insert ("nmm:frequency", results.at(0).at(1));
        }
    }
    else if (classUri == FolderType)
    {
        QString moreInfoQuery = QString ("SELECT nfo:fileName(<%1>) nie:url(<%1>) WHERE { }").arg(uri);
        results = m_tracker->rawSparqlQuerySync (moreInfoQuery);

        if (results.size () != 1) {
            qCritical() << "Unable to get folder information for uri : "<< uri ;
        }
        else
        {
            primaryText = results.at(0).at(0).toString() ; // nie:url
            if (primaryText.isEmpty()) primaryText = results.at(0).at(1).toUrl().toEncoded();
            else fts_field_index = 1;

            if (results.at(0).at(0).isValid()) itemProperties.insert ("nfo:fileName", results.at(0).at(0));
            if (results.at(0).at(1).isValid()) itemProperties.insert ("nie:url", results.at(0).at(1));
        }
    }
    else
    {
        //qCritical() << "Unhandled type" <<classUri;
    }

    // append all searchable fields
    if (fts_field_index && results.size() == 1)
    { 
        for (int i=fts_field_index; i<results.at(0).size(); i++)
            if (!results.at(0).at(i).toString().isEmpty())
                ftsText.append (results.at(0).at(i).toString() + " ") ;

        //qDebug() << classUri << ":" << "fts :" << ftsText;
    }

    itemProperties.insert( "defaultLabel",      primaryText         );
    itemProperties.insert( "ftsText",           ftsText             );

    if(grouping) {
        if(isGroup) itemProperties.insert( "isGroup", true);
        itemProperties.insert( "groupUris", groupUris );
        itemProperties.insert( "groupPrimaryLabels", groupPrimaryLabels );
    }

    return itemProperties;
}

void TrackerHarvester::resetTimer() {
    if (m_queueTimer != NULL) {
        if ( m_queueTimer->isActive() ) {
            m_queueTimer->stop();
        }
        m_queueTimer->start();
    }
}

QVector<QStringList> TrackerHarvester::convertActions(const QStringList &identifiers)
{
    QList<ContentAction::Action> actions = ContentAction::Action::actions(identifiers);
    QVector<QStringList> actionsList ;

    for(int i=0;i<actions.size();i++) {
        QStringList action ;
        action.append (actions.at(i).localizedName());
        action.append (actions.at(i).name());
        action.append (actions.at(i).localizedName());

        actionsList.append (action) ;
    }

    return actionsList ;
}

QVector<QStringList> TrackerHarvester::convertActionsForMime(const QString &identifier)
{
    QList<ContentAction::Action> actions = ContentAction::actionsForMime(identifier);
    QVector<QStringList> actionsList ;
    
    for(int i=0;i<actions.size();i++) {
        QStringList action ;
        action.append (actions.at(i).localizedName());
        action.append (actions.at(i).name());
        action.append (actions.at(i).localizedName());

        actionsList.append (action) ;
    }
    
    return actionsList ;
}

bool TrackerHarvester::executeItems(const QStringList &items, const QString &action)
{
    if (items.size() == 0) return false;
    QList<ContentAction::Action> actions = ContentAction::Action::actions(items);

    // validate actions
    if (actions.size () == 0) return false;

    // If no action passed assume first in the list as default
    if (action.isEmpty()) {
        if (actions.size()) actions.at(0).trigger();
        return true;
    }
    
    for(int i=0;i<actions.size(); i++) {
        if (actions.at(i).localizedName() == action) {
            actions.at(i).trigger();
            return true;
        }
    }

    return false;
}
