/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <QSparqlConnection>
#include <QSparqlResult>
#include <QSparqlError>
#include <QStringList>

#include <QDebug>

#include "TrackerDBus.h"
#include "TrackerDBusResult.h"

// constructor
TrackerDBus::TrackerDBus( const QString &connectionType, const QString &watchFor, QObject* parent) : 
    QObject(parent), m_trackerConnection(new QSparqlConnection(connectionType.isEmpty() ? "QTRACKER" : connectionType))
    ,m_watcher(0)
{
    qDebug () << "[TrackerDBus] : drivers available :" << m_trackerConnection->drivers() ;
    if(!watchFor.isEmpty()){
        m_watcher = new TrackerChangeNotifier(watchFor);

        qCritical () << "[TrackerDbus] : Listening changes on :" << watchFor ;        
        //listen for changes
        connect (m_watcher, SIGNAL(changed(QList<TrackerChangeNotifier::Quad>, QList<TrackerChangeNotifier::Quad>)),
             this, SLOT(graphUpdatedReceived(QList<TrackerChangeNotifier::Quad>, QList<TrackerChangeNotifier::Quad>)));
    }
}

// destructor
TrackerDBus::~TrackerDBus() {
    if (m_trackerConnection) delete m_trackerConnection;
    if (m_watcher) delete m_watcher;
}

// raw sparql sync query using timeout
QVector<QVariantList> TrackerDBus::rawSparqlQuerySync(const QString & query) {

    QVector<QVariantList> result;

    if (!m_trackerConnection) {
        qWarning () << "TrackerDBus::rawSparqlQuery(): no Tracker connection opened";
        return result ;
    }
    
    QSparqlQuery q(query); 
    QSparqlResult *reply = m_trackerConnection->exec (q);

    if (!reply) {
        qCritical() << "[TrackerDBus] NULL Results form tracker connection " ;
        return result;
    }
    
    reply->waitForFinished();

    if (reply->hasError()) {
        qCritical() << "[TrackerDBus] Error :" << reply->lastError().message();
        qCritical() << "query was :" << query ;
        delete reply;
        return result;
    }

    while (reply->next()) {
        QVariantList list ;
        QSparqlResultRow row = reply->current();

        for (int i=0; i<row.count(); i++){
            list.append(row.value(i));
        }

        result.append(list);
    }

    //clean up
    delete reply;

    return result;
}

// raw sparql sync update using timeout
bool TrackerDBus::rawSparqlUpdateSync(const QString & query) {

    if (!m_trackerConnection) {
        qCritical () << "TrackerDBus::rawSparqlQuery(): no Tracker connection opened";
        return false;
    }

    QSparqlQuery q(query); 
    QSparqlResult *reply = m_trackerConnection->syncExec (q);

    if (!reply) {
        qCritical() << "[TrackerDBus] NULL Results form tracker connection " ;
        return false;
    }

    reply->waitForFinished();

    if (reply->hasError()) {
        qCritical() << "[TrackerDBus] Error :" << reply->lastError().message();
        qCritical() << "query was :" << query ;
        delete reply;
        return false;
    }

    //clean up
    delete reply;

    // success
    return true;
}

// raw sparql async query
TrackerDBusResult * TrackerDBus::rawSparqlQueryAsync (const QString & query) {
    QVector<QVariantList> result;

    if (!m_trackerConnection) {
        qCritical () << "TrackerDBus::rawSparqlQuery(): no Tracker connection opened";
        return 0;
    }
    
    QSparqlQuery q(query); 
    QSparqlResult *reply = m_trackerConnection->exec (q);

    if (!reply) {
        qCritical() << "[TrackerDBus] NULL Results form tracker connection " ;
        return 0;
    }

    TrackerDBusResult *newresult = new TrackerDBusResult();
    connect (reply, SIGNAL (finished ()), newresult, SLOT (trackerResultReceived ()));

    return newresult;
}


// SLOTS

void TrackerDBus::graphUpdatedReceived (QList<TrackerChangeNotifier::Quad> deletes, const QList<TrackerChangeNotifier::Quad> inserts) {
    QVector<QList<int> > insertList , deleteList ;

    qCritical() << "Deletes : " << deletes.size() ; 
    for (int i=0; i<deletes.size(); i++){
        QList<int> list ;
        //qCritical() << deletes.at(i).graph << ":" << deletes.at(i).subject << ":" << deletes.at(i).predicate << ":" << deletes.at(i).object ;
        
        list << deletes.at(i).graph << deletes.at(i).subject << deletes.at(i).predicate << deletes.at(i).object ;
        deleteList << list ;
    }
    
    qCritical() << "Inserts :"<< inserts.size() ;
    for (int i=0; i<inserts.size(); i++) {
        QList<int> list ;
        
        //qCritical() << inserts.at(i).graph << ":" << inserts.at(i).subject << ":" << inserts.at(i).predicate << ":" << inserts.at(i).object ;
      
        list << inserts.at(i).graph << inserts.at(i).subject << inserts.at(i).predicate << inserts.at(i).object ;
        insertList << list;
    }

    emit graphUpdated(m_watcher->watchedClass(), deleteList, insertList);
}

// EOF
