/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef RELEVANCESTORAGESERVICE_H
#define RELEVANCESTORAGESERVICE_H

#include <QObject>
#include <QStringList>
#include <QDebug>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QDateTime>
#include <QVariant>
#include <QTimer>
#include <QCoreApplication>
#include <QMap>
#include <QHash>
#include <QDateTime>

#include "ActivityCounter.h"

class AbstractHarvester;

/**
 * \brief The service provider to lauch the activity tracker
 *
 * This class provides methods for accessing the activity database.
 * Only the public slots are exposed via the service (dbus) API,
 * the other methods are only accessible by directly linking to the class. It is foreseen
 * that more methods may be moved to the service API, hence the Q_NOREPLY marker has been
 * used where appropriate even though it has no meaning when a method is not exposed via DBus.
 *
 * The data model is based on the ZeitGeist framework (https://launchpad.net/zeitgeist).
 * Activities ("Events" in Zeitgeist) are represented as QVariantMaps. The keys are all
 * URIs (QStrings), the values are always QStrings (QStringLists are also supported,
 * but it is only a convenience and has the same semantics as storing multiple values
 * for the same URI in the QVariantMap).
 *
 * In difference to Zeitgeist data model, metadata for content items
 * (e.g. tags related to an image) are not stored in the activity database. They
 * should be stored in the tracker.
 *
 * Note that the above is not visible to service users in the current simplified service API,
 * which only allows a subject URI to be reported (with defaults used for all other information).
 */

class RelevanceStorageService: public QObject
{
    Q_OBJECT


public:
    /**
     * \brief Constructor
     */
    RelevanceStorageService(QObject* parent = NULL);

    /**
     * \brief Destructor
     * Simply calls close().
     */
    ~RelevanceStorageService();

    /**
     * \brief Initialize the service.
     *
     * Opens the database, creates tables if not present, and loads total counts and ontology tables to memory.
     *
     * \param dbFile the name of database file to open. ":memory:" can be used for testing purposes.
     * \param idleTimeout the timeout (in seconds) after which the service shuts down.
     * \return 0 if successful, -1 if failed.
     */
    int initialize(const QString &dbFile);

    /**
     * \brief Adds new item classes.
     */
    bool registerClasses( QVector<QStringList> );

    //FIXME: Make next two private, use friend in ifdefs (like RelevanceDaemon)

     /**
     * \brief Access database object
     *
     * \deprecated Provides access to internal database object for testing purposes. This method will likely be removed in the future.
     *
     * \param dbFile the name of database file to open. ":memory:" can be used for testing purposes.
     * \param idleTimeout the timeout (in seconds) after which the service shuts down.
     */
    inline QSqlDatabase* contentDb() { return &m_contentDb; }

    /**
     * \brief Close database
     *
     * This method allows the object to be initialized again. Useful for unit testing purposes.
     */
    void close();

    // Dump db to stdout
    int dump() { dump (m_contentDb); dump (m_userDb); return 0;}


    //FIXME: Note that ActivityURI is not actually globally unique (only within a single host)

    /**
     * \brief Add a new activity.
     *
     * This is a convenience method which creates the QVariantMap from the parameters and calls the overloaded function.
     * Use empty string (-1 for the duration) to omit a value from the QVariantMap.
     *
     * \param subject value for RelevanceStorage_SUBJECT key
     * \param application value for RelevanceStorage_APPLICATION key
     * \param type value for RelevanceStorage_TYPE key
     * \param timestamp value for RelevanceStorage_TIMESTAMP key
     * \param duration value for RelevanceStorage_DURATION key
     * \param source value for RelevanceStorage_SOURCE key
     */
    void addActivity(const QString &subject, const QString &application, const QString &type,
                               const QString &timestamp, int duration, const QString &source);

    /**
     * \brief Add a new activity.
     *
     * Inserts a new activity record into the database.
     * The activity is represented as a QVariantMap. The keys are all
     * URIs (QStrings), the values are always QStrings (QStringLists are also supported,
     * but it is only a convenience and has the same semantics as storing multiple values
     * for the same URI in the QVariantMap).
     *
     * Some uris have special semantics, see RelevanceStorageDefines.h:
     * @li RELEVANCE_STORAGE_SUBJECT - The URI of the item that the was accessed
     * @li RELEVANCE_STORAGE_TIMESTAMP - The time when the access happened (using yyyy-dd-mmThh:mm:ss format)
     * @li RELEVANCE_STORAGE_DURATION - The duration of the activity in seconds (interpreted as an integer, 0 = instantaneous)
     * @li RELEVANCE_STORAGE_APPLICATION - The application that handled the access (full path of .desktop file where applicable)
     * @li RELEVANCE_STORAGE_SOURCE - The source of the activity information, e.g. user activity, web history, heuristic activity, etc.
     * @li RELEVANCE_STORAGE_TYPE - The type of the activity, e.g. create, modify, visit, etc.
     *
     * However, any URI can be used as a key. URIs that the system does not recognize are simply stored as freeform annotations.
     * Of course, they are only useful to applications which know the semantics of the used URI.
     *
     * Of the above URIs, only RELEVANCE_STORAGE_SUBJECT must be present. Defaults will be used for the others if not present, as follows:
     * @li RELEVANCE_STORAGE_TIMESTAMP - Current time minus duration, or current time if duration is not specified
     * @li RELEVANCE_STORAGE_DURATION - Unknown (-1)
     * @li RELEVANCE_STORAGE_APPLICATION - Unknown (empty string)
     * @li RELEVANCE_STORAGE_SOURCE - RelevanceStorage_SOURCE_USERACTIVITY
     * @li RELEVANCE_STORAGE_TYPE - RelevanceStorage_TYPE_VISIT
     */
    void addActivity(QVariantMap activity);

    /**
     * \brief Add new activities.
     *
     * This is a more efficient way to add multiple activities than calling addActivity() several times.
     * In particular, it uses a single transaction for storing the all the activities, whereas using
     * addActivity multiple times would use a separate transaction for each invocation.
     *
     * \param activities A list of QVariantMap objects, each corresponding to a single activity
     */
    void addActivities(QVariantList activities);

    void addActivities(QStringList subjectUris);

    /**
     * \brief Add a new activity.
     *
     * This is a convenience method which creates the QVariantMap from the parameters and calls the overloaded function.
     * Use empty string (-1 for the duration) to omit a value from the QVariantMap.
     *
     * \param subject value for RelevanceStorage_SUBJECT key
     * \param application value for RelevanceStorage_APPLICATION key
     * \param type value for RelevanceStorage_TYPE key
     * \param timestamp value for RelevanceStorage_TIMESTAMP key
     * \param duration value for RelevanceStorage_DURATION key
     * \param source value for RelevanceStorage_SOURCE key
     * \returns Either the identifier URI of the activity record, or an "uri" of the form "error:..."
     */
    QString addActivitySynchronously(const QString &subject, const QString &application, const QString &type,
                                     const QString &timestamp, int duration, const QString &source);

    /**
     * \brief Add a new activity.
     *
     * Inserts a new activity record into the database.
     * The activity is represented as a QVariantMap. The keys are all
     * URIs (QStrings), the values are always QStrings (QStringLists are also supported,
     * but it is only a convenience and has the same semantics as storing multiple values
     * for the same URI in the QVariantMap).
     *
     * Some uris have special semantics, see RelevanceStorageDefines.h:
     * @li RELEVANCE_STORAGE_SUBJECT - The URI of the item that the was accessed
     * @li RELEVANCE_STORAGE_TIMESTAMP - The time when the access happened (using yyyy-dd-mmThh:mm:ss format)
     * @li RELEVANCE_STORAGE_DURATION - The duration of the activity in seconds (interpreted as an integer, 0 = instantaneous)
     * @li RELEVANCE_STORAGE_APPLICATION - The application that handled the access (full path of .desktop file where applicable)
     * @li RELEVANCE_STORAGE_SOURCE - The source of the activity information, e.g. user activity, web history, heuristic activity, etc.
     * @li RELEVANCE_STORAGE_TYPE - The type of the activity, e.g. create, modify, visit, etc.
     *
     * However, any URI can be used as a key. URIs that the system does not recognize are simply stored as freeform annotations.
     * Of course, they are only useful to applications which know the semantics of the used URI.
     *
     * Of the above URIs, only RELEVANCE_STORAGE_SUBJECT must be present. Defaults will be used for the others if not present, as follows:
     * @li RELEVANCE_STORAGE_TIMESTAMP - Current time minus duration, or current time if duration is not specified
     * @li RELEVANCE_STORAGE_DURATION - Unknown (-1)
     * @li RELEVANCE_STORAGE_APPLICATION - Unknown (empty string)
     * @li RELEVANCE_STORAGE_SOURCE - RelevanceStorage_SOURCE_USERACTIVITY
     * @li RELEVANCE_STORAGE_TYPE - RelevanceStorage_TYPE_VISIT
     * \returns Either the identifier URI of the activity record, or an "URI" of the form "error:..."
     */
    QString addActivitySynchronously(QVariantMap activity);


    /**
     * \brief Add new activities.
     *
     * This is a more efficient way to add multiple activities than calling addActivity() several times.
     * In particular, it uses a single transaction for storing the all the activities, whereas using
     * addActivity multiple times would use a separate transaction for each invocation.
     *
     * \param activities A list of QVariantMap objects, each corresponding to a single activity
     * \returns Either a single string of type "error:...", or a string for each activity (in corresponding order) that is either
     * the identifier URI of the created activity or an "URI" of the form "error:..."
     */
    QStringList addActivitiesSynchronously(QVariantList activities);    


    /**
     * \brief Store item information to the database.
     *
     * This method should be called when tracker signals that a new item
     * has been created or modified.
     * \pre URI must not be present in database
     * \returns the integer code assigned for the subject uri, or -1 if failed
     */
    int addSubject(const QString &uri, const QString &classUri, int linkCount, const QString &addTime, AbstractHarvester* harvester );

    /**
     * \brief Load item information from tracker and store it to the database.
     *
     * This method is implicitly called whenever uri2id does not find
     * an existing database entry.
     * \pre The uri must not be already present in the database
     * \returns the integer code assigned for the subject uri, or -1 if failed
     */
    int addSubject(const QString &uri, AbstractHarvester* harvester );


    /**
     * \brief Remove item from database
     */
    int removeSubject(const QString &uri);

    /**
     * \brief Remove subject information from database.
     *
     * While subjects are added implicitly, removal must be done explicitly, typically as a response
     * to an item deletion event from tracker.
     * \returns true if the item was successfully removed or did not exist in the first place
     */
    //bool removeSubject(QString uri);

    /**
     * \brief Retrieve stored activity information for a specific activity record.
     *
     * This method retrieves the activity record corresponding to the identity URI given as parameter.
     *
     * In addition to the stored information, the record will have a new key RelevanceStorage_URI which contains the identity URI.
     *
     * \param activityUri the identity URI of the activity record
     * \returns A QVariantMap containing the activity information, or a map with RelevanceStorage_URI of type "error:..." if not found.
     */
    QVariantMap activity(const QString &activityUri);

    /**
     * \brief Retrieve stored activity information for a specific content item.
     *
     * This method retrieves the activity records corresponding to the subject URI given as parameter.
     *
     * In addition to the stored information, the records will have a new key RelevanceStorage_URI
     * which contains the identity URI for the record.
     *
     * \param subjectUri the subject URI that the activity records should match
     * \returns A list of QVariantMap objects containing the activity information.
     * The list is empty if the subject URI did not match any activities.
     */
    QVariantList activitiesFor(const QString &subjectUri);

    /**
     * \brief Retrieve all stored activity information
     *
     * This method retrieves all activity records in the database.
     *
     * In addition to the stored information, the records will have a new key RelevanceStorage_URI
     * which contains the identity URI for the record.
     *
     * \returns A list of QVariantMap objects containing the activity information.
     * The list may be empty if the there are no activities in the database.
     */
    QVariantList allActivities();

    /**
     * \brief Count how many times an uri has been accessed
     *
     * \param uri the uri for which access should be counted
     * \param mode name of the counting algorithm
     * \return A QVariantList containing the following: { uri, count, "Total", totalCount }
     * where uri and "Total" are of type QString and count and totalCount are of type double.
     */
    QVariantList activityCount(const QString &subjectUri, const QString &mode);

    /**
     * \brief Count how many times uris in the database have been accessed
     * \param mode name of the counting algorithm
     * \return A QVariantList containing the following: { uri1, count1, ..., uriN, countN, "Total", totalCount }
     * where uriX and "Total" are of type QString and countX and totalCount are of type double.
     */
    QVariantList activityCounts(const QString &mode);

    /**
     * \brief Count the sum of all access frequencies.
     *
     * This method simply returns just the last value in the list that activityCount() and activityCounts() return.
     * Note that the sum is cached so this method is very fast.
     *
     * \param mode name of the counting algorithm
     * \return The total count for all accesses
     */
    double activityCountTotal(const QString &mode);

    /**
     * \brief Return a list of available counting modes.
     *
     * \return The available counting modes.
     */
    QStringList activityCountModes();

    // Mark current time as last update received from tracker
    bool markUpdate();
    // Mark given time as last update received from tracker
    bool markUpdate(QDateTime time);
    // Get timestamp of last markUpdate as xsd:date string
    QString lastUpdate();
    // Load class and property information from DB
    bool loadStaticData();

    // Update from tracker at startup
    bool updateRecentChanges( QStringList changes, AbstractHarvester* harvester );

    /**
     * \brief Begin a transaction
     *
     */
    bool beginTransaction(QSqlDatabase db = QSqlDatabase::database());

    //FIXME: Check Sqlite docs, can abort really fail?

    /**
     * \brief Commit a transaction.
     *
     * Retry 10 times if necessary. This may happen with concurrent access to DB,
     * but nobody <b>should<b/> be accessing it except us.
     */
    bool commitTransaction(QSqlDatabase db = QSqlDatabase::database());

    /**
     * \brief Abort a transaction
     *
     * Retry 10 times if necessary. This may happen with concurrent access to DB,
     * but nobody <b>should<b/> be accessing it except us.
     */
    bool abortTransaction(QSqlDatabase db = QSqlDatabase::database());


public slots:

    // Simple DBus API

    /**
     * \brief Report a user activity that accessed a content item.
     *
     * This is a convenience method which invokes addActivity(subjectUri, "", "", "", -1, "");
     * The method is asynchronous, so the control will return to client immediately after method invocation.
     * There is no possibility to detect failure.
     *
     * \param subjectUri uri of the item that the user accessed
     */

    void addActivity(const QString &subjectUri);

    /**
     * \brief Synchronously report a user activity that accessed a content item.
     *
     * This is a convenience method which invokes addActivity(subjectUri, "", "", "", -1, "");
     * The method is synchronous, so the control will return to client only after the data has been written to disk.
     *
     * \param subjectUri uri of the item that the user accessed
     * \return True if the data was successfully committed to database, false if not.
     */

    bool addActivitySynchronously(const QString &subjectUri);


    /**
     * \brief Count how many times an uri has been accessed
     *
     * This is a convenince method which invokes count(uri, mode) with a default mode.
     *
     * \param uri the uri for which access should be counted
     * \return A QVariantList containing the following: { uri, count, "Total", totalCount }
     * where uri and "Total" are of type QString and count and totalCount are of type double.
     */
    QVariantList activityCount(const QString &subjectUri);

    /**
     * \brief Count how many times uris in the database have been accessed
     *
     * This is a convenince method which invokes countAll(mode) with a default mode.
     *
     * \return A QVariantList containing the following: { uri1, count1, ..., uriN, countN, "Total", totalCount }
     * where uriX and "Total" are of type QString and countX and totalCount are of type double.
     */
    QVariantList activityCounts();

    /**
     * \brief Cleanup database
     *
     * Removes activity records older than one month, and counts which are older than a year.
     *
     * This is also invoked randomly when the activities are added to the service, once in a hundred invocations, and with a one-second delay. It is expected that at least ten
     * invocations per day will be encountered, so that cleanup will on average be performed at least once per 10 days.
     *
     * \return 0 if successful, -1 if failed.
     */
    int cleanup();

    /**
     * \brief Search given text
     *
     * Searches the subjects table for given text
     *
     * \return array of stringlist which contains class_id, uri, primary_text, secondary_text, tertiary_text of matched
     * records
     */
    QVector<QVariantMap> doSearch (const QString &text, const QStringList &content_types, const QStringList& content_sources, bool grouping_flag) const ;
 
    QVector<QVariantMap> doSearch (const QString& groupIdentifier, const QString& text, const QStringList& content_types,
                                   const QStringList& content_sources, bool grouping_flag);
        /**
     * \brief Search given uris 
     *
     * Searches the subjects table for given uris
     *
     * \return array of stringlist which contains class_id, uri, primary_text, secondary_text, tertiary_text of matched
     * records
     */
    QVector<QVariantMap> doSearch (const QStringList &uris, const QStringList &content_types, const QStringList& content_sources) const ;

    void dataUpdated(const QSet<QString> &added, const QSet<QString> &removed, const QSet<QString> &changed, const QDateTime &lastUpdate);
    
signals:

    /**
     * \brief A signal that is emitted whenever a new activity has been added.
     *
     * This signal is meant to be used by relevance calculation so that it can immediately
     * react to changes in item access frequences.
     *
     * \param subject subject of the activity
     */
    void activityAdded(const QString &subjectUri);

    void storageDataUpdated(QStringList &uriList);

private:

     /**
     * \brief Cleanup: Recalculate total counts.
     *
     * Ensures that total counts are in sync with URI counts after removing old URI counts.
     */
    bool recalculateTotals();

     /**
     * \brief Cleanup: Remove old activities and their annotations from database.
     *
     * Removes activities for whom timestamp is older than the threshold value (one month)
     * Annotations referring to removed activities are also removed.
     */
    bool removeOldActivities();

     /**
     * \brief Cleanup: Remove old counts and the corresponding uri mapping from database.
     *
     * Removes counts for whom timestamp is older than the threshold value (one year)
     * Uri mapping for the count is also removed. Since an added activity always updates
     * the timestamp of a count, and activities are removed earlier than counts,
     * there cannot be any activities using the removed uri.
     */
    bool removeOldCounts();

    /**
     * \brief Fetch activities related to subjectUri from database.
     *
     * Returns all activities if subjectUri is an empty string.
     * This method is used by activitiesFor() and allActivies() methods.
     *
     * \param subjectUri the subject URI that the activity records should match, if empty string the method will return all activities
     * \returns A list of QVariantMap objects containing the activity information.
     * The list is empty if the subject URI did not match any activities.
     */
    QVariantList fetchActivitiesFor(const QString &subjectUri);


    /**
     * \brief Write a single activity to database.
     *
     * This method does the database operations but transactions are done by the caller. It is used by addActivity and
     * addActivities to optimize the transaction usage for each methods.
     *
     * \param activity the activity to store
     * \returns the identity uri of the activity or "error:..."
     */
    QString writeActivityToDB(QVariantMap &activity);

    /**
     * \brief Add a new counter algorithm to the list of known algorithms.
     *
     * \param counter the algorithm to be added
     */
    void addCounter(ActivityCounter* counter);

    /**
     * \brief Creation of counter objects
     *
     * When a new counter algorithm is taken into use, the creation of the corresponding counter object
     * and the addCounter() invocation is added to this method.
     */
    void initializeCounters();

    /**
     * \brief Update counters when a new activity has been added.
     *
     * \param uri the integer value corresponding to the activity identity URI
     * \param now the writing timestamp of the activity (not the same as the timestamp of the activity)
     * \param activityTime the timestamp of the activity
     * \param activity the activity data
     */
    QString updateCounts(int uri, QDateTime now, QDateTime activityTime, QVariantMap& activity);

    /**
     * \brief Assign an integer code for a subject URI using a mapping table in the database.
     *
     * \param uri the subject URI
     * \returns an integer code assigned for the subject URI
     */
    int uri2id(const QString &uri);

    /**
     * \brief returns the current time stamp of the record in our database.
     *
     * \param uri the subject URI
     * \returns time stamp for the subject URI if valid, else 0.
     */
    unsigned int timeStamp(const QString &uri);

    /**
     * \brief Convert a subject URI integer code to its string counterpart.
     *
     * \param id subject URI integer code
     * \returns corresponding string URI
     */
    QString id2uri(int id);

	/**
     * \brief Convert a group URI to its equalent integer code.
     *
     * \param URI
     * \returns corresponding integer code
     */
    int groupUri2id (const QString &uri);

	/**
     * \brief Convert a group integer code to its equalent URI.
     *
     * \param id of the group
     * \returns corresponding string URI
     */
    QString groupId2uri (int id);

	/**
     * \brief Add new group to the Groups table
     *
     * \param URI of the group
     * \param class uri of the group
     * \param Readable name of the group
     * \returns equalent integer code on success, 0 on failure
     */
    int addGroup (const QString &uri, const QString &classUri, const QString &label);

    /**
     * \brief Assign an integer code for an ontology URI using a mapping table in the database.
     *
     * Note, the whole mapping table is cached in memory
     *
     * \param uri the ontology uri to map
     * \returns the integer code assigned to the uri
     */
    int ontology2code(const QString &uri);

    /**
     * \brief Convert an ontology URI ïnteger code to its string counterpart.
     *
     * Note, the whole mapping table is cached in memory so this is always fast.
     *
     * \param id ontology URI integer code
     * \returns corresponding string URI
     */
    QString code2ontology(int code);


    /**
     * \brief Escape single quotes
     *
     * \param s the string to escape
     * \returns the string with single quotes escaped
     */
    QString sqlString(QString s);

    /**
     * \brief Load annotations from the database
     *
     * \param activityId the integer corresponding to the activity id URI
     * \param activity where to add the annotations
     */
    void fillInAnnotations(int activityId, QVariantMap& activity);

    /**
     * \brief Randomly invoke cleanup (or not)
     */
    void randomCleanup();


    int recSubClassDepth(const QHash<QString, QString> & classHierarchy, const QString & subClass);

    /**
     * \brief Generic method to query the storage database
     *
     * \param sql query to execute
     * \returns result of the query in the form of array of string list, each row in the
     * array contains fiels queried on the database.
     */
    QVector<QVariantMap> query (const QString &query) const ;

    int initializeUserDatabase (const QString &dbFile) ;
    int initializeContentDatabase (const QString &dbFile) ;
    int dump (QSqlDatabase &db);
    
    //! database where activities are persisted
    QSqlDatabase m_contentDb;

    QSqlDatabase m_userDb;
    //! counter algorithms
    QMap<QString, ActivityCounter*> m_counters;
    //! total counts for counter algorithms
    QMap<QString, double> m_totals;
    //! the time when the total counts for counter algorithms where last calculated
    uint m_totalsTimestamp;
    //! last update time
    uint m_lastUpdate;
    //! lookup table for ontology codes (string -> int)
    QHash<QString,int> m_ontologyCodes;
    //! lookup table for ontology codes (int -> string)
    QHash<int, QString> m_ontologyUris;
    QHash<QString, int> m_classUris;
    //! Cache used to detect duplicate activity registrations
    QHash<QString, uint> m_cache;
    //! Minimum time between activities for same URL (seconds)
    int m_cacheTime;

    QStringList getObjectLabelTexts(const QString &uri, const QString &classUri);

    void cleanCache();
    QSet<QString> pruneCached(QSet<QString> uriList);
};

#endif // RELEVANCESTORAGESERVICE_H
