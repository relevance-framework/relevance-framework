# Copyright (c) 2010, Nokia Corporation
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the Nokia Corporation nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

TEMPLATE = app
TARGET = relevance-framework-daemon

QT -= gui

QT += sql
CONFIG += console link_pkgconfig

HEADERS += RelevanceDaemon.h \
           RelevanceStorageService.h \
           RelevanceDaemonDefines.h \
           ActivityCounter.h \
           ExpDecayCounter.h \
           LinearDecayCounter.h \
           NoDecayCounter.h \
           RelevanceEngine.h \
           RelevanceGraph.h \
           RelevanceCalc.h \
           CalculationState.h \
           ContentStorage.h \
           ActivityPersonalization.h \
           AbstractHarvester.h \
           AbstractHarvesterFactory.h \

SOURCES = Main.cpp \
          RelevanceDaemon.cpp \
          RelevanceStorageService.cpp \
          RelevanceEngine.cpp \
          RelevanceGraph.cpp \
          RelevanceCalc.cpp \
          CalculationState.cpp \
          ContentStorage.cpp \
          ActivityPersonalization.cpp

use_dbus: {

QT += dbus
CONFIG += dbus
HEADERS += RelevanceDaemonIfAdaptor.h
SOURCES += RelevanceDaemonIfAdaptor.cpp 

} else {

QT += network
HEADERS += ServerSocket.h
SOURCES += ServerSocket.cpp

}

OBJECTS_DIR = ./.obj
MOC_DIR = ./.moc

use_dbus: {

OTHER_FILES += com.nokia.RelevanceDaemonIf.xml com.nokia.RelevanceDaemon.service RelevanceDaemonIf.h RelevanceDaemonIf.cpp

# DBus interface description XML
# We export public slots and all signals
dbusif.target = com.nokia.RelevanceDaemonIf.xml
dbusif.commands = qdbuscpp2xml \
    RelevanceDaemon.h \
    -o \
    com.nokia.RelevanceDaemonIf.xml
redoif.target = interface
redoif.depends = removeinterface \
    RelevanceDaemonIfAdaptor.h
removeif.target = removeinterface
removeif.commands = rm \
    -f \
    com.nokia.RelevanceDaemonIf.xml \
    RelevanceDaemonIfAdaptor.h \
    RelevanceDaemonIfAdaptor.cpp \
    RelevanceDaemonIf.h \
    RelevanceDaemonIf.cpp

# Generated adaptor files
adaptor.target = RelevanceDaemonIfAdaptor.h
adaptor.commands = qdbusxml2cpp \
    -a \
    RelevanceDaemonIfAdaptor \
    -c \
    RelevanceDaemonIfAdaptor \
    com.nokia.RelevanceDaemonIf.xml; \
    qdbusxml2cpp -p \
    RelevanceDaemonIf \
    -c \
    RelevanceDaemonIf \
    -i RelevanceDaemonMetaTypes.h \
    com.nokia.RelevanceDaemonIf.xml
adaptor.depends = com.nokia.RelevanceDaemonIf.xml

# service file installation
service_file.files += com.nokia.RelevanceDaemon.service
service_file.path = /usr/share/dbus-1/services/

INSTALLS += service_file

QMAKE_EXTRA_TARGETS += dbusif adaptor removeif redoif
}

headerfiles.files += RelevanceDaemonDefines.h \
                     AbstractHarvester.h \
                     AbstractHarvesterFactory.h \

headerfiles.path = /usr/include/relevancei-framework

target.path = /usr/bin

INSTALLS += target \
            headerfiles

include (../version.pri)
include (../build.pri)
