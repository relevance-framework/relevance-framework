/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef RELEVANCE_CALC_H
#define RELEVANCE_CALC_H

#include <QSet>
#include <QList>

class RelevanceGraph;

/**
 * Calculates the personalized page rank value for a RelevanceGraph and personalization vector
 */
class RelevanceCalc
{

 public:

    RelevanceCalc();

    ~RelevanceCalc();

    int iterations() const;

    void setIterations(int value);
    
    /**
     * Calculate the personalized rank vector.
     * This process now relies on bidirectional links, which means that dangling nodes have
     * always zero in and out links
     *
     * @param RelevanceGraph the RelevanceGraph
     * @param iterations amount of iterations
     * @param personalization_d a value between 0..1 - how much personalization vector is taken into account
     * @param random_d a value between 0..1 - the random jump probability
     * @param relevance array where the relevance should be stored
     * @param normalizeTo to what number to normalize the total sum of all relevance
     * @param personalization to what number to normalize the total sum of all relevance. Should be normalized to normalizeTo
     */
    void personalizedRank(RelevanceGraph* graph, 
                          double personalization_d, double random_d, double* relevance,
                          double normalizeTo, double personalization[], int *retDanglingNodeCount = 0,
                          double *retDanglingNodeInbound = 0) const;

    void personalizedRank(RelevanceGraph* graph, 
                          double personalization_d, double random_d, 
                          double normalizeTo, 
                          int *retDanglingNodeCount = 0, double *retDanglingNodeInbound = 0) const;
    
    void personalizedBlockRank(RelevanceGraph* graph, 
                               double personalization_d, double random_d, double relevance[], double normalizeTo,
                               double personalization[],  int outlinksArr[], int nodecountArr[], QSet<int> ignore, bool initializeRelevance = true) const;
    
    void simpleIncremental(RelevanceGraph* graph,
                           QList<int> items,
                           int graphNodeCount, int danglingNodeCount,
                           double personalization_d, double random_d, double normalizeTo,
                           double danglingNodeInbound = -1) const;
    
    double simpleIncremental(int graphNodeCount, int directLinkCount, int danglingNodeCount,
                             double personalization_d, double random_d, double relevance[], double normalizeTo,
                             double personalization, int linkCount[], double danglingNodeInbound = -1) const;

 private:

    //! How many iterations to use for full relevance calc
    int m_iterations;

};

#endif
