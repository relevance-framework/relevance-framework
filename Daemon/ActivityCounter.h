/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ACTIVITYCOUNTER_H
#define ACTIVITYCOUNTER_H

#include <QString>
#include <QVariant>

// Some time definitions
#define ONE_DAY_IN_SECONDS       86400
#define TWO_DAYS_IN_SECONDS     172800
#define ONE_WEEK_IN_SECONDS     604800
#define TWO_WEEKS_IN_SECONDS   1209600
#define ONE_MONTH_IN_SECONDS   2592000
#define TWO_MONTHS_IN_SECONDS  5184000
#define ONE_YEAR_IN_SECONDS   31536000


/**
 * \brief The base class for counter algorithms.
 *
 * This is an abstract class which is inherited by all counter algorithms. The algorithms are used
 * to implement different decay functions. The decay is based on the idea that a frequency score is always
 * represented in the terms of a "currency" of a specific time, and the currency loses value over time (=inflation).
 * Inflation is defined by a method which converts old currency to new currency. A score expressed in an old currency
 * is reduced (or at most kept equal) when expressed in a newer currency.
 */

class ActivityCounter
{
public:

    /**
     * \brief Defines the name of the algorithm
     *
     * The name of the algorithm is used as the mode parameter in Activity Tracker service API.
     *
     * \returns the name of the algorithm
     */
    virtual QString name() = 0;

    /**
     * \brief The score of a new activity at creation time
     *
     * Default value is 1.0. Note that this assumes creation time is the same as current time.
     * By redefining this method subclasses can put more weight on certain activities,
     * e.g. based on activity duration or content item type.
     *
     * \returns the score of a new activity at creation time
     */
    virtual double baseValue(QVariantMap& /* activity */) { return 1.0; }

//TODO: Support also case oldTime > newTime with reverse function
// This will require changes in ActivityTrackerService code too

    /**
     * \brief Convert an activity frequency score from one time to another.
     *
     * This first checks if oldTime > newTime, and returns oldValue in that case.
     * Otherwise it delegates to doConvert(), which is defined in the subclasses.
     *
     * \param oldValue the original score
     * \param oldTime the timestamp of the original score
     * \param newTime the timestamp to which the original score score should be translated
     * \returns the score in newTime currency.
     */
    inline double convert(double oldValue, uint oldTime, uint newTime) {
        if (oldTime > newTime) {
            return oldValue;
        }
        return doConvert(oldValue, oldTime, newTime);
    }

protected:
    /**
     * \brief Convert an activity frequency score from one time to another.
     *
     * This is used to implement decay of the frequecy score, so it should return
     * something that is less than oldValue. newTime is guaranteed to be greater
     * or equal to oldtime by convert().
     *
     * \param oldValue the original score
     * \param oldTime the timestamp of the original score
     * \param newTime the timestamp to which the original score score should be translated
     * \returns the score in newTime currency.
     */
    virtual double doConvert(double oldValue, uint oldTime, uint newTime) = 0;
};

#endif // ACTIVITYCOUNTER_H
