/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef RELEVANCE_GRAPH_H
#define RELEVANCE_GRAPH_H

#include <QList>
#include <QVector>
#include <QSet>

/**
 * A simple way to store the RelevanceGraph using QT containers.
 * Currently only supports unidirectional links
 */

class RelevanceGraph
{
 public:

    /**
     * FIXME: change size->ensureCapacity, and do not actually grow the graph size (first version can just ignore this parameter)
     * if size > 0 then nodes will be created without links (nodeCount == size)
     * When a edge is added to a node larger than nodeCount, the graph will be
     * filled with nodes until that edge can be created
     */
    RelevanceGraph(int size = 0, bool directed = false);

    /**
     * Destructor
     */
    ~RelevanceGraph();

    /**
     * add a edge to the RelevanceGraph
     * When a edge is added to a node larger than nodeCount, the graph will be
     * filled with nodes until that edge can be created
     */
    void addEdge(int from, int to);

    /**
     * FIXME: remove this method and addEdge should look at this->directed member to decide
     */
    void addDirectedEdge(int from, int to);

    /**
     * query if edge exists
     */
    bool hasEdge(int from, int to);

    /**
     * count outgoing edges from this node. For undirected graphs this is the size of links TO->FROM, and
     * for directed graphs, this is gotten from this->outgoingEdgeCount
     */
    int countEdges(int from);

    /**
     * get an iterator for edges coming to this node
     */
    QSetIterator<int> edgesAt(int node);

    /**
     * The count of nodes
     */
    int nodeCount();

    /**
     * The count of links
     */
    int linkCount();

    /**
     * Add a node without edges
     */
    int addNode();

    /**
     * return personalization for edge
     **/
    double personalization(int node);

    /**
     * set personalization for edge
     **/
    void setPersonalization(int node, double value);

    /**
     * return relevance value for edge
     **/
    double relevance(int node);

    /**
     * set relevance value for edge
     **/
    void setRelevance(int node, double value);

    /**
     * return modification time for edge
     **/
    uint modificationTime(int node);

    /**
     * set modification time for edge
     **/
    void setModificationTime(int node, uint value);

 private:

    QList< QSet<int>* >* m_links;
    QList<int> m_outgoingEdgeCount; // for directed graphs we need this since we record only incoming edges

    int m_linkCountNum;
    bool m_directed;

    void growIfNeeded(int expectedSize);
    void incrementOutgoingEdgeCount(int from);
    void addToVector(QSet<int>* vector, int value);

    QVector<double> m_personalization;
    QVector<double> m_relevance;
    QVector<uint> m_modificationTime;
};

#endif
