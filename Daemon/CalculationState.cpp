/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "CalculationState.h"

#include <QSqlQuery>
#include <QSqlError>
#include <QVariant>
#include <QDebug>

CalculationState::CalculationState() 
    : m_lastDanglingNodeCount(0),
      m_lastNodeCount(0),
      m_lastFullTimestamp(0),
      m_lastActivity(-1.0),
      m_lastAgesSum(0.0),
      m_lastDanglingNodeActivity(0.0)
{
    // do nothing
}

CalculationState::~CalculationState() 
{
    // do nothing
}

int CalculationState::lastDanglingNodeCount() const
{
    return m_lastDanglingNodeCount;
}

void CalculationState::setLastDanglingNodeCount(int value)
{
    m_lastDanglingNodeCount = value;
}

int CalculationState::lastNodeCount() const
{
    return m_lastNodeCount;
}

void CalculationState::setLastNodeCount(int value)
{
    m_lastNodeCount = value;
}

unsigned int CalculationState::lastFullTimestamp() const
{
    return m_lastFullTimestamp;
}

void CalculationState::setLastFullTimestamp(unsigned int value)
{
    m_lastFullTimestamp = value;
}

double CalculationState::lastActivity() const 
{
    return m_lastActivity;
}

void CalculationState::setLastActivity(double value)
{
    m_lastActivity = value;
}

double CalculationState::lastAgesSum() const
{
    return m_lastAgesSum;
}

void CalculationState::setLastAgesSum(double value)
{
    m_lastAgesSum = value;
}

double CalculationState::lastDanglingNodeActivity() const
{
    return m_lastDanglingNodeActivity;
}

void CalculationState::setLastDanglingNodeActivity(double value)
{
    m_lastDanglingNodeActivity = value;
}
