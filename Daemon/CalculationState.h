/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CALCULATION_STATE_H
#define CALCULATION_STATE_H

class CalculationState 
{
public:
    CalculationState();

    ~CalculationState();

    int lastDanglingNodeCount() const;

    void setLastDanglingNodeCount(int value);

    int lastNodeCount() const;

    void setLastNodeCount(int value);

    unsigned int lastFullTimestamp() const;

    void setLastFullTimestamp(unsigned int value);

    double lastActivity() const;

    void setLastActivity(double value);

    double lastAgesSum() const;

    void setLastAgesSum(double value);

    double lastDanglingNodeActivity() const;

    void setLastDanglingNodeActivity(double value);

 private:

    //! The  number of dangling nodes from the last full calculation
    int m_lastDanglingNodeCount;

    //! The total number of nodes from the last full calculation
    int m_lastNodeCount;

    //! Last full run timestamp in secs after 1970
    unsigned int m_lastFullTimestamp;

    //! The total sum of activity  from the last full calculation
    double m_lastActivity;

    //! The total sum of ages
    double m_lastAgesSum;

    //! The total sum of activity on dangling nodes
    double m_lastDanglingNodeActivity;
};

#endif
