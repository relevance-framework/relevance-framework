/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cmath>
#include <unistd.h>
#include <QDate>
#include <QTime>
#include <QSqlDriver>
#include <QSqlField>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QUrl>
#include <QBuffer>

#include "RelevanceStorageService.h"
#include "RelevanceDaemonDefines.h"

#include "ExpDecayCounter.h"
#include "LinearDecayCounter.h"
#include "NoDecayCounter.h"

#include "AbstractHarvester.h"

// Cleanup thresholds - older activity entries and counts are removed
// NB. This does not remove URI mappings!
#define ACTIVITY_THRESHOLD ONE_MONTH_IN_SECONDS
#define COUNT_THRESHOLD ONE_YEAR_IN_SECONDS

// Chance of cleanup at idle timeout exit is CLEANUP_CHANCE in a million
#define CLEANUP_CHANCE 10000

#if ACTIVITY_THRESHOLD >= COUNT_THRESHOLD
#error ACTIVITY THRESHOLD MUST BE LESS THAN COUNT THRESHOLD FOR CLEANUP PROCESS TO WORK!
#endif


#define CONTENT_DB_FILE "/.cache/relevance/content.db"
// Minimum time between activities for same URL (seconds)
// TODO: This is copied from Main and it should be just passed as parameter for the constructor.
// But to do so we need to refactor the ownerships.
#define CACHE_TIME 10

static bool SqliteFTSenabled = true;

RelevanceStorageService::RelevanceStorageService(QObject* parent): QObject(parent), 
                                                                   // Default Database, all queries which have not mentioned any 
                                                                   // database acts on this database, i.e, Content database
                                                                   m_contentDb(QSqlDatabase::addDatabase("QSQLITE")), 
                                                                   m_userDb(QSqlDatabase::addDatabase("QSQLITE", "user_db")),
                                                                   m_cacheTime(CACHE_TIME)
{
}

RelevanceStorageService::~RelevanceStorageService() {
    close();
}

void RelevanceStorageService::addCounter(ActivityCounter* counter) {
    m_counters.insert(counter->name(), counter);
}

#define DEFAULT_COUNTER "ExpDecay_2weeks"

void RelevanceStorageService::initializeCounters() {
    // Uncomment to enable counters.
    // You can enable new counters on an existing database, but you cannot remove counters once enabled.

    // addCounter(new ExpDecayCounter("ExpDecay_day", ONE_DAY_IN_SECONDS));
    // addCounter(new ExpDecayCounter("ExpDecay_2days", TWO_DAYS_IN_SECONDS));
    // addCounter(new ExpDecayCounter("ExpDecay_week", ONE_WEEK_IN_SECONDS));
    addCounter(new ExpDecayCounter("ExpDecay_2weeks", TWO_WEEKS_IN_SECONDS));
    // addCounter(new ExpDecayCounter("ExpDecay_month", ONE_MONTH_IN_SECONDS));
    // addCounter(new ExpDecayCounter("ExpDecay_2months", TWO_MONTHS_IN_SECONDS));
    // addCounter(new ExpDecayCounter("ExpDecay_year", ONE_YEAR_IN_SECONDS));

    // addCounter(new LinearDecayCounter("LinearDecay_day", ONE_DAY_IN_SECONDS));
    // addCounter(new LinearDecayCounter("LinearDecay_2days", TWO_DAYS_IN_SECONDS));
    // addCounter(new LinearDecayCounter("LinearDecay_week", ONE_WEEK_IN_SECONDS));
    // addCounter(new LinearDecayCounter("LinearDecay_2weeks", TWO_WEEKS_IN_SECONDS));
    // addCounter(new LinearDecayCounter("LinearDecay_month", ONE_MONTH_IN_SECONDS));
    // addCounter(new LinearDecayCounter("LinearDecay_2months", TWO_MONTHS_IN_SECONDS));
    // addCounter(new LinearDecayCounter("LinearDecay_year", ONE_YEAR_IN_SECONDS));

    // addCounter(new NoDecayCounter());
}

QStringList RelevanceStorageService::activityCountModes() {
    return m_counters.keys();
}

// FIXME: Ensure SQL parameters are always in quotes (sqlstring method)
// Also, recheck SQL parameters API
QString RelevanceStorageService::sqlString(QString s) {
    return s.replace("'", "''");
}

// TODO: Combine Counts and Uris tables so that counts contains URI string.
// It will make both addActivity() and cleanup() more efficient.

// TODO: There are now several indexes that are meant to make cleanup operation faster.
// See indexes commented with "To enable fast cleanup..."
// However, they will make insert operations slower. May be better to just let cleanup be slow
// since it can be done in background. Fortunately it is easy to change, just remove or comment out the index
// creation code below.

int RelevanceStorageService::initializeUserDatabase (const QString &dbFile)
{
    bool created = false;

    QFileInfo fi (dbFile);
    if (!fi.exists()) {
        if (!QFile::exists(fi.absolutePath())) {
            QDir().mkpath(fi.absolutePath());
        }
    }
    qDebug () << "User Db path :" << dbFile ;
    m_userDb.setDatabaseName(dbFile);
    if (!m_userDb.open()) {
        qCritical() << "Unable to establish a database connection for user database.";
        return -1;
    }

    // Sql query object used through out the method
    QSqlQuery user_db_query (m_userDb) ;

    if (!user_db_query.exec("CREATE TABLE IF NOT EXISTS SchemaVersion (id INTEGER PRIMARY KEY, major INTEGER, minor INTEGER)")){
        qCritical() << "Unable to create table for schema version: " << user_db_query.lastError().text();
        return -1;
    }

    if (!user_db_query.exec("SELECT major,minor FROM SchemaVersion WHERE id=1")) {
        qCritical() << "Unable to retrieve schema version: " << user_db_query.lastError().text();
        return -1;
    }
    if (user_db_query.next()) {
        bool ok;
        int major = user_db_query.value(0).toInt(&ok);
        if (!ok) {
            qCritical() << "Illegal major schema version: " << user_db_query.value(0).toString();
            return -1;
        }
        int minor = user_db_query.value(1).toInt(&ok);
        if (!ok) {
            qCritical() << "Illegal minor schema version: " << user_db_query.value(1).toString();
            return -1;
        }

        // Minor version number change => database can be upgraded with an O(1) operation
        // Major version number change => database upgrade requires an O(n) operation

        // Attempting to use a newer database than supported by current software version is not possible
        if (major != 1 || minor != 0) {
            qCritical() << "This version of RelevanceStorage can only handle an activity database with schema version 1.0 (was: "
                    << major << "." << minor << ")";
            return -1;
        }

        //qDebug() << "Database schema version is " << major << "." << minor;

        // If we supported older database schema versions, conversion would be performed now, before continuing with initialization

    } else {
        if (!user_db_query.exec("INSERT INTO SchemaVersion VALUES (1, 1, 0)")) {
            qCritical() << "Unable to initialize database schema version to 1.0: " << user_db_query.lastError().text();
            return -1;
        }
        created = true;
        //qDebug() << "Initialized database schema version to 1.0.";
    }

    // Storage for user activities
    if (!user_db_query.exec("CREATE TABLE IF NOT EXISTS Activities (id INTEGER PRIMARY KEY, subject INTEGER, source INTEGER, "
                    "type INTEGER, application INTEGER, timestamp INTEGER, duration INTEGER)")) {
        qCritical() << "Unable to create table for activities: " << user_db_query.lastError().text();
        return -1;
    }

    // To enable fast cleanup based on timestamp
    if (!user_db_query.exec("CREATE INDEX IF NOT EXISTS Activities_timestamp ON Activities(timestamp)")) {
        qCritical() << "Unable to create index for Activities timestamp field: " << user_db_query.lastError().text();
        return -1;
    }

    if (!user_db_query.exec("CREATE TABLE IF NOT EXISTS OntologyCodes (id INTEGER PRIMARY KEY, uri TEXT)")) {
        qCritical() << "Unable to create table for OntologyCodes: " << user_db_query.lastError().text();
        return -1;
    }

    // Storage for Annotations
    if (!user_db_query.exec("CREATE TABLE IF NOT EXISTS Annotations (id INTEGER PRIMARY KEY, activity INTEGER, type INTEGER, value TEXT)")) {
        qCritical() << "Unable to create table for Annotations: " << user_db_query.lastError().text();
        return -1;
    }

    // To enable fast cleanup based on activity
    if (!user_db_query.exec("CREATE INDEX IF NOT EXISTS Annotations_activity ON Annotations(activity)")) {
        qCritical() << "Unable to create index for Annotations activity field: " << user_db_query.lastError().text();
        return -1;
    }

    if (!user_db_query.exec("CREATE TABLE IF NOT EXISTS Totals (id INTEGER PRIMARY KEY, timestamp INTEGER)")) {
        qCritical() << "Unable to create table for totals: " << user_db_query.lastError().text();
        return -1;
    }

    QString queryString;
    QSqlRecord rec = m_userDb.record("Totals");
    for (QMap<QString, ActivityCounter*>::const_iterator i = m_counters.constBegin(); i != m_counters.constEnd(); i++) {
        QString name = i.key();
        if (!rec.contains("c_" + name)) {
            queryString = "ALTER TABLE Totals ADD COLUMN c_%1 REAL DEFAULT 0";
            if (user_db_query.exec(queryString.arg(name))) {
                // qDebug() << "Added column " << name << " to Totals";
            } else {
                qCritical() << "Unable to add column " << name << " to Totals: " << user_db_query.lastError().text();
            }
        }
    }

    // Totals are cached for quick access, so we need to retrieve the old values at startup
    if (!user_db_query.exec("SELECT * FROM Totals WHERE id=1")) {
        qCritical() << "Unable to retrieve old totals: " << user_db_query.lastError().text();
        return -1;
    }
    if (user_db_query.next()) {
        QSqlRecord rec = user_db_query.record();
        for (QMap<QString, ActivityCounter*>::const_iterator i = m_counters.constBegin(); i != m_counters.constEnd(); i++) {
            QString name = i.key();
            int field = rec.indexOf("c_" + name);
            if (field >= 0) {
                bool ok;
                double value = user_db_query.value(field).toDouble(&ok);
                if (ok) {
                    m_totals.insert(name, value);
                }
            }
        }
        int timeField = rec.indexOf("timestamp");
        if (timeField < 0) {
             qCritical() << "Timestamp not found in totals table";
            return -1;
        }
        bool ok;
        uint timestamp = user_db_query.value(timeField).toUInt(&ok);
        if (!ok) {
            qCritical() << "Illegal timestamp in totals table";
            return -1;
        }
        m_totalsTimestamp = timestamp;

        // qDebug() << "Retrieved old counter totals";
    } else {
        // If Totals table is empty, we need to insert the initial zero values for every counter.

        QString queryString = "INSERT INTO Totals VALUES %1";
        QString initialValues = "( 1, %1";
        for (QMap<QString, ActivityCounter*>::const_iterator i = m_counters.constBegin(); i != m_counters.constEnd(); i++) {
            initialValues.append(", 0");
            m_totals.insert(i.key(), 0.0);
        }
        initialValues.append(")");
        queryString = queryString.arg(initialValues.arg(QDateTime::currentDateTime().toTime_t()));
        // qDebug() << "Initializing totals with query: " << queryString;
        if (!user_db_query.exec(queryString)) {
            qCritical() << "Unable to initialize totals: " << user_db_query.lastError().text();
            return -1;
        }
    }

    // Cache Ontology Uris & Ids for fast accessing
    if (!user_db_query.exec("SELECT uri,id FROM OntologyCodes")) {
        qCritical() << "Unable to retrieve ontology mappings: " << user_db_query.lastError().text();
        return -1;
    }
    while(user_db_query.next()) {
        QString uri = user_db_query.value(0).toString();
        bool ok;
        int code = user_db_query.value(1).toInt(&ok);
        if (ok) {
            m_ontologyCodes.insert(uri, code);
            m_ontologyUris.insert(code, uri);
        }
    }
    return 0;
}

int RelevanceStorageService::initializeContentDatabase (const QString &dbFile)
{
    bool created = false;

    // Set up DB
    QFileInfo fi(dbFile);
    if (!fi.exists()) {
        if (!QFile::exists(fi.absolutePath())) {
            QDir().mkpath(fi.absolutePath());
        }
    }
    qDebug () << "Content Db Location :" << dbFile ;
    m_contentDb.setDatabaseName(dbFile);
    if (!m_contentDb.open()) {
        qCritical() << "Unable to establish a database connection for content Database.";
        return -1; 
    }

    // Query object used through out this method
    QSqlQuery query (m_contentDb);

    if (!query.exec("CREATE TABLE IF NOT EXISTS SchemaVersion (id INTEGER PRIMARY KEY, major INTEGER, minor INTEGER)")){
        qCritical() << "Unable to create table for schema version: " << query.lastError().text();
        return -1;
    }

    if (!query.exec("SELECT major,minor FROM SchemaVersion WHERE id=1")) {
        qCritical() << "Unable to retrieve schema version: " << query.lastError().text();
        return -1;
    }
    if (query.next()) {
        bool ok;
        int major = query.value(0).toInt(&ok);
        if (!ok) {
            qCritical() << "Illegal major schema version: " << query.value(0).toString();
            return -1;
        }
        int minor = query.value(1).toInt(&ok);
        if (!ok) {
            qCritical() << "Illegal minor schema version: " << query.value(1).toString();
            return -1;
        }

        // Minor version number change => database can be upgraded with an O(1) operation
        // Major version number change => database upgrade requires an O(n) operation

        // Attempting to use a newer database than supported by current software version is not possible
        if (major != 1 || minor != 0) {
            qCritical() << "This version of RelevanceStorage can only handle an activity database with schema version 1.0 (was: "
                    << major << "." << minor << ")";
            return -1;
        }

        //qDebug() << "Database schema version is " << major << "." << minor;

        // If we supported older database schema versions, conversion would be performed now, before continuing with initialization

    } else {
        if (!query.exec("INSERT INTO SchemaVersion VALUES (1, 1, 0)")) {
            qCritical() << "Unable to initialize database schema version to 1.0: " << query.lastError().text();
            return -1;
        }
        created = true;
        //qDebug() << "Initialized database schema version to 1.0.";
    }

    // Here we create the database schema. In the main Activities table we store known attributes:
    // subject, source, type, application, timestamp, duration. Additional attributes are stored
    // as annotations in the Annotations table. An annotation is simply a (type, value) pair,
    // where both are strings.
    // Counts for each URI are stored in the Counts table, which has a column for each supported count type.
    // There is also a Totals table which contains a single row that has a column for each supported count type.

    // In order to prevent database from growing indefinitely, we remove old data from the Activities and
    // Counts table occasionally. This may lead to loss of information if count type is not decaying,
    // i.e. count value does not decrease with time.

    // In order to optimize space taken by the database, we map common strings to integers.
    // Subject URIs have their own mapping table, all other strings use the OntologyCodes table:
    // Subject URIs => Uris table
    // source => OntologyCodes table
    // type => OntologyCodes table
    // application => OntologyCodes table
    // Annotation type => OntologyCodes table
    //
    // Note that OntologyCodes lookup data is kept in memory in the m_ontologyCodes and m_ontologyUris hashtables.
    // SubjectUris, on the other hand, are not kept in memory.
    // The assumption is that the set of OntologyCodes values is much smaller than that of Subject Uris, so
    // it can be kept entirely in memory for fast integer code lookups.
    // For Subject URI integer mapping lookup efficiency we rely on SQLite.


    // This contains timestamp for last update from tracker as well as (later) some general variables for relevance calc

    if (!query.exec("CREATE TABLE IF NOT EXISTS State (id INTEGER PRIMARY KEY, last_update INTEGER, "
                    "last_dangling_node_count INTEGER, last_node_count INTEGER, last_full_timestamp INTEGER, last_activity REAL, last_ages_sum REAL, last_dangling_node_activity REAL)")) {
        qCritical() << "Unable to create table for State: " << query.lastError().text();
        return -1;
    }

    if (!query.exec("SELECT last_update FROM State WHERE id=1")) {
        qCritical() << "Unable to retrieve State: " << query.lastError().text();
        return -1;
    }
    if (query.next()) {
        bool ok;
        m_lastUpdate = query.value(0).toUInt(&ok);
        if (!ok) {
            qCritical() << "Illegal timestamp value: " << query.value(0).toString();
            return -1;
        }
    } else {
        if (!query.exec("INSERT INTO State VALUES (1, 0, 0, 0, 0, -1.0, 0.0, 0.0)")) {
            qCritical() << "Unable to initialize State table: " << query.lastError().text();
            return -1;
        }
        m_lastUpdate = 0;
    }

/*
    if (!query.exec("CREATE TABLE IF NOT EXISTS Links (subject_uri TEXT, link_count INTEGER)")) {
        qCritical() << "Unable to create table for Links: " << query.lastError().text();
        return -1;
    }

    if (!query.exec("CREATE INDEX IF NOT EXISTS Links_subject_uri ON Links(subject_uri)")) {
        qCritical() << "Unable to create index for Links subject_uri field: " << query.lastError().text();
        return -1;
    }
*/
/*
    if (!query.exec("CREATE TABLE IF NOT EXISTS Uris (id INTEGER PRIMARY KEY, uri TEXT)")) {
        qCritical() << "Unable to create table for Uris: " << query.lastError().text();
        return -1;
    }

    if (!query.exec("CREATE UNIQUE INDEX IF NOT EXISTS Uris_uri ON Uris(uri)")) {
        qCritical() << "Unable to create index for Uris uri field: " << query.lastError().text();
        return -1;
    }
*/
    // Changes to Subjects table : to hold primary, secodary, tertiary images and text
    if (!query.exec("CREATE TABLE IF NOT EXISTS Subjects (id INTEGER PRIMARY KEY, uri TEXT UNIQUE, class INTEGER, link_count INTEGER,"
                    " add_time INTEGER, relevance REAL, timestamp INTEGER"
                    ", details BLOB)")) {
        qCritical() << "Unable to create table for Subjects: " << query.lastError().text();
        return -1;
    }

    // To enable fast lookup based on uri string
    if (!query.exec("CREATE INDEX IF NOT EXISTS Subjects_uri ON Subjects(uri)")) {
        qCritical() << "Unable to create index for Subjects uri field: " << query.lastError().text();
        return -1;
    }

    // To enable fast cleanup based on timestamp
    if (!query.exec("CREATE INDEX IF NOT EXISTS Subjects_timestamp ON Subjects(timestamp)")) {
        qCritical() << "Unable to create index for Subjects timestamp field: " << query.lastError().text();
        return -1;
    }

    if (!query.exec("CREATE TABLE IF NOT EXISTS ClassHierarchy (id INTEGER PRIMARY KEY, class_uri TEXT UNIQUE, depth INTEGER, link_count INTEGER, relevance REAL)")) {
        qCritical() << "Unable to create table for ClassHierarchy: " << query.lastError().text();
        return -1;
    }

    if (!query.exec("CREATE TABLE IF NOT EXISTS Relations (fromNode INTEGER, toNode INTEGER)")) {
        qCritical() << "Unable to create table for relations: " << query.lastError().text();
        return -1;
    }

    if (!created)
        // fts_table must exists 
        SqliteFTSenabled = true;
    else if (!query.exec("CREATE VIRTUAL TABLE fts_table USING fts3(id INTEGER PRIMARY KEY, content TEXT, tokenize=simple)")) {
        qCritical() << "Unable to create table for fts_table : " << query.lastError().text();
        qCritical() << "Trying without FTS ...";
        if (!query.exec("CREATE TABLE fts_table (id INTEGER PRIMARY KEY, content TEXT)")) {
            qCritical() << "Unable to Create table :" << query.lastError().text();
        }
        SqliteFTSenabled = false;
    }
    else SqliteFTSenabled = true;
    

    if (!query.exec("CREATE TABLE IF NOT EXISTS Groups (groupID INTEGER PRIMARY KEY, class TEXT, uri TEXT, primary_label TEXT)")) {
        qCritical() << "Unable to create table for Groups : " << query.lastError().text();
    }

    if (!query.exec("CREATE TABLE IF NOT EXISTS ItemGroups (groupID INTEGER, itemID INTEGER)")) {
        qCritical() << "Unable to create table for ItemGroups : " << query.lastError().text();
    }


    // If we have more counter algorithms available now than when the table was created, we need to add
    // the corresponding columns to the Counts and Totals tables.

    QString queryString;
    QSqlRecord rec = m_contentDb.record("Subjects");
    for (QMap<QString, ActivityCounter*>::const_iterator i = m_counters.constBegin(); i != m_counters.constEnd(); i++) {
        QString name = i.key();
        if (!rec.contains("c_" + name)) {
            queryString = "ALTER TABLE Subjects ADD COLUMN c_%1 REAL DEFAULT 0";
            if (query.exec(queryString.arg(name))) {
                // qDebug() << "Added column " << name << " to Subjects";
            } else {
                qCritical() << "Unable to add column " << name << " to Subjects: " << query.lastError().text();
            }
        }
    }
 
    loadStaticData();

    return 0;
}

int RelevanceStorageService::initialize(const QString &dbFile) {

    initializeCounters(); 

    if (initializeUserDatabase (dbFile) != 0) {
        qCritical () << "Error : Could not initialize user dabase" ;
        return -1; // Something went wrong while initializing user database
    }

    // open Content Db
    QString contentDbFile = getenv("HOME") ;
    contentDbFile.append (CONTENT_DB_FILE);
    if (initializeContentDatabase ( contentDbFile ) != 0) {
        qCritical () << "Error : Could not initialize content dabase" ;
        return -1; // Something went wrong while initializing content database
    }

    // Setup randomness (currently only used for cleanup chance at idle timeout exit)
    qsrand(QDateTime::currentDateTime().toTime_t());

    return 0;
}

//TODO: Use SQL JOIN or similar instead of multiple deletes
bool RelevanceStorageService::removeOldActivities() {
    // qDebug() << "Removing old activities";
    uint now = QDateTime::currentDateTime().toTime_t();
    uint timeThreshold = now - ACTIVITY_THRESHOLD;
    QString queryString =   "SELECT id FROM Activities WHERE timestamp < %1";

    // qDebug() << "Activity threshold time is" << timeThreshold;

    QSqlQuery query(m_userDb);
    if (!query.exec(queryString.arg(timeThreshold))) {
        qCritical() << "Unable to query: " << query.lastError().text();
        return false;
    }

    QSqlQuery removeQuery(m_userDb);
    QString removeActivityQueryString = "DELETE FROM Activities WHERE id = %1";
    QString removeAnnotationQueryString = "DELETE FROM Annotations WHERE activity = %1";

    while (query.next()) {
        //QVariantMap activity;
        bool ok;
        int id = query.value(0).toInt(&ok);
        // qDebug() << "Found activity to remove:" << id;
        if (!ok) {
            qWarning() << "Unable to read activity id:" << query.value(0).toString();
            continue;
        }
        if (!removeQuery.exec(removeActivityQueryString.arg(id))) {
            qCritical() << "Unable to remove activity: " << removeQuery.lastError().text();
            return false;
        } else {
            if (!removeQuery.exec(removeAnnotationQueryString.arg(id))) {
                qCritical() << "Unable to remove annotations: " << removeQuery.lastError().text();
                return false;
            }
        }
    }

    return true;
}
// TODO: Do we still remove subject table lines which have not been accessed in a long time?
// Implication is that they cannot be found by search. This might not be a good thing...
// Maybe we should only remove explicitly. Need to add method for that.
//TODO: Use SQL JOIN or similar instead of multiple deletes
bool RelevanceStorageService::removeOldCounts() {

 /* COUNTS MUST BE REMOVED EXPLICTLY FROM NOW ON

    // qDebug() << "Removing old counts";

    uint now = QDateTime::currentDateTime().toTime_t();
    uint countThreshold = now - COUNT_THRESHOLD;
    QString queryString =   "SELECT uri FROM Counts WHERE timestamp < %1";

    // qDebug() << "Count threshold time is" << countThreshold;

    QSqlQuery query;
    if (!query.exec(queryString.arg(countThreshold))) {
        qCritical() << "Unable to query: " << query.lastError().text();
        return false;
    }

    QSqlQuery removeQuery;
    QString removeCountQueryString = "DELETE FROM Counts WHERE uri = %1";
    QString removeUriQueryString = "DELETE FROM Uris WHERE id = %1";

    while (query.next()) {
        QVariantMap activity;
        bool ok;
        int uri = query.value(0).toInt(&ok);
        // qDebug() << "Found count to remove:" << uri;
        if (!ok) {
            qWarning() << "Unable to read count uri:" << query.value(0).toString();
            continue;
        }
        if (!removeQuery.exec(removeCountQueryString.arg(uri))) {
            qCritical() << "Unable to remove count: " << removeQuery.lastError().text();
            return false;
        } else {
            if (!removeQuery.exec(removeUriQueryString.arg(uri))) {
                qCritical() << "Unable to remove uri: " << removeQuery.lastError().text();
                return false;
            }
        }
    }
    */
    return true;
}


bool RelevanceStorageService::recalculateTotals() {
    // qDebug() << "Recalculating totals";
    uint totalTime = QDateTime::currentDateTime().toTime_t();
    QMap<QString, double> totals;
    for (QMap<QString, double>::const_iterator i = m_totals.constBegin(); i != m_totals.constEnd(); i++) {
        totals.insert(i.key(), 0.0);
    }

    QSqlQuery user_db_query (m_userDb);
    QString errorString;
    QString queryString = "SELECT * FROM Totals";
    if (!user_db_query.exec(queryString)) {
        qCritical() << "Unable to retrieve Totals: " << user_db_query.lastError().text();
        return false;
    }
    QSqlRecord rec = user_db_query.record();
    int tsIndex = rec.indexOf("timestamp");
    if (tsIndex < 0) {
            qCritical() << "Cannot find timestamp field in Subjects table, using current time instead!";
    }
    while (user_db_query.next()) {
        uint counterTime;
        if (tsIndex >= 0) {
            bool ok;
            counterTime = user_db_query.value(tsIndex).toUInt(&ok);
            if (!ok) {
                qCritical() << "Illegal timestamp in Subjects table, using current time instead!";
                counterTime = totalTime;
            }
        } else {
            counterTime = totalTime;
        }

        for (int i = 0; i < rec.count(); i++) {
            QString fieldName = rec.fieldName(i);
            if (fieldName.startsWith("c_")) {
                QString counterName = fieldName.mid(2);
                ActivityCounter* counter = m_counters.value(counterName);
                if (counter != NULL) {
                    bool ok;
                    double counterValue = user_db_query.value(i).toDouble(&ok);
                    if (ok) {
                        double totalValue = totals.value(counterName, 0.0);
                        totalValue += counter->convert(counterValue, counterTime, totalTime);
                        totals.insert(counterName, totalValue);
                    }
                }
            }
        }
    }

    // Now that we have successfully recalculated totals we can update the in-use totals

    m_totals = totals;
    m_totalsTimestamp = totalTime;
    queryString = "UPDATE Totals SET timestamp=%1";
    QString assignList;
    QString assignment = ", c_%1=%2";
    for (QMap<QString, double>::const_iterator i = m_totals.constBegin(); i != m_totals.constEnd(); i++) {
        assignList.append(assignment.arg(i.key()).arg(i.value()));
    }
    queryString.append(assignList);
    queryString.append(" WHERE id=1");
    qDebug() << "query for Totals UPDATE: " << queryString;

    if (!user_db_query.exec(queryString.arg(totalTime))) {
        qCritical() << "Unable to update Totals: " << user_db_query.lastError().text();
        errorString = "error:internal_error/unable_to_update_totals/";
        errorString.append(user_db_query.lastError().text());
        return false;
    }

    return true;
}


int RelevanceStorageService::cleanup() {
    qDebug() << "Running cleanup";
    beginTransaction(m_userDb);
    if (removeOldActivities()) {
        if (removeOldCounts()) {
            if (recalculateTotals()) {
                commitTransaction(m_userDb);
                return 0;
            }
        }
    }
    if ( abortTransaction(m_userDb) != TRUE ) {
      qDebug() << "Failed to abort Transaction";
    };
    return -1;
}

void RelevanceStorageService::close() {
    m_contentDb.close();
    m_userDb.close();
}


void RelevanceStorageService::addActivity(const QString &subject, const QString &application, const QString &type,
                                          const QString &timestamp, int duration, const QString &source) {
    addActivitySynchronously(subject, application, type, timestamp, duration, source);
}

void RelevanceStorageService::addActivity(QVariantMap activity) {
    addActivitySynchronously(activity);
}

void RelevanceStorageService::addActivities(QVariantList activities) {
    addActivitiesSynchronously(activities);
}

QString RelevanceStorageService::addActivitySynchronously(const QString &subject, const QString &application, const QString &type,
                                                          const QString &timestamp, int duration, const QString &source) {
    QVariantMap activity;
    // Subject is mandatory
    activity.insert(RELEVANCESTORAGE_SUBJECT, subject);

    // The others are optional
    if (application != "") {
        activity.insert(RELEVANCESTORAGE_APPLICATION, application);
    }
    if (type != "") {
        activity.insert(RELEVANCESTORAGE_TYPE, type);
    }
    if (timestamp != "") {
        activity.insert(RELEVANCESTORAGE_TIMESTAMP, timestamp);
    }
    if (duration >= 0) {
        activity.insert(RELEVANCESTORAGE_DURATION, QString::number(duration));
    }
    if (source != "") {
        activity.insert(RELEVANCESTORAGE_SOURCE, source);
    }

    return addActivitySynchronously(activity);
}

void RelevanceStorageService::addActivities(QStringList subjectUris) {
    QVariantList activitiesList;
    foreach(QString uri, subjectUris) {
        QVariantMap activity;
        // Subject is mandatory
        activity.insert(RELEVANCESTORAGE_SUBJECT, uri);
        activitiesList << activity;
    }
    addActivities(activitiesList);
}

//FIXME: Comment what this does (and how) in more detail

QString RelevanceStorageService::updateCounts(int uri, QDateTime now, QDateTime activityTime, QVariantMap& activity) {
    // Timestamp for new count - always "now", value is converted if needed
    QDateTime countTime = now;
    QMap<QString, double> totals = m_totals;
    QSqlQuery query;
    QString errorString;
    QMap<QString, double> counterValues;
    QString queryString = "SELECT * FROM Subjects WHERE id=%1";
    if (!query.exec(queryString.arg(uri))) {
        qCritical() << "Unable to retrieve old counts: " << query.lastError().text();
        return "error:internal_error/unable_to_retrieve_counts";
    }
    QSqlRecord rec = query.record();
    uint oldTime = 0;
    if (query.next()) {
        for (int i = 0; i < rec.count(); i++) {
            QString fieldName = rec.fieldName(i);
            if (fieldName == "timestamp") {
                bool ok;
                oldTime = query.value(i).toUInt(&ok);
                if (!ok) {
                    qCritical() << "Illegal timestamp in Subjects table, using current time instead!";
                    oldTime = now.toTime_t();
                }
            } else if (fieldName.startsWith("c_")) {
                QString counterName = fieldName.mid(2);
                bool ok;
                double oldValue = query.value(i).toDouble(&ok);
                if (ok) {
                    counterValues.insert(counterName, oldValue);
                }
            }
        }
    }

    if (oldTime == 0){
        qCritical() << "[Relevance Storage] No record found for URI : " << uri ;
        return QString("error:internal_error/invalid_uri");
    }

    for (QMap<QString, ActivityCounter*>::const_iterator i = m_counters.constBegin(); i != m_counters.constEnd(); i++) {
        QString name = i.key();
        ActivityCounter* counter = i.value();
        double value = counter->convert(counter->baseValue(activity), activityTime.toTime_t(), countTime.toTime_t());
        // qDebug() << "Adding to " << counter->name() << ": " << value << "(" << activityTime.toString(Qt::ISODate) << ")";
        double oldValue = counterValues.value(name, 0.0);
        value += counter->convert(oldValue, oldTime, countTime.toTime_t());
        counterValues.insert(name, value);

        double diff = value - oldValue;
        double oldTotal = totals.value(name, 0.0);
        totals.insert(name, oldTotal + diff);
    }


    queryString = "UPDATE Subjects SET timestamp=%2";
    QString assignList;
    QString assignment = ", %1=%2";
    for (QMap<QString, double>::const_iterator i = counterValues.constBegin(); i != counterValues.constEnd(); i++) {
        assignList.append(assignment.arg("c_" + i.key()).arg(i.value()));
    }
    queryString.append(assignList);
    queryString.append(" WHERE id=%1");

    if (!query.exec(queryString.arg(uri).arg(countTime.toTime_t()))) {
        qCritical() << "Unable to update Subjects: " << query.lastError().text();
        errorString = "error:internal_error/unable_to_update_count/";
        errorString.append(query.lastError().text());
        return errorString;
    }
    // Clear for resue
    assignList.clear();
    assignment.clear();

    // Now that Subjects update succeeded we can update totals

    m_totals = totals;
    m_totalsTimestamp = countTime.toTime_t();
    queryString = "UPDATE Totals SET timestamp=%1";
    QSqlQuery user_db_query (m_userDb);
    assignment = ", %1=%2";
    for (QMap<QString, double>::const_iterator i = m_totals.constBegin(); i != m_totals.constEnd(); i++) {
        assignList.append(assignment.arg("c_" + i.key()).arg(i.value()));
    }
    queryString.append(assignList);
    queryString.append(" WHERE id=1");
    // qDebug() << "query for Totals UPDATE: " << queryString;

    if (!user_db_query.exec(queryString.arg(countTime.toTime_t()))) {
        qCritical() << "Unable to update Totals: " << query.lastError().text();
        errorString = "error:internal_error/unable_to_update_totals/";
        errorString.append(query.lastError().text());
        return errorString;
    }

    return "";
}

unsigned int RelevanceStorageService::timeStamp(const QString &uri)
{
    if (uri.isEmpty()) return 0 ;
    QSqlQuery query(m_contentDb);
   
    // query Subjects table for timestamp of uri
    if (!query.exec (QString("SELECT timestamp FROM Subjects WHERE uri='%1'").arg (uri))){
        //unable to exectue the query, some thing wron with db
        return 0 ;
    }
    
    //No valid record with uri
    if (!query.next ()) return 0;
    
    bool ok;
    unsigned int timestamp = query.value(0).toUInt(&ok);

    return ok ? timestamp : 0;
}

int RelevanceStorageService::uri2id(const QString &uri) {
    QSqlQuery query;
    QString queryString = "SELECT id FROM Subjects WHERE uri='%1'";
    if (!query.exec(queryString.arg(sqlString(uri)))) {
        qCritical() << "Unable to retrieve uri code: " << query.lastError().text();
        return -1;
    }
    if(!query.next()) { // Uri not found : leave decission to caller.
        return -1;
    }
    QVariant id = query.value(0);

    bool ok;
    int returnValue = id.toInt(&ok);
    if (ok) {
        return returnValue;
    } else {
        qCritical() << "Unable convert row id for new uri mapping to integer";
        return -1;
    }
}

QString RelevanceStorageService::id2uri(int id) {
    QSqlQuery query;
    QString queryString = "SELECT uri FROM Subjects WHERE id=%1";
    if (!query.exec(queryString.arg(id))) {
        qCritical() << "Unable to retrieve uri string: " << query.lastError().text();
        return "";
    }
    if(!query.next()) { // We need to create a new mapping
        qCritical() << "Unable to find mapping for uri code " << id << " in Uris ";
        return "";
    } else {
        return query.value(0).toString();
    }
}

int RelevanceStorageService::groupUri2id (const QString &uri)
{
    QSqlQuery query;
    QString queryString = "SELECT groupId FROM Groups WHERE uri='%1'";
    if (!query.exec(queryString.arg(sqlString(uri)))) {
        qCritical() << "Unable to retrieve uri code: " << query.lastError().text();
        return -1;
    }
    if(!query.next()) { 
        return -1;
    }
    bool ok;
    int id = query.value(0).toInt(&ok);

    if (!ok) 
        return -1;

    return id;
}

QString RelevanceStorageService::groupId2uri (int id) {
    QSqlQuery query;
    QString queryString = "SELECT uri FROM Groups WHERE id=%1";
    if (!query.exec(queryString.arg(id))) {
        qCritical() << "Unable to retrieve uri string: " << query.lastError().text();
        return "";
    }
    if(!query.next()) { 
        return "";
    } else {
        return query.value(0).toString();
    }
}

int RelevanceStorageService::addGroup (const QString &uri, const QString &classUri, const QString &label)
{
    QSqlQuery query ;
    QString queryString = "INSERT INTO Groups (uri, class, primary_label) values('%1', '%2', '%3')" ;

    if (!query.exec (queryString.arg(sqlString(uri), classUri, sqlString(label)))) {
        qCritical () << "Unabled to add new group : " << query.lastError().text();
        return -1; 
    }
    int rowId = query.lastInsertId().toInt();

    return rowId ;
}

//TODO: Load data from tracker
int RelevanceStorageService::addSubject( const QString &uri, AbstractHarvester* harvester ) {
    QStringList objectClass = harvester->getItemClass(uri);

    // Store objects and classes to node URI and edge lists

    if (objectClass.size() == 0) {
       // qCritical() << "No object classes found for" << uri;
        return -1;
    }

    QString leafClass = objectClass.at(0);
    QString timestamp = objectClass.at(1);

    return addSubject(uri, leafClass, 0, timestamp, harvester);
}

int RelevanceStorageService::addSubject(const QString &uri, const QString &classUri, int linkCount, const QString &addTime, AbstractHarvester* harvester ) {
    QSqlQuery query;
    QString primaryText(""), secondaryText(""), ftsText("") ;
    QStringList groupPrimaryLabels, groupUris ;
    int itemID;

    int leafClassId = m_classUris.value(classUri, 0);

    /* FIXME : for few cases leafclassId is getting ZORE.
     * Hence validating leafclassId
     */
    if (!leafClassId)
    {
        qCritical () << "Invalid leaf calls : Uri - " << uri << ", classUri - " << classUri ;
        return -1;
    }
    // Read Item properties from source
    QMap<QString,QVariant> itemProperties = harvester->getItemProperties( uri, classUri );

    bool isGroup = itemProperties.value ("isGroup").toBool();
    // If its a group, add to Groups table and return
    if (isGroup) {
        QString label = itemProperties.value ("groupPrimaryLabels").toStringList().at(0);
        return addGroup (uri, classUri, label);
    }
    primaryText = itemProperties.value ("defaultLabel").toString();
    secondaryText = itemProperties.value ("secondaryText").toString();
    ftsText = itemProperties.value ("ftsText").toString();
    itemProperties.remove ("ftsText"); // No more requreid in this MAP as it should go to fts_table
    
    groupUris = itemProperties.value ("groupUris").toStringList ();
    if(groupUris.size()) {
        groupPrimaryLabels = itemProperties.value ("groupPrimaryLabels").toStringList();

        itemProperties.remove ("groupUris");
        itemProperties.remove ("groupPrimaryLabels");
    }
    
    // TODO: Handle timezones
    uint addTimeInt = QDateTime::fromString(addTime,"yyyy-MM-ddTHH:mm:ssZ").toTime_t();

    QString queryTemplate = "INSERT INTO Subjects ";
    QString nameList = "(id, uri, class, link_count, add_time, timestamp, relevance, details";
    QString valueList = "(NULL, '%1', %2, %3, %4, %5, 0, :details";

    QString field = ", %1";
    for (QMap<QString, ActivityCounter*>::const_iterator i = m_counters.constBegin(); i != m_counters.constEnd(); i++) {
        nameList.append(field.arg("c_" + i.key()));
        valueList.append(field.arg("0.0"));
    }
    nameList.append(")");
    valueList.append(")");
    queryTemplate.append(nameList);
    queryTemplate.append(" VALUES ");
    queryTemplate.append(valueList.arg (sqlString(uri), QString::number(leafClassId), QString::number(linkCount), 
                                        QString::number(addTimeInt), QString::number(QDateTime::currentDateTime().toTime_t())));

    QBuffer buffer ;
    QByteArray data;

    buffer.open(QBuffer::ReadWrite);
    QDataStream blobData (&buffer) ;

    //write properties to Datastream 
    blobData << itemProperties ;

    buffer.reset ();
    data = buffer.data() ;
    buffer.close() ;

    query.prepare (queryTemplate);
    query.bindValue (":details", data);

    // write data to DB
    if (!query.exec()) {
        qCritical() << "Unable to add subject(" << uri << "): " << query.lastError().text();
        // old details of subject might be existing, try after removing 
        removeSubject(uri);
        if (!query.exec()) {
            qCritical() << "Retrying... FAILED." << endl << "   Query was:" << queryTemplate;
            return -1;
        }
        else {
            qDebug() << "Retrying... success";
        }
    }

    QVariant id = query.lastInsertId();
    bool ok;
    int returnValue = id.toInt(&ok);

    int fromId = returnValue;

    itemID = returnValue;

    // implement the grouping / sub-grouping of subjects
    while (groupUris.size()) {
        const QString &groupUri = groupUris.at(0);
        const QString &groupLabel = groupPrimaryLabels.at(0);
        
        int groupId = groupUri2id (groupUri);
        // if no group exists with this id, create new one
        if (groupId <= 0) groupId = addGroup (groupUri, classUri, groupLabel);

        if (groupId >0) {
            QString insertItemGroups = "INSERT INTO ItemGroups VALUES (%1, %2 )";
            if(!query.exec(insertItemGroups.arg(QString::number(groupId),QString::number(itemID)))) {
                qCritical() << " did not insert to table ItemGroups " << query.lastError().text();
            }
        }
        groupUris.removeFirst();
        groupPrimaryLabels.removeFirst();
    }

    QSqlQuery ftsQuery;
    QString ftsQueryTemplate = "INSERT INTO fts_table VALUES (%1, '%2')";
    QString ftsQueryString = ftsQueryTemplate.arg(QString::number(fromId), sqlString(primaryText + " " + secondaryText + " " + ftsText));
    if (!ftsQuery.exec(ftsQueryString)) {
        qCritical() << " Unable to insert in fts_table" << ftsQuery.lastError().text();
        return -1;
    }

    QStringList result = harvester->getRelatedItems(uri);
    
    QSqlQuery queryInsert;
    QString queryInsertString = "INSERT INTO Relations values (%1, %2)";

    for(int i=0; i < result.size(); i++) {
        int toId = uri2id(result.at(i));
        if (toId == -1) // URI mapping not found try adding now
            toId = this->addSubject(result.at(i), harvester) ;
        if (toId != -1 && !queryInsert.exec(queryInsertString.arg(QString::number(fromId), QString::number(toId)))) {
            qCritical() << "Insert to Relations table failed "<< queryInsert.lastError().text();
            return -1;
        }
    }

    if (ok) {
        return returnValue;
    } else {
        qCritical() << "Unable convert row id for new uri mapping to integer";
        return -1;
    }
}

int RelevanceStorageService::removeSubject(const QString &uri) {
    QSqlQuery query;
    QString queryString = "DELETE FROM Subjects WHERE uri = '%1'";
    if (!query.exec(queryString.arg(sqlString(uri)))) {
        qCritical() << "Unable to create remove subject " << uri << ":" << query.lastError().text();
        return -1;
    }
    return 0;
}

// TODO: Need to check DB for existing entry before inserting a new one
// (Another process may have already added an entry for the URI)
int RelevanceStorageService::ontology2code(const QString &uri) {
    int code = m_ontologyCodes.value(uri, -1);
    if (code >=0) {
        return code;
    }
    QSqlQuery user_db_query (m_userDb);
    QString queryString = "INSERT INTO OntologyCodes values(NULL, '%1')";
    if (!user_db_query.exec(queryString.arg(sqlString(uri)))) {
        qCritical() << "Unable to create new uri code mapping for ontology: " << user_db_query.lastError().text();
        return -1;
    }

    bool ok;
    code = user_db_query.lastInsertId().toInt(&ok);
    if (ok) {
        m_ontologyCodes.insert(uri, code);
        m_ontologyUris.insert(code, uri);
        return code;
    } else {
        qCritical() << "Unable convert row id for new ontolgoy uri mapping to integer";
        return -1;
    }
}

// TODO: Need to check DB in case of cache miss
QString RelevanceStorageService::code2ontology(int code) {
    return m_ontologyUris.value(code, "");
}

bool RelevanceStorageService::beginTransaction(QSqlDatabase db) {
     return db.transaction();
}

bool RelevanceStorageService::abortTransaction(QSqlDatabase db) {
    int maxTries = 10;
    bool success = false;
    while(--maxTries >= 0) {
        success = db.rollback();
        if (success) {
            break;
        }
        qWarning() << "Unable to rollback, sleeping 1s and retrying";
        sleep(1);
    }
    return success;
}


bool RelevanceStorageService::commitTransaction(QSqlDatabase db) {
    int maxTries = 10;
    bool success = false;
    while(--maxTries >= 0) {
        success = db.commit();
        if (success) {
            break;
        }
        qWarning() << "Unable to commit, sleeping 1s and retrying";
        sleep(1);
    }
    return success;
}

QString RelevanceStorageService::writeActivityToDB(QVariantMap &activity) {
    QString subjectString = activity.value(RELEVANCESTORAGE_SUBJECT, "").toString();
    activity.remove(RELEVANCESTORAGE_SUBJECT);
    QString timestamp = activity.value(RELEVANCESTORAGE_TIMESTAMP, "").toString();
    activity.remove(RELEVANCESTORAGE_TIMESTAMP);
    QString durationString = activity.value(RELEVANCESTORAGE_DURATION, "").toString();
    activity.remove(RELEVANCESTORAGE_DURATION);
    QString applicationString = activity.value(RELEVANCESTORAGE_APPLICATION, "").toString();
    activity.remove(RELEVANCESTORAGE_APPLICATION);
    QString sourceString = activity.value(RELEVANCESTORAGE_SOURCE, RELEVANCESTORAGE_SOURCE_USERACTIVITY).toString();
    activity.remove(RELEVANCESTORAGE_SOURCE);
    QString typeString = activity.value(RELEVANCESTORAGE_TYPE, RELEVANCESTORAGE_TYPE_VISIT).toString();
    activity.remove(RELEVANCESTORAGE_TYPE);

    if (subjectString == "") {
        return QString("error:no_subject_defined");
    }

    int subject = uri2id(subjectString);
    if (subject < 0) {
        qCritical() << "Subject uri to id mapping failed for" << subjectString;
        return QString("error:internal_error/subject_uri_mapping_failed");
    }

    // Zero is a legal duration, meaning "instant"
    // -1 means "not given, or was negative, or could not be parsed"
    bool ok;
    int duration = durationString.toInt(&ok);
    if (ok) {
        if (duration < 0) {
            duration = 0;
        }
    } else {
        duration = -1;
    }

    int application = 0;
    if (applicationString != "") {
        application = ontology2code(applicationString);
        if (application < 0) {
            qCritical() << "Application to id mapping failed";
            return QString("error:internal_error/application_mapping_failed");
        }
    }

    int source = ontology2code(sourceString);
    if (source < 0) {
        qCritical() << "Source to id mapping failed";
        return QString("error:internal_error/source_mapping_failed");
    }

    int type = ontology2code(typeString);
    if (type < 0) {
        qCritical() << "Type to id mapping failed";
        return QString("error:internal_error/type_mapping_failed");
    }

    // Timestamp for new activity - by default "now", but may be set by client
    QDateTime now = QDateTime::currentDateTime();
    QDateTime activityTime = now;

    if (timestamp != "") {
        QDateTime newTime = QDateTime::fromString(timestamp,"yyyy-MM-ddTHH:mm:ss");
        // qDebug() << "Newtime: " << newTime.toString(Qt::ISODate) << "(" << timestamp << ")";
        if (newTime.isValid()) {
            activityTime = newTime;
        } else {
            if (timestamp != "now") {
                qWarning() << "Invalid timestamp: " << timestamp << ", using " << activityTime.toString(Qt::ISODate) << " instead.";
            }
            // We assume that activity is reported immediately after completion, unless timestamp has been specified
            if (duration > 0) {
                activityTime.addSecs(-duration);
            }
        }
    }

    QSqlQuery user_db_query (m_userDb);
    QString queryString;
    QString errorString;
    // fields: id, subject, source, type, application, timestamp
    queryString = "INSERT INTO Activities values(NULL, %1, %2, %3, %4, %5, %6)";

    if (!user_db_query.exec(queryString.arg(subject).arg(source).arg(type).arg(application).arg(activityTime.toTime_t()).arg(duration))) {
        qCritical() << "Unable to insert to Activities: " << user_db_query.lastError().text();
        QString errorString("error:unable_to_insert_activity/");
        errorString.append(user_db_query.lastError().text());
        return errorString;
    }
    QVariant rowId = user_db_query.lastInsertId();
    // qDebug() << "RowId type: " << rowId.typeName();
    int activityId = rowId.toInt(&ok);
    if (!ok) {
        qCritical() << "Illegal activityId assigned by db, aborting transaction";
        return "error:/internal_error/illegal_activity_id_assigned_by_db";
    }


    errorString = updateCounts(subject, now, activityTime, activity);
    if (errorString != "") {
        return errorString;
    }

    // Insert various annotations to DB
    for (QVariantMap::const_iterator i = activity.constBegin(); i != activity.constEnd(); i++) {
        QString annotationUri = i.key();
        QVariant annotationValue = i.value();
        QStringList annotationValues;
        if (annotationValue.type() == QVariant::String) {
            annotationValues << annotationValue.toString();
        } else if (annotationValue.type() == QVariant::StringList) {
            annotationValues = annotationValue.toStringList();
        } else {
            continue;
        }
        int annotationCode = ontology2code(annotationUri);
        queryString = "INSERT INTO Annotations values(NULL, %1, %2, '%3')";
        for(int i=0; i < annotationValues.size(); i++) {
            // qDebug() << "Storing annotation:" << annotationUri << "=" << annotationValues[i];
            // qDebug() << "Query: " << queryString.arg(activityId).arg(annotationCode).arg(sqlString(annotationValues[i]));
            if (!user_db_query.exec(queryString.arg (QString::number(activityId), QString::number(annotationCode), sqlString(annotationValues[i])))) {
                qCritical() << "Unable to insert to Annotations: " << user_db_query.lastError().text();
            }
        }
    }

    QString activityUriFormat = "activity:%1";
    QString activityUri = activityUriFormat.arg(activityId);
    emit activityAdded(subjectString);
    return activityUri;
}

QString RelevanceStorageService::addActivitySynchronously(QVariantMap activity) {
    if (!beginTransaction(m_userDb)) {
        qCritical() << "Database does not support transactions!";
        return QString("error:internal_error/transactions_not_supported");
    }

    QString result = writeActivityToDB(activity);

    if (result.startsWith("error:")) {
        if (!abortTransaction(m_userDb)) {
            qCritical() << "Unable to abort transaction, transaction will be left in unfinished state";
        }
    } else {
        if(!commitTransaction(m_userDb)) {
            qCritical() << "Unable to commit, trying to abort instead";
            result = "error:internal_error/unable_to_commit_transaction/transaction_aborted";
            if (!abortTransaction(m_userDb)) {
                qCritical() << "Unable to abort either, transaction will be left in unfinished state";
                result = "error:internal_error/unable_to_commit_transaction/unable_to_abort_transaction";
            }
        }
    }
    return result;
}


QStringList RelevanceStorageService::addActivitiesSynchronously(QVariantList activities) {
    QString result;
    QStringList allResults;
    // qDebug() << "insertActivitiesSynchronously(" << activities << ")";

    if (!beginTransaction(m_userDb)) {
        qCritical() << "Database does not support transactions!";
        allResults << "error:internal_error/transactions_not_supported";
        return allResults;
    }

    for(int i=0; i < activities.size(); i++) {
        // qDebug() << "Activity " << (i+1);
        // qDebug() << "Activity type: " << activities.at(i).typeName();
        QString typeName = activities.at(i).typeName();
        QVariantMap activity;
#if 0
        if (typeName == "QDBusArgument") { // Remote invocation
            QDBusArgument arg = activities.at(i).value<QDBusArgument>();
            if (arg.currentType() == QDBusArgument::MapType) {
                arg >> activity;
            } else {
                qCritical() << "Activity DBus type was not a map!";
                allResults << "error:activity_dbus_type_was_not_map";
                continue;
            }
        } else // Local invocation (unit test)
#endif
        {
            QVariant var = activities.at(i);
            if (var.type() == QVariant::Map) {
                activity = activities.at(i).toMap();
            } else {
                qCritical() << "Activity Variant type was not a map!";
                allResults << QString("error:activity_variant_type_was_not_map");
                continue;
            }
        }
        result = writeActivityToDB(activity);

        if (result.startsWith("error:")) {
            if (!abortTransaction(m_userDb)) {
                qCritical() << "Unable to abort transaction, transaction will be left in unfinished state";
            }
            allResults.clear();
            allResults << result;
            return allResults;
        }

        allResults << result;
    }

    if(!commitTransaction(m_userDb)) {
        qCritical() << "Unable to commit, trying to abort instead";
        result = "error:internal_error/unable_to_commit_transaction/transaction_aborted";
        if (!abortTransaction(m_userDb)) {
            qCritical() << "Unable to abort either, transaction will be left in unfinished state";
            result = "error:internal_error/unable_to_commit_transaction/unable_to_abort_transaction";
        }
        allResults.clear();
        allResults << result;
    }

    return allResults;
}

QVariantList RelevanceStorageService::allActivities() {
    return fetchActivitiesFor("");
}

QVariantList RelevanceStorageService::activitiesFor(const QString &subjectUri) {
    if (subjectUri == "") {
        return QVariantList();
    } else {
        return fetchActivitiesFor(subjectUri);
    }
}

QVariantList RelevanceStorageService::fetchActivitiesFor(const QString &subjectUri) {
    QString queryString =   "SELECT Activities.id, Subjects.uri, Activities.source, Activities.type, Activities.application,"
                            " Activities.timestamp, Activities.duration FROM Activities "
                            "JOIN Subjects ON Activities.subject=Subjects.id WHERE Subjects.uri='%1'";
    if (subjectUri == "") {
        QSqlQuery query ;

        //Amar query.exec ("SELECT id, uri from Subjects");
        queryString = "SELECT Activities.id, Subjects.uri, Activities.source, Activities.type, Activities.application,"
                      " Activities.timestamp, Activities.duration FROM Activities "
                      "JOIN Subjects ON Activities.subject=Subjects.id";
    } else {
        QSqlQuery query ;

        //Amar query.exec ("SELECT id, uri from Subjects WHERE uri = '%1'");
        queryString = queryString.arg(sqlString(subjectUri));
    }
    QSqlQuery user_db_query (m_userDb);
    if (!user_db_query.exec(queryString)) {
        qCritical() << "Unable to query: " << user_db_query.lastError().text();
        return QVariantList();
    }

    QVariantList result;

    while (user_db_query.next()) {
        QVariantMap activity;
        bool ok;
        int uriInt = user_db_query.value(0).toInt(&ok);
        if (!ok) {
            qWarning() << "Unable to read activity id";
            continue;
        }
        QString subject = user_db_query.value(1).toString();
        int sourceInt =  user_db_query.value(2).toInt(&ok);
        if (!ok) {
            qWarning() << "Unable to read activity source";
            continue;
        }
        QString source = code2ontology(sourceInt);
        int typeInt = user_db_query.value(3).toInt(&ok);
        if (!ok) {
            qWarning() << "Unable to read activity type";
            continue;
        }
        QString type = code2ontology(typeInt);
        int applicationInt = user_db_query.value(4).toInt(&ok);
        if (!ok) {
            qWarning() << "Unable to read activity application";
            continue;
        }
        QString application = "";
        if (applicationInt > 0) {
            application = code2ontology(applicationInt);
        }
        int timeInt = user_db_query.value(5).toInt(&ok);
        if (!ok) {
            qWarning() << "Unable to read activity time";
            continue;
        }
        QString uriTemplate="activity:%1";
        QDateTime time;
        time.setTime_t(timeInt);
        int duration = user_db_query.value(6).toInt(&ok);
        if (!ok) {
            qWarning() << "Unable to read activity duration (" << user_db_query.value(6).toString() << ")";
            continue;
        }
        activity.insert(RELEVANCESTORAGE_URI, uriTemplate.arg(uriInt));
        activity.insert(RELEVANCESTORAGE_TIMESTAMP, time.toString(Qt::ISODate));
        activity.insert(RELEVANCESTORAGE_DURATION, QString::number(duration));
        activity.insert(RELEVANCESTORAGE_SUBJECT, subject);
        activity.insert(RELEVANCESTORAGE_SOURCE, source);
        activity.insert(RELEVANCESTORAGE_TYPE, type);
        activity.insert(RELEVANCESTORAGE_APPLICATION, application);

        fillInAnnotations(uriInt, activity);

        result << activity;
    }
    return result;
}

QVariantMap RelevanceStorageService::activity(const QString &activityUri) {
    QVariantMap activity;
    if (!activityUri.startsWith("activity:") || activityUri.length() < 10) {
        activity.insert(RELEVANCESTORAGE_URI, "error:illegal_activity_uri");
        return activity;
    }
    bool ok;
    int activityId = activityUri.mid(9).toInt(&ok);
    if (!ok)  {
        activity.insert(RELEVANCESTORAGE_URI, "error:illegal_activity_uri");
        return activity;
    }


    QString queryString = "SELECT Activities.id, Subjects.uri, Activities.source, Activities.type, Activities.application,"
                          " Activities.timestamp, Activities.duration FROM Activities "
                          "JOIN Subjects ON Activities.subject=Subjects.id WHERE Activities.id=%1";
    QSqlQuery query;
    if (!query.exec(queryString.arg(activityId))) {
        qCritical() << "Unable to query for activity: " << query.lastError().text();
        QString errorString = "error:unable_to_query_for_activity/";
        errorString.append(query.lastError().text());
        activity.insert(RELEVANCESTORAGE_URI, errorString);
        return activity;
    }

    if (query.next()) {
        QVariantMap activity;
        bool ok;
        int uriInt = query.value(0).toInt(&ok);
        if (!ok) {
            qCritical() << "Unable to read activity id";
            activity.insert(RELEVANCESTORAGE_URI, "error:unable_to_read_activity_id");
            return activity;
        }
        QString subject = query.value(1).toString();
        int sourceInt =  query.value(2).toInt(&ok);
        if (!ok) {
            qCritical() << "Unable to read activity source";
            activity.insert(RELEVANCESTORAGE_URI, "error:unable_to_read_activity_source");
            return activity;
        }
        QString source = code2ontology(sourceInt);
        int typeInt = query.value(3).toInt(&ok);
        if (!ok) {
            qCritical() << "Unable to read activity type";
            activity.insert(RELEVANCESTORAGE_URI, "error:unable_to_read_activity_type");
            return activity;
        }
        QString type = code2ontology(typeInt);
        int applicationInt = query.value(4).toInt(&ok);
        if (!ok) {
            qCritical() << "Unable to read activity application";
            activity.insert(RELEVANCESTORAGE_URI, "error:unable_to_read_activity_application");
            return activity;        }
        QString application = "";
        if (applicationInt > 0) {
            application = code2ontology(applicationInt);
        }
        int timeInt = query.value(5).toInt(&ok);
        if (!ok) {
            qCritical() << "Unable to read activity time";
            activity.insert(RELEVANCESTORAGE_URI, "error:unable_to_read_activity_time");
            return activity;
        }
        QString uriTemplate="activity:%1";
        QDateTime time;
        time.setTime_t(timeInt);
        int duration = query.value(6).toInt(&ok);
        if (!ok) {
            qCritical() << "Unable to read activity duration (" << query.value(6).toString() << ")";
            activity.insert(RELEVANCESTORAGE_URI, "error:unable_to_read_activity_duration");
            return activity;
        }

        activity.insert(RELEVANCESTORAGE_URI, uriTemplate.arg(uriInt));
        activity.insert(RELEVANCESTORAGE_TIMESTAMP, time.toString(Qt::ISODate));
        activity.insert(RELEVANCESTORAGE_DURATION, QString::number(duration));
        activity.insert(RELEVANCESTORAGE_SUBJECT, subject);
        activity.insert(RELEVANCESTORAGE_SOURCE, source);
        activity.insert(RELEVANCESTORAGE_TYPE, type);
        activity.insert(RELEVANCESTORAGE_APPLICATION, application);

        fillInAnnotations(uriInt, activity);
        return activity;
    } else {
        activity.insert(RELEVANCESTORAGE_URI, "error:activity_not_found");
        return activity;
    }
}

void RelevanceStorageService::fillInAnnotations(int activityId, QVariantMap& activity) {
    QSqlQuery annotationQuery (m_userDb);
    QString queryString = "SELECT type, value FROM Annotations WHERE activity=%1";
    bool ok;
    if (annotationQuery.exec(queryString.arg(activityId))) {
        while(annotationQuery.next()) {
            int annotationType = annotationQuery.value(0).toInt(&ok);
            QString annotationValue = annotationQuery.value(1).toString();
            if (ok) {
                QString annotationUri = code2ontology(annotationType);
                if (annotationUri != "") {
                    activity.insertMulti(annotationUri, annotationValue);
                }
            }
        }
    } else {
        qWarning() << "Unable to query Annotations for activity:" << activityId << ": " << annotationQuery.lastError().text();
    }
}

double RelevanceStorageService::activityCountTotal(const QString &mode) {
    // qDebug() << "countTotal(" << mode << ")";
    ActivityCounter* counter = m_counters.value(mode, NULL);
    if (counter == NULL) {
        qCritical() << "Unknown counting mode " << mode;
        return -1.0;
    }
    return counter->convert(m_totals.value(mode, 0.0), m_totalsTimestamp, QDateTime::currentDateTime().toTime_t());
}

QVariantList RelevanceStorageService::activityCount(const QString &subjectUri, const QString &mode) {
    QSqlQuery query;
    QString queryString;

    QVariantList result;

    ActivityCounter* counter = m_counters.value(mode, NULL);
    if (counter == NULL) {
        qCritical() << "Unknown counting mode " << mode;
        return result;
    }

    uint now = QDateTime::currentDateTime().toTime_t();

    if (subjectUri == "") {
        //qDebug() << "Asking count for Empty URI, returning zero";
        result << subjectUri << (double) 0.0;
        result << "Total" << counter->convert(m_totals.value(mode, 0.0), m_totalsTimestamp, now);
        return result;
    }

    // qDebug() << "count(" << uri << ", " << mode << ")";

    queryString = "SELECT Subjects.c_%2,Subjects.timestamp FROM Subjects WHERE Subjects.uri='%1'";

    double oldValue = 0;
    uint oldTime = 0;
    if (query.exec(queryString.arg(subjectUri).arg(mode))) {
        if (query.next()) {
            oldValue = query.value(0).toDouble(NULL);
            oldTime = query.value(1).toInt(NULL);
            result << subjectUri << counter->convert(oldValue, oldTime, now);
        } else {
            result << subjectUri << (double) 0.0;
        }
        result << "Total" << counter->convert(m_totals.value(mode, 0.0), m_totalsTimestamp, now);
    } else {
        qCritical() << "Unable to query for count: " << query.lastError().text();
        QString errorString = "error:unable_to_query_count/";
        errorString.append(query.lastError().text());
        result << errorString;
    }
    return result;
}

QVariantList RelevanceStorageService::activityCounts(const QString &mode) {
    QSqlQuery query;
    QString queryString = "SELECT Subjects.uri,Subjects.c_%1,Subjects.timestamp FROM Subjects";
    QVariantList result;

    // qDebug() << "countAll(" << mode << ")";

    ActivityCounter* counter = m_counters.value(mode, NULL);
    if (counter == NULL) {
        qCritical() << "Unknown counting mode " << mode;
        QString errorString = "error:unknown_counting_mode/";
        errorString.append(mode);
        result << errorString;
        return result;
    }

    if (!query.exec(queryString.arg(mode))) {
        qCritical() << "Unable to query: " << query.lastError().text();
        QString errorString = "error:unable_to_query_counts/";
        errorString.append(query.lastError().text());
        result << errorString;
        return result;
    }

    uint now = QDateTime::currentDateTime().toTime_t();

    while (query.next()) {
        QString uri = query.value(0).toString();
        double oldValue = query.value(1).toDouble(NULL);
        uint oldTime = query.value(2).toInt(NULL);

        result << uri << counter->convert(oldValue, oldTime, now);
    }
    result << "Total" << counter->convert(m_totals.value(mode, 0.0), m_totalsTimestamp, now);
    return result;
}

void RelevanceStorageService::randomCleanup() {
    // Randomly do cleanup after a one second delay
    // Randomness should quarantee that we don't always do it at some inconvenient time
    // TODO: This would be better done when the phone is idle and on battery
    // e.g. we could expose cleanup on dbus interface and create a small tool for calling it
    // from the "Context Chron" when it becomes available.
    uint r = qrand() % 1000000;
    if (r < CLEANUP_CHANCE) {
        QTimer::singleShot(1000, this, SLOT(cleanup()));
    }
}

void RelevanceStorageService::dataUpdated(const QSet<QString> &added, const QSet<QString> &removed, const QSet<QString> &changed, const QDateTime &lastUpdate)
{
    qDebug() << "dataUpdated:";
    qDebug() << "  - added:" << added;
    qDebug() << "  - removed:" << removed;
    qDebug() << "  - changed:" << changed;

    AbstractHarvester *harvester = (AbstractHarvester*)sender();

    beginTransaction();
    foreach(QString uri, removed) {
        removeSubject( uri );
    }
    foreach(QString uri, added) {
        addSubject( uri, harvester );
    }
    // FIX THIS, add activities for changed items    addActivities(changed);
    
    if (!commitTransaction()) {
        abortTransaction();
        // TODO: Retry would require saving subjects in some local thingy. Is it required?
        //resetTimer(); // If at first you don't succeed...
        return;
    }
    
    markUpdate(lastUpdate);
    cleanCache();
    QSet<QString> addedPruned = pruneCached(added);
    QSet<QString> changedPruned = pruneCached(changed);
    QStringList uriList;
    uriList += QStringList(addedPruned.toList());
    uriList += QStringList(changedPruned.toList());

    emit storageDataUpdated( uriList );
}



/********************** Simple API *************************/

void RelevanceStorageService::addActivity(const QString &subjectUri) {
    addActivity(subjectUri, "", "", "", -1, "");
    randomCleanup();
}

bool RelevanceStorageService::addActivitySynchronously(const QString &subjectUri) {
    QString result = addActivitySynchronously(subjectUri, "", "", "", -1, "");
    randomCleanup();
    return ! result.startsWith("error:");
}

QVariantList RelevanceStorageService::activityCount(const QString &subjectUri)  {
    return activityCount(subjectUri, DEFAULT_COUNTER);
}

QVariantList RelevanceStorageService::activityCounts() {
    return activityCounts(DEFAULT_COUNTER);
}

// Mark current time as last update received from tracker
bool RelevanceStorageService::markUpdate(QDateTime time) {
    QSqlQuery query;
    m_lastUpdate = time.toTime_t();
    QString queryString = "UPDATE State SET last_update=%1 WHERE id=1";
    if (!query.exec(queryString.arg(m_lastUpdate))) {
        return false;
    }
    return true;
}



// Get timestamp of last markUpdate as xsd:date string
QString RelevanceStorageService::lastUpdate() {
    QDateTime time;
    time.setTime_t(m_lastUpdate);
    return time.toString(Qt::ISODate);
}


int RelevanceStorageService::recSubClassDepth(const QHash<QString, QString> & classHierarchy, const QString & subClass) {
    QList<QString> subClasses = classHierarchy.values(subClass);

    int subDepth = 0;
    for(int j=0; j<subClasses.size(); j++) {
        int depth = recSubClassDepth(classHierarchy, subClasses[j]) + 1;
        if (depth > subDepth) {
            subDepth = depth;
        }
    }

    return subDepth;
}



bool RelevanceStorageService::registerClasses( QVector<QStringList> classes) 
{
    QSqlQuery query;

    QHash<QString, QString> classHierarchy;
    for(int i=0; i<classes.size(); i++) {
        classHierarchy.insertMulti(classes[i][0], classes[i][1]);
    }

    QHash<QString, int> classDepths;
    for(int i=0; i<classes.size(); i++) {
        int depth = recSubClassDepth(classHierarchy, classes[i][0]);
        classDepths.insert(classes[i][0], depth);
    }

    beginTransaction();
    QHashIterator<QString, int> it(classDepths);

    int added = 0;
    int existing = 0;
    int error = 0;
    while (it.hasNext()) {
        it.next();

        if( m_classUris.contains(it.key()) ) {
            existing++;
            continue;
        }

        QString queryString = "INSERT INTO ClassHierarchy (class_uri, depth, link_count, relevance) VALUES ('%1', %2, 0, 0.0)";
        if (!query.exec(queryString.arg(it.key(), QString::number(it.value())))) {
            qCritical() << "Unable to initialize class hierarchy for" << it.key() << ": " << query.lastError().text();
        }
        QVariant id = query.lastInsertId();
        bool ok;
        int classId = id.toInt(&ok);
        if (ok) {
            added++;
            m_classUris.insert(it.key(), classId);
        } else {
            error++;
            qCritical() << "Unable to get class id for " << it.key();
        }
    }

    if (!commitTransaction()) {
        qCritical() << "Transaction for adding class hierarchy failed!";
        abortTransaction();
        return false;
    }

    qDebug() << "Registering classes, existing:" << existing << "new:" << added << "errors:" << error;

    return true;
}

bool RelevanceStorageService::loadStaticData() {
    QSqlQuery query;
    if (!query.exec("SELECT id, class_uri, depth FROM ClassHierarchy")) {
        qCritical() << "Unable to read ClassHierarchy table: " << query.lastError().text();
        return false;
    }

    while(query.next()) {
        bool ok;
        int classId = query.value(0).toInt (&ok);
        QString class_uri = query.value(1).toString();

        m_classUris.insert(class_uri, classId);
    }

    // We are not interested in the properties... as yet, anyway.

    return true;
}

bool RelevanceStorageService::updateRecentChanges( QStringList recentChanges, AbstractHarvester *harvester ) 
{
    QTime t;
qDebug () << " No fo Changes " << recentChanges.size();
    t.start();
    beginTransaction();
    int added = 0;
    for(int i=0;i<recentChanges.size();++i) {
        const QString &uri = recentChanges.at(i) ;
       
        // if its really older than lastupdate then only add - To avoid recurcive adds
        if (timeStamp(uri) <= m_lastUpdate) {
            added++;
            addSubject (uri, harvester);
        }
    }
    if (!commitTransaction()) {
        qCritical() << "Transaction for adding subjects failed while updating recent changes!";
        abortTransaction();
        return false;
    }
    qDebug() << "Updating recent changes, changes:" << recentChanges.size() << "actual changes:" << added;
    qDebug() << "Saved objects to database, time:" << t.elapsed();
    markUpdate(QDateTime::currentDateTime());

    return true;
}


int RelevanceStorageService::dump(QSqlDatabase &db) {
    QTextStream cout(stdout);
    QSqlQuery query(db);
    QStringList tables = db.tables();

    for(int i=0; i < tables.size(); i++) {
        QString queryString("SELECT * FROM %1");

        if (!query.exec(queryString.arg(tables.at(i)))) {
            qDebug() << "Unable to query: " << query.lastError().text();
            return -1;
        }

        cout << endl << "Table " << tables.at(i) << ": " << endl;
        QSqlRecord rec = query.record();
        for(int i=0; i< rec.count(); i++) {
            cout << rec.fieldName(i) << "\t";
        }
        cout << endl << "====================================" << endl;
        while (query.next()) {
            for(int i=0; i < rec.count(); i++) {
                cout << query.value(i).toString() << "\t";
            }
            cout << endl;
        }

    }

    return 0;
}

QVector<QVariantMap> RelevanceStorageService::query(const QString &queryString) const
{
    QVector<QVariantMap> detailsList ;

    enum {
        CLASS_URI = 0, 
        URI, 
        DETAILS,
        GROUP_LABEL = DETAILS,
        RELEVANCE,
        IS_GROUP
    };
 
    qDebug () << queryString ;

    QSqlQuery query (queryString);
    if (!query.exec ()) {
        qCritical () << "Unable to execute search query : " << query.lastError().text() ;
        return detailsList;
    }

    int size = 0;
    bool isValid = query.first();
    while (isValid) {
        QVariantMap details ;
        bool isGroup = query.value(IS_GROUP).toBool();

        details.insert ("isGroup", isGroup);

        if (isGroup) // Groups
        {
            details.insert ("defaultLabel", query.value(GROUP_LABEL).toString());
        }
        else
        { 
            QByteArray data = query.value(DETAILS).toByteArray() ;
            QBuffer buffer(&data);
            QDataStream ds(&buffer);

            buffer.open(QIODevice::ReadOnly);
            ds >> details ;
        }
        details.insert ("rdfs:type", query.value(CLASS_URI).toString()) ;
        details.insert ("uri", query.value(URI).toString()) ;
        details.insert ("relevanceValue", query.value(RELEVANCE).toDouble());

        detailsList.append (details);

        isValid = query.next() ;
        size++;
    }
 
    return detailsList ;
}

QVector<QVariantMap> RelevanceStorageService::doSearch (const QString &text, const QStringList &content_types, const QStringList &content_sources, bool grouping_flag) const
{
    Q_UNUSED (content_sources);

    QString queryString;

    queryString.append ("SELECT c.class_uri, s.uri, s.details, s.relevance as relevance, \'false\'"
                        " FROM Subjects as s, ClassHierarchy as c WHERE");

    // add fts match statement
    if (!text.isEmpty()) {
        if (SqliteFTSenabled)
        	queryString.append(" s.id IN (SELECT id FROM fts_table WHERE content MATCH '*%1*') and");
        else
        	queryString.append(" s.id IN (SELECT id FROM fts_table WHERE content LIKE '%%1%') and");
        queryString = queryString.arg (text);
    }
    
    queryString.append (" c.id=s.class");

    if (content_types.size()) {
        queryString.append (" AND c.id IN (");
        int i=0;
        for (; i<content_types.size()-1; i++) {
            queryString.append (QString::number (m_classUris.value (content_types.at(i), 0)));
            queryString.append (",");
        }
        queryString.append (QString::number (m_classUris.value (content_types.at(i), 0)));
        queryString.append (")");
    }

    QVector<QVariantMap> detailsList ;

    // groups
    // TODO: Add max(relevance) for Groups where group relevance is the relevance of the highest item inside group
    if (grouping_flag) {
        queryString.append (" UNION SELECT class, uri, primary_label, \'0.0\' as relevance, \'true\'"
                        " FROM Groups WHERE groupID IN (SELECT groupID FROM ItemGroups");
        if (!text.isEmpty()) {
            if (SqliteFTSenabled)
            	queryString.append (" WHERE itemID IN (SELECT id FROM fts_table WHERE content MATCH '*%1*')");
            else
            	queryString.append (" WHERE itemID IN (SELECT id FROM fts_table WHERE content LIKE '%%1%')");
            queryString = queryString.arg (text);
        }
        queryString.append (")");
    }

    // ordering
    queryString.append (" ORDER BY relevance") ;

   return query(queryString);
}

QVector<QVariantMap> RelevanceStorageService::doSearch (const QStringList &uris, const QStringList &content_types, const QStringList& content_sources) const
{
    Q_UNUSED (content_types);
    Q_UNUSED (content_sources);

    QString queryString  = "SELECT c.class_uri, s.uri, s.details, s.relevance, 'false' " 
                           "from Subjects as s, ClassHierarchy as c " ;

    if (!uris.isEmpty()) {
        QString uriFilter = uris.join ("' or uri is '");
	    queryString = queryString + "WHERE (uri is '" + uriFilter + "') ";
    }
    queryString.append (" and c.id = s.class ");
    queryString.append ("ORDER BY s.relevance");

    return query (queryString);
}

QVector<QVariantMap> RelevanceStorageService::doSearch (const QString& groupIdentifier, const QString& text, const QStringList& content_types,
                                                        const QStringList& content_sources, bool grouping_flag)
{
    Q_UNUSED (text);
    Q_UNUSED (content_types);
    Q_UNUSED (content_sources);
    Q_UNUSED (grouping_flag);

    QString queryString;
    // get all items under group, no other filter used at the moment.
    queryString.append ("SELECT c.class_uri, s.uri, s.details, s.relevance, 'false' FROM Subjects as s,"
                        " ClassHierarchy as c WHERE s.id IN (SELECT itemID FROM ItemGroups WHERE groupId=(SELECT groupId from Groups WHERE uri='%1'))");
    queryString.append (" and c.id=s.class ORDER BY s.relevance");

    queryString = queryString.arg (groupIdentifier);
    return query (queryString);
}

void RelevanceStorageService::cleanCache() {
    qDebug() << "Cleaning cache...";
    uint now = QDateTime::currentDateTime().toTime_t();
    QMutableHashIterator<QString, uint> i(m_cache);
    while(i.hasNext()) {
        i.next();
        uint timestamp = i.value();
        if ((now - timestamp) > (uint) m_cacheTime) {
            qDebug() << "     - removing " << i.key();
            i.remove();
        }
    }
}

QSet<QString>  RelevanceStorageService::pruneCached(QSet<QString> uriList) {
    QSet<QString>  prunedList;
    uint now = QDateTime::currentDateTime().toTime_t();
    foreach(QString uri, uriList) {
        if (m_cache.contains(uri)) {
            qDebug() << "Already in cache, ignored: " << uri;
            // Update timestamp.
            // There has to be at least m_cacheTime interval between accesses
            // before event is counted again.
            m_cache.remove(uri);
            m_cache.insert(uri, now);
        } else {
            prunedList << uri;
            m_cache.insert(uri, now);
        }
    }
    return prunedList;
}
