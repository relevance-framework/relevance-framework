/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef NODECAYCOUNTER_H
#define NODECAYCOUNTER_H

#include "ActivityCounter.h"

class NoDecayCounter: public ActivityCounter
{
public:
    /**
     * \brief Defines the name of the algorithm
     *
     * \returns the name of the algorithm, i.e. "NoDecay"
     */
    virtual QString name() { return "NoDecay"; }
protected:
    /**
     * \brief Convert an activity frequency score from one time to another.
     *
     * This implements the algorithm newValue = oldValue.
     * Use of this algorithm is not recommended, because it will lead to counter score changes
     * after periodical database cleanup. The cleanup relies on decay to remove the importance of old scores,
     * without decay old scores will not be approximately zero and thus removing them will affect scores.
     *
     * \param oldValue the original score
     * \param oldTime the timestamp of the original score
     * \param newTime the timestamp to which the original score score should be translated
     * \returns the score in newTime currency.
     */
    virtual double doConvert(double oldValue, uint /* oldTime */, uint /* newTime */) {
        return oldValue;
    }
};


#endif // NODECAYCOUNTER_H
