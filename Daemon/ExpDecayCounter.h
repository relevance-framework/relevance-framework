/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef EXPDECAYCOUNTER_H
#define EXPDECAYCOUNTER_H

#include "ActivityCounter.h"

// Any score older than this many periods is considered to be zero
#define MAX_PERIODS 100.0


/**
 * \brief An exponential decay algorithm.
 *
 * This class implements a decay algorithm where the value is halved after a certain time period.
 * To be more exact, newValue = pow(0.5, periods) * oldValue;
 * In other words, the value is 0.5 * oldValue after one period, 0.25 * oldValue after two periods, and so on.
 */


class ExpDecayCounter: public ActivityCounter
{
public:
    /**
     * \brief Constructor
     *
     *
     * \param name name of the algorithm (used as the mode parameter when using the Activity Tracker service)
     * \param period the half-life for the decay function
     */
    ExpDecayCounter(QString name, int period): m_name(name),m_period(period) {}
    /**
     * \brief Defines the name of the algorithm
     *
     * \returns the name of the algorithm
     */
    virtual QString name() { return m_name; }
protected:
    /**
     * \brief Convert an activity frequency score from one time to another.
     *
     * This implements the algorithm newValue = pow(0.5, periods) * oldValue.
     *
     * \param oldValue the original score
     * \param oldTime the timestamp of the original score
     * \param newTime the timestamp to which the original score score should be translated
     * \returns the score in newTime currency.
     */
    virtual double doConvert(double oldValue, uint oldTime, uint newTime) {
        double periods = ((double) (newTime - oldTime)) / m_period;
        if (periods > MAX_PERIODS) {
            return 0.0;
        }
        return pow(0.5, periods) * oldValue;
    }
private:
    //! name of the algorithm (= mode parameter in Activity Tracker service API)
    QString m_name;
    //! Half-life period of the decay
    int m_period;
};

#endif // EXPDECAYCOUNTER_H
