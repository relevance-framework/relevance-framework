/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <QTextStream>
#include <QCoreApplication>
#include <QStringList>
#include <QDebug>
#include <QVariant>
#include <QDateTime>
#include <errno.h>

#include "RelevanceDaemon.h"
#ifdef QT_DBUS_LIB
#include "RelevanceDaemonIfAdaptor.h"
#else
#include "ServerSocket.h"
#endif

// Minimum time between activities for same URL (seconds)
#define CACHE_TIME 10
// Maximum time full relevance calculation may take (milliseconds)
#define TIMEOUT_FULL 300000
// Maximum time incremental relevance calculation may take (milliseconds)
#define TIMEOUT_INCREMENTAL 30000
// Time before we do first calc (i.e. time after program start, presumable done at boot time) (1min)
#define TIME_FIRST_FULL_CALC 6000
// Time between calcs after the first calc (24h)
#define TIME_BETWEEN_CALCS 86400000
// Time used for sampling idleness information (i.e. between the two checks to /proc/uptime) (10s)
#define TIME_IDLE_SAMPLE 10000
// Time between idleness checks (assuming that battery is charging or full) (10 min)
#define TIME_WAIT_FOR_IDLE 600000
// Time to wait for rechecking idle when doing first calc (10s)
#define TIME_FIRST_WAIT_FOR_IDLE 10000
// Idleness percentage lower limit for performing relevance calc (1-99)
#define IDLE_LIMIT 90
// Time to wait between retries to register for tracker events
#define TIME_REGISTRATION_RETRY 10000
// How many times we try to register for tracker events
#define REGISTRATION_RETRIES 5
// Default database file location
#define DEFAULT_DB_FILE "/.cache/search/search.db"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    QStringList arguments = QCoreApplication::arguments();
    int cacheTime = CACHE_TIME;
    int timeoutFull = TIMEOUT_FULL;
    int timeoutIncremental = TIMEOUT_INCREMENTAL;
    int timeFirstFullCalc = TIME_FIRST_FULL_CALC;
    int timeBetweenCalcs = TIME_BETWEEN_CALCS;
    int timeIdleSample = TIME_IDLE_SAMPLE;
    int timeWaitForIdle = TIME_WAIT_FOR_IDLE;
    int timeFirstWaitForIdle = TIME_FIRST_WAIT_FOR_IDLE;
    int idleLimit = IDLE_LIMIT;
    int registrationRetryDelay = TIME_REGISTRATION_RETRY;
    int registrationRetries = REGISTRATION_RETRIES;

    QString dbFile;
    const char* env_dbFile = getenv("RELEVANCESTORAGE_DB_FILE");
    if (env_dbFile != NULL) {
        dbFile = env_dbFile;
        if (dbFile == "") {
            dbFile = QString(getenv("HOME")) + DEFAULT_DB_FILE;
        }
    } else {
        dbFile = QString(getenv("HOME")) + DEFAULT_DB_FILE;
    }

    RelevanceDaemon daemon(dbFile, cacheTime, timeoutFull, timeoutIncremental,
                           timeFirstFullCalc, timeBetweenCalcs,
                           timeIdleSample, timeWaitForIdle, timeFirstWaitForIdle, idleLimit,
                           registrationRetryDelay, registrationRetries);
    if (daemon.initialize() < 0) {
        qCritical() << "Failed to initialize Relevance Daemon!";
        exit (EXIT_FAILURE);
    }

#ifdef QT_DBUS_LIB
    // set up a DBus adaptor for the engine
    RelevanceDaemonIfAdaptor adaptor(&daemon);
    if ( ! QDBusConnection::sessionBus().registerObject ("/", &daemon) ) {
        qCritical() << "failed to register DBus adaptor:" << QDBusConnection::sessionBus().lastError().message();
        exit (EXIT_FAILURE);
    }

    // register our name on the bus so that others can use it
    QDBusConnection::sessionBus().registerService("com.nokia.RelevanceDaemon");
    qDebug() << "DBus service registered";
#else
	ServerSocket adaptor("com.nokia.RelevanceDaemon", &daemon);
    qDebug() << "Listening on socket ...";
#endif
    if (arguments.length() > 1 && arguments[1] == "-dump") {
        qDebug() << "Dumping database:";
        daemon.dump();
    }

    return app.exec();
}
