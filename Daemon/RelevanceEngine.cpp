/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "RelevanceEngine.h"
#include "RelevanceGraph.h"
#include "RelevanceCalc.h"
#include "ContentStorage.h"
#include "ActivityPersonalization.h"

#include <cmath>
#include "ExpDecayCounter.h"

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QVariant>

#include <QDateTime>

#include <QDebug>

RelevanceEngine::RelevanceEngine() {
    this->normalizeTo = 1000000;

    this->personalization_d = 0.2;
    this->aging_d = 0.05;

    this->random_d = 0.05;
}

RelevanceEngine::~RelevanceEngine() {
    // do nothing
}

void RelevanceEngine::calculateFully(bool save) {
    QTime t;
    t.start();
    qDebug() << "RelevanceEngine: calculateFully started.";

    ContentStorage contentStorage;
    contentStorage.loadAllContent();

    RelevanceGraph *graph = contentStorage.graph();

    CalculationState state;
    
    state.setLastNodeCount(graph->nodeCount());

    ActivityPersonalization activityPersonalization;

    double normalizeActivity = normalizeTo * personalization_d / (personalization_d + aging_d);
    double count = activityPersonalization.normalize(graph, normalizeActivity);
    state.setLastActivity(count);
    
    uint referenceTime = QDateTime::currentDateTime().toTime_t();
    state.setLastFullTimestamp(referenceTime);
    
    double normalizeAging = normalizeTo * aging_d / (personalization_d + aging_d);
    double sumAges = activityPersonalization.personalize(graph, normalizeAging, referenceTime);
    state.setLastAgesSum(sumAges);
    
    if (count == 0) {
        random_d += personalization_d;
        personalization_d = 0;
    }
    
    RelevanceCalc relevanceCalculator;
    
    int lastDanglingNodeCount = 0;
    double lastDanglingNodeActivity = 0.0;
    
    relevanceCalculator.personalizedRank(graph, 
                                         personalization_d+aging_d,
                                         random_d, 
                                         normalizeTo, 
                                         &lastDanglingNodeCount, 
                                         &lastDanglingNodeActivity);
    
    state.setLastDanglingNodeCount(lastDanglingNodeCount);
    state.setLastDanglingNodeActivity(lastDanglingNodeActivity);
    
    if (save) {
        contentStorage.saveAllContent();
        contentStorage.setState(state);
    }

    qDebug() << "RelevanceEngine: calculateFully ended in " << t.elapsed() << " milliseconds.";
    
}

void RelevanceEngine::calculateIncremental(const QStringList &URIs, bool save)
{
    QTime t;
    t.start();
    qDebug() << "RelevanceEngine: calculateIncremental started.";
    
    ContentStorage contentStorage;
    QList<int> items = contentStorage.loadSelectedContent(URIs);
    
    RelevanceGraph *graph = contentStorage.graph();
    
    CalculationState state = contentStorage.getState();

    ActivityPersonalization activityPersonalization;

    double lastActivity = state.lastActivity();

    if (lastActivity > 0) {
        for(int i=0; i<items.size(); i++) {
            graph->setPersonalization(items.at(i), graph->personalization(items.at(i)) / lastActivity);
        }
    }

    for(int i=0; i<items.size(); i++) {
        if (lastActivity == 0 && graph->personalization(items.at(i) == 0)) {
            random_d += personalization_d;
            personalization_d = 0;
        }
    }
    
    for(int i=0; i<items.size(); i++) {
        uint timestamp = graph->modificationTime(items.at(i));
        uint age = (timestamp > state.lastFullTimestamp()) ?  0 : state.lastFullTimestamp() - timestamp;
        double aging = activityPersonalization.agingFunc(age);
        
        double personalizationCombined  = 0.0;
        if (aging_d > 0.0 || personalization_d > 0.0)  {
            double normalizeAging = (aging_d / (personalization_d + aging_d) ) * normalizeTo;
            double normalizePersonalization = (personalization_d / (personalization_d + aging_d) ) * normalizeTo;
            double agingWeight = aging/ state.lastAgesSum() * normalizeAging;
            personalizationCombined = graph->personalization(items.at(i)) * normalizePersonalization + agingWeight;
        }

        graph->setPersonalization(items.at(i), personalizationCombined);
    }
    
    RelevanceCalc relevanceCalculator;
    relevanceCalculator.simpleIncremental(graph,
                                          items,
                                          state.lastNodeCount(), state.lastDanglingNodeCount(),
                                          personalization_d + aging_d, random_d, normalizeTo,
                                          state.lastDanglingNodeActivity());

    if (save) {
        contentStorage.saveSelectedContent(items);
    }

    qDebug() << "RelevanceEngine: calculateIncremental ended in " << t.elapsed() << " milliseconds.";
    
}

static QString & sqlString(QString s) {
    return s.replace("'", "''");
}

QList<int> getLinkCounts(QList<QString> subjectUris) {
    QList<int> ret;
    QString queryString = "SELECT link_count FROM Subjects WHERE uri = '%1'";
    QSqlQuery query;

    for(int i=0; i<subjectUris.size(); i++) {
        
        if (!query.exec(queryString.arg(sqlString(subjectUris.at(i))))) {
            qCritical() << "Unable to count query: " << query.lastError().text();
            ret.append(0);
            continue;
        }
        
        if (query.next()) {
            ret.append(query.value(0).toInt());
        } else {
            ret.append(0);
        }
    }
    
    return ret;
}
 
QVariantList activityCount(const QString &subjectUri) {
    QSqlQuery query;
    QString queryString;

    QVariantList result;

    ExpDecayCounter counter("ExpDecay_2weeks", TWO_WEEKS_IN_SECONDS);

    double m_totals = 0.0;
    uint m_totalsTimestamp = 0;
    
    if (!query.exec("SELECT timestamp, c_ExpDecay_2weeks FROM Totals WHERE id=1")) {
        qCritical() << "Unable to retrieve totals: " << query.lastError().text();
        return QVariantList();
    }
    if (query.next()) {
        bool ok;
        m_totals = query.value(0).toDouble(&ok);
        if (!ok) {
            qCritical() << "Illegal total in totals table";
            return QVariantList();
        }
        m_totalsTimestamp = query.value(1).toUInt(&ok);
        if (!ok) {
            qCritical() << "Illegal timestamp in totals table";
            return QVariantList();
        }
    }
    
    uint now = QDateTime::currentDateTime().toTime_t();

    if (subjectUri == "") {
        //qDebug() << "Asking count for Empty URI, returning zero";
        result << subjectUri << (double) 0.0;
        result << "Total" << counter.convert(m_totals, m_totalsTimestamp, now);
        return result;
    }

    // qDebug() << "count(" << uri << ", " << mode << ")";

    queryString = "SELECT Subjects.c_%2,Subjects.timestamp FROM Subjects WHERE Subjects.uri='%1'";

    double oldValue = 0;
    uint oldTime = 0;
    if (query.exec(queryString.arg(subjectUri).arg("ExpDecay_2weeks"))) {
        if (query.next()) {
            oldValue = query.value(0).toDouble(NULL);
            oldTime = query.value(1).toInt(NULL);
            result << subjectUri << counter.convert(oldValue, oldTime, now);
        } else {
            result << subjectUri << (double) 0.0;
        }
        result << "Total" << counter.convert(m_totals, m_totalsTimestamp, now);
    } else {
        qCritical() << "Unable to query for count: " << query.lastError().text();
        QString errorString = "error:unable_to_query_count/";
        errorString.append(query.lastError().text());
        result << errorString;
    }
    return result;
}

QVariantList activityCounts() {
    QSqlQuery query;
    QString queryString = "SELECT Subjects.uri,Subjects.c_%1,Subjects.timestamp FROM Subjects";
    QVariantList result;

    ExpDecayCounter counter("ExpDecay_2weeks", TWO_WEEKS_IN_SECONDS);

    double m_totals = 0.0;
    uint m_totalsTimestamp = 0;
    
    if (!query.exec("SELECT timestamp, c_ExpDecay_2weeks FROM Totals WHERE id=1")) {
        qCritical() << "Unable to retrieve totals: " << query.lastError().text();
        return QVariantList();
    }
    if (query.next()) {
        bool ok;
        m_totals = query.value(0).toDouble(&ok);
        if (!ok) {
            qCritical() << "Illegal total in totals table";
            return QVariantList();
        }
        m_totalsTimestamp = query.value(1).toUInt(&ok);
        if (!ok) {
            qCritical() << "Illegal timestamp in totals table";
            return QVariantList();
        }
    }
    
    if (!query.exec(queryString.arg("ExpDecay_2weeks"))) {
        qCritical() << "Unable to query: " << query.lastError().text();
        QString errorString = "error:unable_to_query_counts/";
        errorString.append(query.lastError().text());
        result << errorString;
        return result;
    }

    uint now = QDateTime::currentDateTime().toTime_t();

    while (query.next()) {
        QString uri = query.value(0).toString();
        double oldValue = query.value(1).toDouble(NULL);
        uint oldTime = query.value(2).toInt(NULL);

        result << uri << counter.convert(oldValue, oldTime, now);
    }
    result << "Total" << counter.convert(m_totals, m_totalsTimestamp, now);
    return result;
}

double RelevanceEngine::agingFunc(uint ageInSecs) {
    Q_UNUSED (ageInSecs)
    //    return pow(2.0, -((double)ageInSecs)/ ((double)(this->ageHalflife)));
    return 0;
}

