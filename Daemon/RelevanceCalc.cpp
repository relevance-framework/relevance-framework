/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "RelevanceCalc.h"
#include "RelevanceGraph.h"
#include <unistd.h>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <QDebug>

RelevanceCalc::RelevanceCalc() 
    : m_iterations(30)
{
    // do nothing
}

RelevanceCalc::~RelevanceCalc() 
{
    // do nothing
}

int RelevanceCalc::iterations() const
{
    return m_iterations;
}

void RelevanceCalc::setIterations(int value)
{
    m_iterations = value;
}

/* To calculate the dangling relevance
relD = normRand * countD / Ln + (personalization_d) * inboundD  + damping * relD * countD / Ln;
relD -  damping * relD * countD / Ln = normRand * countD / Ln + (personalization_d) * inboundD
relD (1 - damping * countD / Ln) = normRand * countD / Ln + (personalization_d) * inboundD
relD =( normRand * countD / Ln + (personalization_d) * inboundD) / (1 - damping * countD / Ln)

r == n (c/l) + p I + d r (c/l)  <==>  r = (-c n-i l p)/(c d-l)
*/

void RelevanceCalc::personalizedRank(RelevanceGraph* graph, 
                                     double personalization_d, double random_d, double normalizeTo, 
                                     int *retDanglingNodeCount, double *retDanglingNodeInbound) const 
{
    int Ln = graph->nodeCount();
    double normalizedRandom = random_d * normalizeTo;
    qDebug() << "Relevance calc started for RelevanceGraph size:" << Ln;
    
    double damping = (double)1.0 - personalization_d - random_d;
    
    // dangling nodes are thought to have a link to every node (even itself) in the system
    // we don't want to create these links (would be too slow), therefore they are handled separately
    // first search for the  dangling nodes
    std::vector<int> isDanglingNode;
    int danglingCount = 0;
    double danglingNodeInbound = 0.0;
    int maxIndegree = 0;
    
    // this process now relies on bidirectional links, which means that dangling nodes have
    // always zero in and out links
    for (int i = 0 ; i < Ln; i++) {
        int inDegree = graph->countEdges(i);
        if (inDegree == 0) {
            isDanglingNode.push_back(1);
            danglingCount ++;
            if (personalization_d > 0.0) danglingNodeInbound += graph->personalization(i);
        }
        isDanglingNode.push_back(0);
        if (inDegree > maxIndegree) maxIndegree= inDegree;
    }
    qDebug() << "maxIndegree:" << maxIndegree << ". dangling nodecount:" << danglingCount;
    
    double danglingNodeRelevance = ( normalizedRandom * (double)danglingCount 
                                     / (double)Ln + (personalization_d) * danglingNodeInbound )
        / ( 1 - damping * (double)danglingCount / (double)Ln );
    
    if (retDanglingNodeCount) *retDanglingNodeCount = danglingCount;
    if (retDanglingNodeInbound) *retDanglingNodeInbound = danglingNodeInbound;
    
    // initialize to 1/n
    for (int i = 0; i < Ln; i++) graph->setRelevance(i, (double)1.0 / (double)Ln);

    for (int iteration = 0; iteration < m_iterations; iteration++) {
        for (int i = 0 ; i < Ln; i++) {
            if (isDanglingNode[i] == 1) {
                // this is only run at first iteration
                if ( iteration == 0) {
                    // since we expect bidirectional links always, any dangling node is completely unconnected
                    // and we can compute a global dangling node relevance value
                    // FIXME: add the math statement that we are calculating here
                    double individualsum = danglingNodeRelevance  / (double)Ln;
                    if (personalization_d > 0.0) {
                        double pers = graph->personalization(i);
                        graph->setRelevance(i, normalizedRandom / (double)Ln + (personalization_d) * pers + damping * individualsum);
                    } else
                        graph->setRelevance(i, normalizedRandom / (double)Ln +damping * individualsum);
                }
            }  else {
                // FIXME: add the math statement that we are calculating here
                double sum = 0;
                QSetIterator<int> edges = graph->edgesAt(i);
                while (edges.hasNext()) {
                    int fromIndex =  edges.next();
                    double fromRel = graph->relevance(fromIndex);
                    int fromOutboundcount = graph->countEdges(fromIndex);
                    sum += fromRel / (double)fromOutboundcount;
                }

                // we simulate dangling nodes having a link to every node (even itself)
                sum += danglingNodeRelevance  / Ln;
                graph->setRelevance(i, normalizedRandom / (double)Ln + damping * sum);

                if (personalization_d > 0) graph->setRelevance(i, graph->relevance(i) + personalization_d * graph->personalization(i));
            }
        }
    }
    //for (int i = 0; i < 5; i++) std::cout << i << " : " << relevance[i] << "\n";
}

// OLD version
void RelevanceCalc::personalizedRank(RelevanceGraph* graph, 
                                     double personalization_d, double random_d, double relevance[],
                                     double normalizeTo, double personalization[], int *retDanglingNodeCount,
                                     double *retDanglingNodeInbound) const 
{
    int Ln = graph->nodeCount();
    double normalizedRandom = random_d * normalizeTo;
    qDebug() << "Relevance calc started for RelevanceGraph size:" << Ln;
    
    double damping = (double)1.0 - personalization_d - random_d;
    
    // dangling nodes are thought to have a link to every node (even itself) in the system
    // we don't want to create these links (would be too slow), therefore they are handled separately
    // first search for the  dangling nodes
    std::vector<int> isDanglingNode;
    int danglingCount = 0;
    double danglingNodeInbound = 0.0;
    int maxIndegree = 0;
    
    // this process now relies on bidirectional links, which means that dangling nodes have
    // always zero in and out links
    for (int i = 0 ; i < Ln; i++) {
        int inDegree = graph->countEdges(i);
        if (inDegree == 0) {
            isDanglingNode.push_back(1);
            danglingCount ++;
            if (personalization_d > 0.0) danglingNodeInbound += personalization[i];
        }
        isDanglingNode.push_back(0);
        if (inDegree > maxIndegree) maxIndegree= inDegree;
    }
    qDebug() << "maxIndegree:" << maxIndegree << ". dangling nodecount:" << danglingCount;
    
    double danglingNodeRelevance = ( normalizedRandom * (double)danglingCount 
                                     / (double)Ln + (personalization_d) * danglingNodeInbound )
        / ( 1 - damping * (double)danglingCount / (double)Ln );
    
    if (retDanglingNodeCount) *retDanglingNodeCount = danglingCount;
    if (retDanglingNodeInbound) *retDanglingNodeInbound = danglingNodeInbound;
    
    // initialize to 1/n
    for (int i = 0; i < Ln; i++) relevance[i] = (double)1.0 / (double)Ln;
    for (int iteration = 0; iteration < m_iterations; iteration++) {
        for (int i = 0 ; i < Ln; i++) {
            if (isDanglingNode[i] == 1) {
                // this is only run at first iteration
                if ( iteration == 0) {
                    // since we expect bidirectional links always, any dangling node is completely unconnected
                    // and we can compute a global dangling node relevance value
                    // FIXME: add the math statement that we are calculating here
                    double individualsum = danglingNodeRelevance  / (double)Ln;
                    if (personalization_d > 0.0) {
                        double pers = personalization[i];
                        relevance[i] =  normalizedRandom  / (double)Ln + (personalization_d) * pers + damping * individualsum;
                    } else
                        relevance[i] =   normalizedRandom  / (double)Ln +damping * individualsum;
                }
            }  else {
                // FIXME: add the math statement that we are calculating here
                double sum = 0;
                QSetIterator<int> edges = graph->edgesAt(i);
                while (edges.hasNext()) {
                    int fromIndex =  edges.next();
                    double fromRel = relevance[fromIndex];
                    int fromOutboundcount = graph->countEdges(fromIndex);
                    sum += fromRel / (double)fromOutboundcount;
                }

                // we simulate dangling nodes having a link to every node (even itself)
                sum += danglingNodeRelevance  / Ln;
                relevance[i] = normalizedRandom  / (double)Ln + damping * sum;

                if (personalization_d > 0) relevance[i] += personalization_d * personalization[i];
            }
        }
    }
    //for (int i = 0; i < 5; i++) std::cout << i << " : " << relevance[i] << "\n";
}

// FIXME: add comments and the mathc statements
void RelevanceCalc::simpleIncremental(RelevanceGraph* graph,
                                      QList<int> items,
                                      int graphNodeCount, int danglingCount,
                                      double personalization_d, double random_d, double normalizeTo,
                                      double danglingNodeInbound) const
{
    double damping = (double)1.0 - personalization_d - random_d;
    int Ln = graphNodeCount;
    
    double normalizedRandom = random_d * normalizeTo;
    if  (danglingNodeInbound < 0) danglingNodeInbound = normalizeTo * ( (double)danglingCount / (double)Ln );
    
    double danglingNodeRelevance =( normalizedRandom *  (double)danglingCount / (double)Ln + (personalization_d) * danglingNodeInbound)
        / (1 - damping * (double)danglingCount / (double)Ln);
    
    for(int itemId = 0; itemId < items.size(); itemId++) {
        double sum = 0;
        double retRelevance = 0;
        
        QSetIterator<int> edges = graph->edgesAt(items.at(itemId));
        while(edges.hasNext()) {
            int current = edges.next();
            sum += graph->relevance(current) / graph->countEdges (current);
        }
        sum += danglingNodeRelevance / (double)Ln;
        
        retRelevance = normalizedRandom / (double)Ln + damping * sum;
        if (personalization_d > 0) 
            retRelevance += personalization_d * graph->personalization(items.at(itemId));

        graph->setRelevance(items.at(itemId), retRelevance);
    }
}

// OLD version
double RelevanceCalc::simpleIncremental(int graphNodeCount, int directLinkCount, int danglingCount,
                                        double personalization_d, double random_d, double relevance[], double normalizeTo,
                                        double personalization, int linkCount[], double danglingNodeInbound) const 
{
    double sum = 0;
    double retRelevance = 0;
    double damping = (double)1.0 - personalization_d - random_d;
    int Ln = graphNodeCount;

    double normalizedRandom = random_d * normalizeTo;
    if  (danglingNodeInbound < 0) danglingNodeInbound = normalizeTo * ( (double)danglingCount / (double)Ln );

    double danglingNodeRelevance =( normalizedRandom *  (double)danglingCount / (double)Ln + (personalization_d) * danglingNodeInbound)
             / (1 - damping * (double)danglingCount / (double)Ln);

    for (int i = 0; i < directLinkCount; i++) {
        sum += relevance[i] / (double)linkCount[i];
    }
    sum += danglingNodeRelevance  / (double)Ln;

    retRelevance = normalizedRandom  / (double)Ln + damping * sum;
    if (personalization_d > 0) retRelevance += personalization_d * personalization;
    return retRelevance;
}

void RelevanceCalc::personalizedBlockRank(RelevanceGraph* graph, 
                                          double personalization_d, double random_d, double relevance[], double normalizeTo, double personalization[],
                                          int outlinksArr[], int nodecountArr[], QSet<int> ignore, bool initializeRelevance) const 
{
    // FIXME: now if random_d is >0 this will fail since it should weight the randomness by the size of aggr node
    double normalizedRandom = random_d * normalizeTo;
    int Ln = graph->nodeCount();
    double damping = (double)1.0 - personalization_d - random_d;

    int totalNodeCount = 0;
    for (int i = 0; i < Ln; i++) totalNodeCount += nodecountArr[i];

    // initialize to 1/n
    if (initializeRelevance) {
        for (int i = 0; i < Ln; i++) relevance[i] = nodecountArr[i] / totalNodeCount;
    }
    // TODO: dangling nodes like above
    for (int iteration = 0; iteration < m_iterations; iteration++) {
        // TODO: add dangling node support similar to above normal calc
        for (int i = 0 ; i < Ln; i++) {
            if (!ignore.contains(i)) {
                double sum = 0;

                QSetIterator<int> edges = graph->edgesAt(i);
                while (edges.hasNext()) {
                    int fromIndex =  edges.next();
                    double fromRel = relevance[fromIndex];
                    double fromOutboundcount;
                    
                    fromOutboundcount = (double)nodecountArr[i] / (double)outlinksArr[fromIndex];
                    sum += fromRel * fromOutboundcount;
                }
                // TODO: dangling nodes like above
                relevance[i] = normalizedRandom * (double)nodecountArr[i] / (double)totalNodeCount + damping * sum;
                if (personalization_d > 0) relevance[i] += personalization_d * personalization[i];
            }
        //for (int i = 0; i < Ln; i++) std::cout << i << " : " << relevance[i] << "\n";
        }
    }
}
