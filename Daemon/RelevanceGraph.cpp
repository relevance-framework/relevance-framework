/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "RelevanceGraph.h"

#include <QDebug>

RelevanceGraph::RelevanceGraph(int size, bool directed) 
    : m_links(new QList< QSet<int>* >()),
      m_linkCountNum(0),
      m_directed(directed)
{
    for (int i = 0; i < size; i++) {
        QSet<int>* linksArr = new QSet<int>();
        m_links->append(linksArr);
        if (m_directed)
            m_outgoingEdgeCount.append(0);

        m_personalization.append(0.0);
        m_relevance.append(0.0);
        m_modificationTime.append(0);
    }
}

RelevanceGraph::~RelevanceGraph() 
{
    for (int i = 0; i < m_links->size(); i++) {
        QSet<int>* linksArr = m_links->at(i);
        delete linksArr;
    }
    delete m_links;
    m_links = 0;
}
    
void RelevanceGraph::incrementOutgoingEdgeCount(int from) 
{
    m_linkCountNum++;
    if (m_directed)
        m_outgoingEdgeCount.replace(from, m_outgoingEdgeCount.at(from) + 1);
}
    
int RelevanceGraph::nodeCount() 
{
    return m_links->size();
}

void RelevanceGraph::addToVector(QSet<int>* vector, int value) 
{
    if (vector->contains(value)) 
        return;

    // grow by one if necessary
    if (vector->capacity() <= vector->size()) {
        vector->reserve(vector->size() + 1);
    }
    vector->insert(value);
}

void RelevanceGraph::growIfNeeded(int expectedSize) 
{
    if (m_links->size() < expectedSize) {
        for (int i = m_links->size(); i < expectedSize; i++) {
            QSet<int>* linksArr = new QSet<int>();
            m_links->append(linksArr);
            if (m_directed)
                m_outgoingEdgeCount.append(0);

            m_personalization.append(0.0);
            m_relevance.append(0.0);
            m_modificationTime.append(0);
        }
    }
}

int RelevanceGraph::addNode() 
{
    growIfNeeded(m_links->size()+1);
    return nodeCount()-1;
}

void RelevanceGraph::addEdge(int from, int to) 
{
    int expectedSize = std::max(from, to) + 1;

    // increase the size if necessary
    growIfNeeded(expectedSize);

    if (!hasEdge(from,to)) {
        QSet<int>* linksTo = m_links->at(to);
        addToVector(linksTo, from);
        incrementOutgoingEdgeCount(from);
    }
    if (!hasEdge(to, from)) {
        QSet<int>* linksFrom = m_links->at(from);
        addToVector(linksFrom, to);
        incrementOutgoingEdgeCount(to);
    }
}

void RelevanceGraph::addDirectedEdge(int from, int to) 
{
    int expectedSize = std::max(from, to) + 1;

    // increase the size if necessary
    growIfNeeded(expectedSize);

    if (!hasEdge(from,to)) {
        QSet<int>* linksTo = m_links->at(to);
        addToVector(linksTo, from);
        incrementOutgoingEdgeCount(from);
    }
}

bool RelevanceGraph::hasEdge(int from, int to) 
{
    if (std::max(to,from) > nodeCount()-1) 
        return 0;

	QSet<int>* vector = m_links->at(to);
	return vector->contains(from);
}

// FIXME: when using directed graph, we need to keep the count of outgoing edges in a separate array!
int RelevanceGraph::countEdges(int from) 
{
    if (from > nodeCount()-1) 
        return 0;

    if (m_directed) {
        return m_outgoingEdgeCount.at(from);
    } else {
        QSet<int>* vector = m_links->at(from);
        return vector->size();
    }
}

QSetIterator<int> RelevanceGraph::edgesAt(int node) 
{
    QSet<int>* list = m_links->at(node);
    QSetIterator<int> i(*list);
    return i;
}

int RelevanceGraph::linkCount() 
{ 
    return m_linkCountNum; 
}

double RelevanceGraph::personalization(int node) 
{
    return m_personalization.value(node);
}

void RelevanceGraph::setPersonalization(int node, double value)
{
    m_personalization.replace(node, value);
}

double RelevanceGraph::relevance(int node)
{
    return m_relevance.value(node);
}

void RelevanceGraph::setRelevance(int node, double value)
{
    m_relevance.replace(node, value);
}

uint RelevanceGraph::modificationTime(int node)
{
    return m_modificationTime.value(node);
}

void RelevanceGraph::setModificationTime(int node, uint value)
{
    m_modificationTime.replace(node, value);
}
