/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ABSTRACTHARVESTER_H
#define ABSTRACTHARVESTER_H

#include <QObject>
#include <QMap>
#include <QDateTime>

class AbstractHarvester : public QObject
{
    Q_OBJECT
    public:
        virtual ~AbstractHarvester() {}

        // API for initialization

        virtual bool initialize() = 0;

        virtual QStringList getRecentChanges( const QString &lastUpdate ) = 0;

        virtual QVector<QStringList> getSupportedClasses() = 0;

        // API for item details

        virtual QStringList getItemClass(const QString &uri) = 0;

        virtual QMap<QString,QVariant> getItemProperties(const QString &uri, const QString &classUri) = 0;

        virtual QStringList getRelatedItems(const QString &uri) = 0;

        // API for item actions

        virtual QVector<QStringList> convertActions(const QStringList &) = 0;

        virtual QVector<QStringList> convertActionsForMime(const QString &) = 0;

        virtual bool executeItems(const QStringList &items, const QString &action = QString ("")) = 0;

    signals:
        void itemsUpdated(const QSet<QString> &added, const QSet<QString> &removed, const QSet<QString> &changed, const QDateTime &lastUpdate);
};

#endif // ABSTRACTHARVESTER_H
