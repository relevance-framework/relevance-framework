/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "ContentStorage.h"

#include <QDebug>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QVariant>
#include <QDateTime>

#include <cmath>

#include "ExpDecayCounter.h"
#include "RelevanceGraph.h"

ContentStorage::ContentStorage()
    : m_graph(new RelevanceGraph()),
      m_typeNodeCount(0), m_contentDb(QSqlDatabase::database())
{
    // do nothing
}

ContentStorage::~ContentStorage()
{
    delete m_graph;
    m_graph = 0;
}

void ContentStorage::loadAllContent()
{
    qDebug() << "Loading all content";
    
    loadTypeNodes();
    
    loadSubjectNodes();
    
    loadRelations();
}

void ContentStorage::saveAllContent()
{
    qDebug() << "Saving all content";

    QSqlQuery query;
    query.prepare("UPDATE ClassHierarchy SET relevance=? WHERE id=?");

    int nodeCount = 0;

    m_contentDb.transaction();
    
    for(int i=0; i<m_typeNodeCount; i++) {
        query.bindValue(0, m_graph->relevance(i));
        query.bindValue(1, m_idMapping.key(i));

        if (!query.exec()) {
            qCritical() << "Unable to save relevance: " << query.lastError().text();
            m_contentDb.rollback();
            return;
        }
        nodeCount++;
    }

    qDebug() << "Updated type nodes" << nodeCount;

    query.prepare("UPDATE Subjects SET relevance=? WHERE id=?");

    nodeCount = 0;
    for(int i=m_typeNodeCount; i<m_graph->nodeCount(); i++) {
        query.bindValue(0, m_graph->relevance(i));
        query.bindValue(1, m_idMapping.key(i));
        
        if (!query.exec()) {
            qCritical() << "Unable to save relevance: " << query.lastError().text();
            m_contentDb.rollback ();
            return;
        }
        nodeCount++;
    }

    if (!m_contentDb.commit()) {
        qWarning () << "[ContentStorage] : could not update database : " << m_contentDb.lastError().text() ;
    } 

    qDebug() << "Updated subject nodes" << nodeCount;
}

QList<int> ContentStorage::loadSelectedContent(const QStringList &selectedUris)
{
    qDebug() << "Loading selected content";
    
    loadTypeNodes();
    
    QList<int> ids = loadSubjectNodes(selectedUris);
    
    loadRelations(ids);
    
    return ids;
}

void ContentStorage::saveSelectedContent(const QList<int>& ids)
{
    qDebug() << "Saving selected content";

    QSqlQuery query;
    query.prepare("UPDATE Subjects SET relevance=? WHERE id=?");
    
    int nodeCount = 0;
    for(int i=0; i<ids.size(); i++) {
        double relevance = m_graph->relevance(ids.at(i));
        int id = m_idMapping.key(ids.at(i));

        query.bindValue(0, relevance);
        query.bindValue(1, id);

        qDebug () << relevance << ":" << id; 
        if (!query.exec()) {
            qCritical() << "Unable to save relevance to subjects: " << query.lastError().text();
            return;
        }
        nodeCount++;
    }
    
    qDebug() << "Updated nodes" << nodeCount;

}

RelevanceGraph * ContentStorage::graph()
{
    return m_graph;
}

void ContentStorage::loadTypeNodes()
{
    QSqlQuery query;
    if (!query.exec("SELECT id FROM ClassHierarchy")) {
        qCritical() << "Unable to read type nodes: " << query.lastError().text();
        return;
    }
    
    int nodeCount = 0;
    while (query.next()) {
        bool ok;
        
        int uriId = query.value(0).toInt(&ok);
        if (!ok) {
            continue;
        }
        
        int nodeId = m_graph->addNode();
        
        m_idMapping.insert(uriId, nodeId);
        m_typeNodeCount++;
        
        nodeCount++;
    }
    
    qDebug() << "Type nodes loaded: " << nodeCount;
}

QList<int> ContentStorage::loadSubjectNodes(const QStringList &uris)
{
    QString queryString("SELECT id, class, timestamp, c_ExpDecay_2weeks FROM Subjects");
    if (!uris.empty()) {
        queryString += " WHERE uri='" + uris.join("' or uri='") + "'";
    }
    qDebug() << queryString;
    
    QSqlQuery query;
    if (!query.exec(queryString)) {
        qCritical() << "Unable to read subject nodes: " << query.lastError().text();
        return QList<int>();
    }
    
    ExpDecayCounter counter("ExpDecay_2weeks", TWO_WEEKS_IN_SECONDS);
    uint now = QDateTime::currentDateTime().toTime_t();

    QList<int> retValues;

    int nodeCount = 0;
    while (query.next()) {
        bool ok;

        int uriId = query.value(0).toInt(&ok);
        if (!ok) {
            continue;
        }

        int nodeId = m_graph->addNode();

        m_idMapping.insert(uriId, nodeId);

        int classId = query.value(1).toInt(&ok);
        if (ok) {
            m_graph->addEdge(nodeId, m_idMapping.value(classId));
        }

        int timestamp = query.value(2).toInt(&ok);
        if (ok) {
            m_graph->setModificationTime(nodeId, timestamp);

            double personalization = query.value(3).toDouble(&ok);
            if (ok) {
                m_graph->setPersonalization(nodeId, counter.convert(personalization, timestamp, now));
            }
        }
        
        nodeCount++;

        retValues.append(nodeId);
    }

    qDebug() << "Subject nodes loaded: " << nodeCount;

    return retValues;
}

void ContentStorage::loadRelations(const QList<int>& ids)
{
    QString queryString("SELECT fromNode, toNode FROM Relations");
    if (!ids.empty()) {
        queryString += " WHERE fromNode=";
        for(int i=0; i<ids.size(); i++) {
            queryString += QString::number(ids.at(i));
            if (i<(ids.size()-1))
                queryString += " or fromNode=";
        } 
        queryString += "";
    }
    qDebug() << queryString;

    QSqlQuery query;
    if (!query.exec(queryString)) {
        qCritical() << "Unable to read relations: " << query.lastError().text();
        return;
    }
    
    int nodeCount = 0;
    while (query.next()) {
        bool ok;

        int uriId = query.value(0).toInt(&ok);
        if (!ok) {
            continue;
        }

        int otherId = query.value(1).toInt(&ok);
        if (ok) {
            m_graph->addEdge(m_idMapping.value(uriId), m_idMapping.value(otherId));
        }

        nodeCount++;
    }

    qDebug() << "Relations loaded: " << nodeCount;
}

CalculationState ContentStorage::getState()
{
    QSqlQuery query;
    CalculationState state;
    if (!query.exec("SELECT last_dangling_node_count, last_node_count, last_full_timestamp, last_activity, last_ages_sum, last_dangling_node_activity FROM State WHERE id=1")) {
        qCritical() << "Unable to load settings: " << query.lastError().text();
        return state;
    }
    
    if (!query.next()) {
        qCritical() << "Unable to load settings: db not initialized";
        return state;
    }
    bool ok;
    int intVal;
    uint uintVal;
    double doubleVal;

    intVal = query.value(0).toInt(&ok);
    if (ok) {
        state.setLastDanglingNodeCount (intVal);
    } else {
        qWarning() << "Unable to read lastDanglingNodeCount";
    }

    intVal = query.value(1).toInt(&ok);
    if (ok) {
        state.setLastNodeCount (intVal);
    } else {
        qWarning() << "Unable to read lastNodeCount";
    }

    uintVal = query.value(2).toUInt(&ok);
    if (ok) {
        state.setLastFullTimestamp (uintVal);
    } else {
        qWarning() << "Unable to read lastFullTimestamp";
    }

    doubleVal = query.value(3).toDouble(&ok);
    if (ok) {
        state.setLastActivity (doubleVal);
    } else {
        qWarning() << "Unable to read lastActivity";
    }

    doubleVal = query.value(4).toDouble(&ok);
    if (ok) {
        state.setLastAgesSum (doubleVal);
    } else {
        qWarning() << "Unable to read lastAgesSum";
    }

    doubleVal = query.value(5).toDouble(&ok);
    if (ok) {
        state.setLastDanglingNodeActivity (doubleVal);
    } else {
        qWarning() << "Unable to read lastDanglingNodeActivity";
    }

    return state ;
}

bool ContentStorage::setState(const CalculationState &state)
{
    QString queryString = "UPDATE State SET last_dangling_node_count=%1, last_node_count=%2, last_full_timestamp=%3, last_activity=%4, last_ages_sum=%5, last_dangling_node_activity=%6 WHERE id=1";
    QSqlQuery query;
    
    if (!query.exec(queryString.arg(state.lastDanglingNodeCount()).arg(state.lastNodeCount()).arg(state.lastFullTimestamp())
                    .arg(state.lastActivity()).arg(state.lastAgesSum()).arg(state.lastDanglingNodeActivity()))) {
        qCritical() << "Unable to save settings:"<< query.lastError().text();

        return false;
    }

    return true;
}
