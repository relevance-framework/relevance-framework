/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "ServerSocket.h"

#include <QLocalServer>
#include <QLocalSocket>
#include <QStringList>
#include <QVariant>
#include <QCoreApplication>

#include "RelevanceDaemon.h"
#include "RelevanceDaemonMetaTypes.h"
#include <signal.h>


ServerSocket::ServerSocket(const QString &address, QObject *p) 
    : QObject(p), m_server(new QLocalServer(this)), m_blockSize(0)
{
    if(!m_server->listen(address)) {
        qWarning () << "ServerSocket : Cannot Listen ... :" << m_server->errorString();
        m_server->close(); return ;
    }

    connect (m_server, SIGNAL(newConnection()), this, SLOT(onClientConnected()));
    
    struct sigaction term;

    term.sa_handler = ServerSocket::termSignalHandler;
    sigemptyset(&term.sa_mask);
    term.sa_flags |= SA_RESTART;

    if (sigaction(SIGHUP, &term, 0) > 0)
        qCritical () << "Could not listen for SIGHUP signal";
    
    if (sigaction(SIGTERM, &term, 0) > 0)
        qCritical () << "Could not listen for SIGTERM signal" ;

    if (sigaction(SIGINT, &term, 0) >0)
        qCritical () << "Could not listen for SIGINT signal" ;

    qRegisterMetaTypeStreamOperators<QVector<QVariantMap> >("QVector<QVariantMap>");
}

ServerSocket::~ServerSocket()
{
    delete m_server;
}

void ServerSocket::onClientConnected()
{
    QLocalSocket *connection = m_server->nextPendingConnection();

    connect (connection, SIGNAL(readyRead()), this, SLOT(handleRequest()));
    connect (connection, SIGNAL(disconnected()), connection, SLOT(deleteLater()));
    connect (connection, SIGNAL(error(QLocalSocket::LocalSocketError)), this, SLOT(onSocketError(QLocalSocket::LocalSocketError)));
}

void ServerSocket::handleRequest()
{
    QLocalSocket *socket = qobject_cast<QLocalSocket*>(sender());
    RelevanceDaemon *daemon = qobject_cast<RelevanceDaemon *>(parent());

    if (!socket || !daemon) {
        qCritical ("ERROR :: assertion(!socket || !daemon)");
        return ;
    }
    
    QDataStream in(socket);
    in.setVersion(QDataStream::Qt_4_0);

    if (!m_blockSize) {
        if (socket->bytesAvailable() < sizeof(m_blockSize)) {
            qCritical() << "Currepted request" ;
            return ;
        }

        // Retrive size of the request
        in >> m_blockSize ;
    }

    // Wait till complete request is ready
    if ((quint32)socket->bytesAvailable() < m_blockSize)
        return ;

    // reinitialize blockSize for next request
    m_blockSize = 0;

    // 
    // Everything ready serve request
    // 
    QByteArray data;
    QDataStream out(&data, QIODevice::WriteOnly);
    out.setVersion (QDataStream::Qt_4_0);
    QString request ;
    quint16 id ;
    QVariant results;
    quint32 resultsBlockSize = (quint32)0;

    // retrive request id
    in >> request ;

    //get uniqid
    in >> id;

    // reserver for block size of results
    out << resultsBlockSize;

    // write back the request id for client's further reference
    out << id ;

    if (request == "doSearch") {
        QString text ;
        QStringList content_types;
        QStringList content_sources;
        bool grouping_flag;

        in >> text ;
        in >> content_types ;
        in >> content_sources ;
        in >> grouping_flag ;

        results = QVariant::fromValue (daemon->doSearch(text, content_types, content_sources, grouping_flag));

   }
    else if (request == "doSearchGroup") {
        QString groupUri;
        QString text;
        QStringList content_types;
        QStringList content_sources;
        bool grouping_flag;

        in >> groupUri ;
        in >> text ;
        in >> content_types ;
        in >> content_sources ;
        in >> grouping_flag ;


        results = QVariant::fromValue (daemon->doSearch(groupUri, text, content_types, content_sources, grouping_flag));

    }
    else if (request == "doSearchItems") {
        QStringList uris;
        QStringList content_types;
        QStringList content_sources;

        in >> uris ;
        in >> content_types ;
        in >> content_sources ;

        results = QVariant::fromValue (daemon->doSearchItems (uris, content_types, content_sources));

    }
    else if (request == "actionsForItem") {
        QVariant var;
        QString uri ;

        in >> var ; 
        uri = var.toString() ;

        QVector<QStringList> results = daemon->actionsForItem (uri);
        out << results;
    }
    else if (request == "actionsForItems") {
        QVariant var;
        QStringList uris ;
        QVector<QStringList> results;

        in >> var ;
        uris = var.toStringList() ;
        results = daemon->actionsForItems (uris);
        out << results;
    }
    else if (request == "executeItem") {
        QVariant var;
        QString uri, action ;

        in >> var ; uri = var.toString();
        in >> var ; action = var.toString();

        out << daemon->executeItem (uri, action);
    }
    else if (request == "executeItems") {
        QVariant var;
        QStringList uris;
        QString action ;

        in >> var ; uris = var.toStringList();
        in >> var ; action = var.toString();
        
        out << daemon->executeItems (uris, action);
    }
    else {
        qCritical () << "[ServerSocket] Unknown Request :" << request ;
    }
   
    if (!results.isNull())  {
        // write reply to the socket
        out << results;
    }
    // Update actual size of the reply (i.e, data.size()-sizeof(uint32))
    if (!out.device()->seek(0)) {
        qCritical() << "ERROR : seek to 0 failed ";
        //FIXME : clean up socket/data strean ??
        return ;
    }
    resultsBlockSize = (quint32)data.size() - sizeof(resultsBlockSize);
    out << resultsBlockSize;

    //write to socket 
    //FIXME : Assuming write is always writes whole data
    //Need to support batch writing
    if (socket->write (data) == -1) {
        qCritical () << "Error writing client socket : " << socket->errorString();
    }
    socket->flush();
}

void ServerSocket::onSocketError(QLocalSocket::LocalSocketError err)
{
    qCritical () << "ServerSocket::onSocketError :" << err ;

    switch(err)
    {
    case QLocalSocket::ServerNotFoundError:
        qCritical () << "QLocalSocket::ServerNotFoundError" ;
        break;
    case QLocalSocket::ConnectionRefusedError:
        qCritical () << "QLocalSocket::ConnectionRefusedError" ;
        break;
    case QLocalSocket::PeerClosedError:
        qCritical () << "QLocalSocket::PeerClosedError" ;
        break;
    default:
        break;
    }
}

void ServerSocket::termSignalHandler(int)
{
    qApp->quit();
}
