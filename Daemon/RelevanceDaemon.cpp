/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <QProcess>
#include <QTimer>
#include <QFile>
#include <QTextStream>
#include <QPluginLoader>
#include <QDir>

#include "RelevanceDaemon.h"
#include "RelevanceEngine.h"
#include "AbstractHarvester.h"
#include "AbstractHarvesterFactory.h"
#include "RelevanceStorageService.h"

#ifdef QT_DBUS_LIB
#include "RelevanceDaemonMetaTypes.h"
#include <QDBusConnection>
#include <QDBusConnectionInterface>
#include <QDBusMessage>
#include <QDBusMetaType>
#endif

RelevanceDaemon::RelevanceDaemon(QString dbFile,
                                 int /*cacheTime*/,
                                 int timeoutFull, int timeoutIncremental,
                                 int timeFirstFullCalc, int timeBetweenCalcs,
                                 int timeIdleSample, int timeWaitForIdle, int timeFirstWaitForIdle, int idleLimit,
                                 int registrationRetryDelay, int registrationRetries,
                                 QObject* parent):
                                    QObject(parent), m_relevanceStorage(NULL), m_dbFile(dbFile),
                                    m_timeoutFull(timeoutFull), m_timeoutIncremental(timeoutIncremental),
                                    m_timeFirstFullCalc(timeFirstFullCalc), m_timeBetweenCalcs(timeBetweenCalcs),
                                    m_timeIdleSample(timeIdleSample), m_timeWaitForIdle(timeWaitForIdle), m_timeFirstWaitForIdle(timeFirstWaitForIdle), m_idleLimit(idleLimit),
                                    m_registrationRetryDelay(registrationRetryDelay), m_registrationRetries(registrationRetries),
                                    m_haveFirstCalc(false), m_firstTimeForFullCalc(true), m_initialized(false)
{
#ifdef QT_DBUS_LIB
    qRegisterMetaType<QVector<QVariantMap> >();
    qDBusRegisterMetaType<QVector<QVariantMap> >();
#endif
}



/********************************* Public Slots **********************************************/

void RelevanceDaemon::addActivity(const QString &subjectUri) {
    qDebug() << "addActivity(" << subjectUri << ")";
    if (ensureStorageIsInitialized()) {
        m_relevanceStorage->addActivity(subjectUri);
        m_activityQueue << subjectUri;
        resetTimer();
    }
}


bool RelevanceDaemon::addActivitySynchronously(const QString &subjectUri) {
    qDebug() << "addActivitySynchronously(" << subjectUri << ")";
    if (ensureStorageIsInitialized()) {
        bool result = m_relevanceStorage->addActivitySynchronously(subjectUri);
        m_activityQueue << subjectUri;
        resetTimer();
        return result;
    } else {
        return false;
    }
}

QVector<QVariantMap> RelevanceDaemon::doSearch (const QString &text, const QStringList &content_types, const QStringList &content_sources, bool grouping_required)
{
    if (ensureStorageIsInitialized()) {
        return m_relevanceStorage->doSearch (text, content_types, content_sources, grouping_required) ;
    }

    return QVector<QVariantMap>();
}

QVector<QVariantMap> RelevanceDaemon::doSearch (const QString &group, const QString &text, const QStringList &content_types, 
                                                      const QStringList &content_sources, bool grouping_required)
{
    if (ensureStorageIsInitialized()) {
        return m_relevanceStorage->doSearch (group, text, content_types, content_sources, grouping_required) ;
    }

    return QVector<QVariantMap>();
}

QVector<QVariantMap> RelevanceDaemon::doSearchItems (const QStringList &uris, const QStringList &content_types, const QStringList &content_sources)
{
    if (ensureStorageIsInitialized()) {
        return m_relevanceStorage->doSearch (uris, content_types, content_sources) ;
    }

    return QVector<QVariantMap>();
}

QVector<QStringList> RelevanceDaemon::actionsForItem(const QString& identifier) {
    QVector<QStringList> result;
    QStringList identifierList(identifier);
    foreach(AbstractHarvester* harvester, m_harvesters) {
        QVector<QStringList> actions = harvester->convertActions (identifierList);
        for(int i=0; i<actions.size(); ++i) {
            result.append( actions.at(i) );
        }
    }
    return result;
}

QVector<QStringList> RelevanceDaemon::actionsForItems(const QStringList& identifiers) {
    QVector<QStringList> result;
    foreach(AbstractHarvester* harvester, m_harvesters) {
        QVector<QStringList> actions = harvester->convertActions (identifiers);
        for(int i=0; i<actions.size(); ++i) {
            result.append( actions.at(i) );
        }
    }
    return result;
}

QVector<QStringList> RelevanceDaemon::actionsForMime(const QString& mimetype) {
    QVector<QStringList> result;
    foreach(AbstractHarvester* harvester, m_harvesters) {
        QVector<QStringList> actions = harvester->convertActionsForMime (mimetype);
        for(int i=0; i<actions.size(); ++i) {
            result.append( actions.at(i) );
        }
    }
    return result;
}

bool RelevanceDaemon::executeItem(const QString &itemIdentifier, const QString& actionIdentifier) {
    QStringList itemIdentifierList(itemIdentifier);
    foreach(AbstractHarvester* harvester, m_harvesters) {
        if (harvester->executeItems( itemIdentifierList, actionIdentifier ) == true)
            return true;
    }
    return false;
}

bool RelevanceDaemon::executeItems(const QStringList& itemIdentifiers, const QString &actionIdentifier) {
    foreach(AbstractHarvester* harvester, m_harvesters) {
        if (harvester->executeItems( itemIdentifiers, actionIdentifier ) == true) 
            return true;
    }

    return false;
}

int RelevanceDaemon::dump() const { 
    return m_relevanceStorage->dump(); 
}

QVariantList RelevanceDaemon::activityCount(const QString &subjectUri) { 
    if (ensureStorageIsInitialized()) 
        return m_relevanceStorage->activityCount(subjectUri); 

    return QVariantList() << QString("error:daemon_not_initialized/");
}

QVariantList RelevanceDaemon::activityCounts() { 
    if (ensureStorageIsInitialized()) 
        return m_relevanceStorage->activityCounts();

    return QVariantList() << QString("error:daemon_not_initialized/");
}

static const char require[] = "relevance::RelevanceAllContentTypes";

void RelevanceDaemon::forceFullCalculation() {
    qDebug() << "forceFullCalculation()";

    if (!m_haveFirstCalc) {
        calculateRelevance();
    }
}

/********************************* Private Slots **********************************************/

void RelevanceDaemon::processQueues() 
{
    qDebug() << "processQueues:";
    qDebug() << "  - activities:" << m_activityQueue;
/*
    // TODO: empty transaction, should this be moved to wrap addActivities call?
    m_relevanceStorage->beginTransaction();
    if (!m_relevanceStorage->commitTransaction()) {
        m_relevanceStorage->abortTransaction();
        resetTimer(); // If at first you don't succeed...
        return;
    }
*/
    m_relevanceStorage->markUpdate(m_lastUpdate);

    // TODO: move these to storageservice, or perhaps this is not needed in the first place.
    //cleanCache();
    //m_activityQueue = pruneCached(m_activityQueue);


    QStringList uriList;
/*
    // TODO: This is left empty now, should it be moved to dataUpdated or do actually something that looks
    // relevant?
#ifdef qtestlib
    foreach(QString uri, uriList) {
        emit addingActivity(uri);
    }
#else
    m_relevanceStorage->addActivities(uriList);
#endif
*/

    foreach(QString uri, m_activityQueue) {
        uriList << uri;
    }

    triggerRelevanceCalculation(uriList);

    m_activityQueue.clear();
}

void RelevanceDaemon::timeForFullCalc() {
    qDebug() << "timeForFullCalc";

    if (m_haveFirstCalc) {
        if (m_firstTimeForFullCalc) {
            qDebug() << "We already did this earlier on request, no need to redo - next calc in " << m_timeBetweenCalcs/1000 << " seconds";
            QTimer::singleShot(m_timeBetweenCalcs, this, SLOT(timeForFullCalc()));
            m_firstTimeForFullCalc = false;
        } 
    } else {
        m_firstTimeForFullCalc = false;
        if (getUptime(m_lastUpTime, m_lastIdleTime)) {
            qDebug() << "Uptime read:" << m_lastUpTime << "," << m_lastIdleTime;
            qDebug() << "Waiting" << m_timeIdleSample/1000 << "seconds before reading second sample...";
            QTimer::singleShot(m_timeIdleSample, this, SLOT(timeForIdleCheck()));
         } else {
             qWarning() << "Unable to read uptime, will retry in 10s";
             QTimer::singleShot(10000, this, SLOT(timeForFullCalc()));
         }
    }
}

void RelevanceDaemon::timeForIdleCheck() {
    qDebug() << "timeForIdleCheck";
    long upTime, idleTime;
    if (getUptime(upTime,idleTime)) {
        qDebug() << "Uptime read:" << upTime << "," << idleTime;
        long upTimeDiff = upTime - m_lastUpTime;
        long idleTimeDiff = idleTime - m_lastIdleTime;
        qDebug() << "Difference:" << upTimeDiff << "," << idleTimeDiff << "(" << (idleTimeDiff * 100 / upTimeDiff) << "%)";
        if (idleTimeDiff * 100 / upTimeDiff > m_idleLimit) {
            qDebug() << "Phone is idle, calculating relevance!";
#ifdef qtestlib
            emit calculatingFullRelevance();
#else
            calculateRelevance();
#endif
            qDebug() << "Next relevance calc in " << m_timeBetweenCalcs/1000 << " seconds";
            QTimer::singleShot(m_timeBetweenCalcs, this, SLOT(timeForFullCalc()));
        } else {
            if (m_haveFirstCalc) {
                qDebug() << "Phone is not idle, retry in" << m_timeWaitForIdle/1000 << "seconds (subsequent calcs)";
                QTimer::singleShot(m_timeWaitForIdle, this, SLOT(timeForFullCalc()));
            } else {
                qDebug() << "Phone is not idle, retry in" << m_timeFirstWaitForIdle/1000 << "seconds (first calc)";
                QTimer::singleShot(m_timeFirstWaitForIdle, this, SLOT(timeForFullCalc()));
            }
        }
    } else {
        qWarning() << "Unable to read uptime, waiting " << m_timeWaitForIdle/1000 << "seconds and retrying";
        QTimer::singleShot(m_timeWaitForIdle, this, SLOT(timeForFullCalc()));
    }
}

/****************************** Private methods *****************************************/

// Initialization is done in this method rather than constructor to enable failure reporting.
// However, currently failures are not returned.
int RelevanceDaemon::initialize() 
{
    m_queueTimer = new QTimer(this);
    m_queueTimer->setInterval(10000);
    m_queueTimer->setSingleShot(true);
    connect ( m_queueTimer, SIGNAL(timeout()), this, SLOT(processQueues()));

   // Again, needed to comment out context reqs to get rid of SIGSEGV. Unit test does not test context anyhow at the moment.
#ifdef qtestlib
    //Trigger::ContextRequirement dummy("Battery.OnBattery", Trigger::Comparison::EQ, false);
    //dummy.activate();
    m_haveFirstCalc = true;
#endif
    m_initialized = true;

    ensureStorageIsInitialized();

    QTimer::singleShot(0, this, SLOT(initializeHarvesters()));

    return 0;
}

void RelevanceDaemon::initializeHarvesters()
{
    QTime t;

    qCritical () << " Harvesting started at ..." ;
    t.start();
    QDir harvestersDir("/usr/lib/relevance-harvesters");
    foreach ( QString fileName, harvestersDir.entryList( QDir::Files | QDir::NoSymLinks ) ) {
        qDebug() << "Loading harvester from" << fileName;
        QPluginLoader loader( harvestersDir.absoluteFilePath( fileName ) );
        AbstractHarvesterFactory *harvesterFactory = qobject_cast< AbstractHarvesterFactory* >( loader.instance() );
        if( harvesterFactory ) {
            AbstractHarvester* harvester = harvesterFactory->createHarvester();
            if( harvester && harvester->initialize() ) {
                QObject::connect(
                        harvester, SIGNAL(itemsUpdated(const QSet<QString> &, const QSet<QString> &, const QSet<QString> &, const QDateTime &)),
                        m_relevanceStorage, SLOT(dataUpdated(const QSet<QString> &, const QSet<QString> &, const QSet<QString> &, const QDateTime &)));
                m_relevanceStorage->registerClasses( harvester->getSupportedClasses() );
                m_relevanceStorage->updateRecentChanges( harvester->getRecentChanges( m_relevanceStorage->lastUpdate() ), harvester );
                // That prev line looks horrible. The goal is to replace it just following and have updateda
                // values be passed with itemsUpdated signal.
                // harvester->getRecentChanges( m_relevanceStorage->lastUpdated() );
                m_harvesters << harvester;
            }
            else {
                if( harvester )
                    qDebug() << "Harvester initialization failed";
                else
                    qDebug() << "HarvesterFactory failed in creating a harvester" << loader.errorString();
            }
        } 
        else {
            qDebug() << "Invalid Harvester plugin" << loader.errorString();
        }
    }

    if(m_harvesters.isEmpty()){
        qWarning() << "No harvesters found";
    }
    // The below code (declaring requirements) for some reason causes SIGSEGV if done within a unit test.
    // So we switch it out in Ut_*.pro files by setting DEFINES += nofullcalc
#ifndef nofullcalc
    qDebug() << "First relevance calculation will be performed in " << m_timeFirstFullCalc/1000 << " seconds";
    QTimer::singleShot(m_timeFirstFullCalc, this, SLOT(timeForFullCalc()));
#endif
    qCritical () << " Time took to harvest :" << t.elapsed();
}

bool RelevanceDaemon::ensureStorageIsInitialized() {
    if (m_initialized) {
        if (m_relevanceStorage == NULL) {
            m_relevanceStorage = new RelevanceStorageService(this);
            m_relevanceStorage->initialize(m_dbFile);

            QObject::connect(
                    m_relevanceStorage, SIGNAL(storageDataUpdated(QStringList &)),
                    this, SLOT(triggerRelevanceCalculation(QStringList &)));
        }
        return true;
    } else {
        return false;
    }
}

void RelevanceDaemon::triggerRelevanceCalculation(QStringList &uriList)
{
    qDebug() << "RelevanceDaemon: triggering relevance calculation for:\n" << uriList;
    if (! uriList.empty()) {
        if (this->m_haveFirstCalc) {
            calculateIncrementalRelevance(uriList);
        } else {
            calculateRelevance();
        }
    }
}

void RelevanceDaemon::resetTimer() {
    if (m_queueTimer != NULL) {
        if ( m_queueTimer->isActive() ) {
            m_queueTimer->stop();
        }
        m_queueTimer->start();
    }
}

void RelevanceDaemon::calculateRelevance() {
    if (!m_haveFirstCalc) {
        ensureStorageIsInitialized();
    }
#ifndef qtestlib
    RelevanceEngine engine;
    engine.calculateFully(true);
    m_haveFirstCalc = true;
#endif
}

void RelevanceDaemon::calculateIncrementalRelevance(QStringList uriList) {
#ifdef qtestlib
    emit calculatingRelevance(uriList);
#else
    RelevanceEngine engine;
    engine.calculateIncremental(uriList, true);
#endif
}

bool RelevanceDaemon::getUptime(long &upTime, long& idleTime) {
    QFile file("/proc/stat");
    if (file.open(QFile::ReadOnly)) {
        QString cpu;
        long user, nice, system, idle, iowait, irq, softirq;
        QTextStream in(&file);
        in >> cpu >> user >> nice >> system >> idle >> iowait >> irq >> softirq;
        upTime = user + nice + system + idle + iowait + irq + softirq;
        idleTime = idle;
        bool statusOk = (in.status() == QTextStream::Ok);
        file.close();
        return statusOk;
    } else {
        return false;
    }
}
