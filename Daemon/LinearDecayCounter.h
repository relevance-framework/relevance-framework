/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef LINEARDECAYCOUNTER_H
#define LINEARDECAYCOUNTER_H

#include "ActivityCounter.h"



class LinearDecayCounter: public ActivityCounter
{
public:
    LinearDecayCounter(QString name, int period):m_name(name),m_period(period) {}
    /**
     * \brief Defines the name of the algorithm
     *
     * \returns the name of the algorithm
     */
    virtual QString name() { return m_name; }
protected:

    /**
     * \brief Convert an activity frequency score from one time to another.
     *
     * This implements the algorithm newValue = (1.0 - periods/2) * oldValue, or 0 if periods > 2.
     *
     * \param oldValue the original score
     * \param oldTime the timestamp of the original score
     * \param newTime the timestamp to which the original score score should be translated
     * \returns the score in newTime currency.
     */
    virtual double doConvert(double oldValue, uint oldTime, uint newTime) {
        double periods = ((double) (newTime - oldTime)) / m_period;
        if (periods > 2) {
            return 0.0;
        }
        return (1.0 - periods/2) * oldValue;
    }
private:
    //! name of the algorithm (= mode parameter in Activity Tracker service API)
    QString m_name;
    //! Half-life period of the decay
    int m_period;
};

#endif // LINEARDECAYCOUNTER_H
