/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef RELEVANCEDAEMON_H
#define RELEVANCEDAEMON_H

#include <QObject>
#include <QStringList>
#include <QVariant>
#include <QDateTime>
#include <QHash>
#include <QDebug>
#ifdef QT_DBUS_LIB
#include <QDBusContext>
#endif

class RelevanceStorageService;
class AbstractHarvester;
class QTimer;

class RelevanceDaemon 
  : public QObject
#ifdef QT_DBUS_LIB
  , protected QDBusContext
#endif
{
    Q_OBJECT
#ifndef QT_DBUS_LIB
    Q_CLASSINFO("D-Bus Interface", "com.nokia.RelevanceDaemonIf")
#endif

public:
    RelevanceDaemon(QString dbFile,
                    int cacheTime,
                    int timeoutFull, int timeoutIncremental,
                    int timeFirstFullCalc, int timeBetweenCalcs,
                    int timeIdleSample, int timeWaitForIdle, int timeFirstWaitForIdle, int idleLimit,
                    int registrationRetryDelay, int registrationRetries,
                    QObject* parent = NULL);

    /**
     * \brief Initialize Relevance daemon
     *
     * This is done in a method separate from constructor to enable error reporting
     *
     * \return 0 if OK, negative otherwise
     */
    int initialize();

    // dump db contents to stdout
    int dump() const;

public slots:

    // DBus API

    /**
     * \brief Report a user activity that accessed a content item.
     *
     * This is a convenience method which invokes addActivity(subjectUri, "", "", "", -1, "");
     * The method is asynchronous, so the control will return to client immediately after method invocation.
     * There is no possibility to detect failure.
     *
     * \param subjectUri uri of the item that the user accessed
     */
#ifdef USE_BUS
    Q_NOREPLY 
#endif
    void addActivity(const QString &subjectUri);

    /**
     * \brief Synchronously report a user activity that accessed a content item.
     *
     * This is a convenience method which invokes addActivity(subjectUri, "", "", "", -1, "");
     * The method is synchronous, so the control will return to client only after the data has been written to disk.
     *
     * \param subjectUri uri of the item that the user accessed
     * \return True if the data was successfully committed to database, false if not.
     */

    bool addActivitySynchronously(const QString &subjectUri);

    /**
     * \brief Count how many times an uri has been accessed
     *
     * This is a convenince method which invokes count(uri, mode) with a default mode.
     *
     * \param uri the uri for which access should be counted
     * \return A QVariantList containing the following: { uri, count, "Total", totalCount }
     * where uri and "Total" are of type QString and count and totalCount are of type double.
     */
    QVariantList activityCount(const QString &subjectUri);

    /**
     * \brief Count how many times uris in the database have been accessed
     *
     * This is a convenince method which invokes countAll(mode) with a default mode.
     *
     * \return A QVariantList containing the following: { uri1, count1, ..., uriN, countN, "Total", totalCount }
     * where uriX and "Total" are of type QString and countX and totalCount are of type double.
     */
    QVariantList activityCounts();

    /**
     * \brief Call this method to ensure that first relevance calc has been done before you continue
     *
     * If first calc has not been done starts it.
     * No reply needed, this is used as async.
     */
    void forceFullCalculation();

        /**
     * \brief Dbus method to search for recrods with the given text
     * 
     * \param string to search for
     * \param content types to look for
     * \param content sources to look for
     * \param Grouping required, by default true
     * 
     */
    QVector<QVariantMap> doSearch (const QString &text, const QStringList &content_types, const QStringList &content_sources, bool grouping_required) ;

    /**
     * \brief Dbus method to search for given uris
     *
     * \param list of uris to search for
     * \param content types to look for
     * \param content sources to look for
     */
    QVector<QVariantMap> doSearchItems (const QStringList &uris, const QStringList &content_types, const QStringList &content_sources);
     /**
     * \brief Dbus method to search for for groups
     *
     * \param list of group identifier to search for
     * \param content types to look for
     * \param content sources to look for
     * \param Grouping required, by default true
     */
    QVector<QVariantMap> doSearch (const QString &group_identifier, const QString &text,const QStringList &content_types, 
                                   const QStringList &content_sources, bool grouping_required);  
    /**
     * Query available actions for items
     *
     * @param identifier item identifier
     **/
    QVector<QStringList> actionsForItem (const QString& identifier);
   
    /**
     * Query available actions for items
     *
     * @param identifiers item identifiers
     **/
    QVector<QStringList> actionsForItems (const QStringList& identifiers);
   
    /**
     * Query available actions for items
     *
     * @param mimetype mime type identifier
     **/
    QVector<QStringList> actionsForMime (const QString& mimetype);
   
    /**
     * Execute action for a item
     *
     * @param itemIdentifier item
     * @param actionIdentifier action
     **/ 
    bool executeItem (const QString &itemIdentifier, const QString& actionIdentifier);

    /**
     * Execute action for items
     *
     * @param itemIdentifier item
     * @param actionIdentifier action
     **/
    bool executeItems (const QStringList &itemIdentifiers, const QString& actionIdentifier);

    void triggerRelevanceCalculation(QStringList &uriList);

#ifdef qtestlib
signals:
    void calculatingRelevance(QStringList uriList);
    void calculatingFullRelevance();
    void addingActivity(QString subject);
#endif

private slots:    
    //void activityAdded(QString subject);

    void timeForFullCalc();
    void timeForIdleCheck();
    void processQueues();
    void initializeHarvesters();

private:
#ifdef qtestlib
    friend class Ut_RelevanceDaemon;
#endif
    void calculateRelevance();
    void calculateIncrementalRelevance(QStringList uriList);
    bool getUptime(long& upTime, long& idleTime);
    void resetTimer();
    bool ensureStorageIsInitialized();


    //! Tracker listening registrations we were unable to complete in initialization
    QStringList m_pendingRegistrations;
    //! How many times we will try to register signalers
    int m_pendingRetries;
    //! DBus reference to relevance storage
    RelevanceStorageService* m_relevanceStorage;
    //! Saved uptime - used to calculate average idleness
    long m_lastUpTime;
    //! Saved idleTime - used to calculate average idleness
    long m_lastIdleTime;

    /* Configuration */
    //! Database filename (full path)
    QString m_dbFile;
    //! Harvester
    QSet<AbstractHarvester*> m_harvesters;
    //! Maximum time full relevance calculation may take (milliseconds)
    int m_timeoutFull;
    //! Maximum time incremental relevance calculation may take (milliseconds)
    int m_timeoutIncremental;
    //! Time before we do first calc (i.e. time after program start, presumable done at boot time) (5min)
    int m_timeFirstFullCalc;
    //! Time between calcs after the first calc (24h)
    int m_timeBetweenCalcs;
    //! Time used for sampling idleness information (i.e. between the two checks to /proc/uptime) (10s)
    int m_timeIdleSample;
    //! Time between idleness checks (assuming that battery is charging or full) (10 min)
    int m_timeWaitForIdle;
    //! Time between idleness checks when waiting for first calc (10s)
    int m_timeFirstWaitForIdle;
    //! Idleness limit percentage for performing relevance calc (1-99)
    int m_idleLimit;
    //! Time to wait before retrying tracker registrations
    int m_registrationRetryDelay;
    //! How many times we retry the registration
    int m_registrationRetries;
    //! Queued activities
    QSet<QString> m_activityQueue;
    //! Timer for handling queues
    QTimer* m_queueTimer;
    //! Time when we got last update from tracker
    QDateTime m_lastUpdate;

    //! True if first calc has been done
    bool m_haveFirstCalc;
    //! True until first full calc timer event has been launched
    bool m_firstTimeForFullCalc;
    //! True if we have initialized tracker successfully
    bool m_initialized;

};

#endif // RELEVANCEDAEMON_H
