/*
 * Copyright (c) 2010, Nokia Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Nokia Corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "ActivityPersonalization.h"

#include <cmath>

#include "RelevanceGraph.h"

ActivityPersonalization::ActivityPersonalization()
    : m_ageHalflife(60 * 60 * 24 * 2) // two days
{
    // do nothing
}

ActivityPersonalization::~ActivityPersonalization()
{
    // do nothing
}

double ActivityPersonalization::normalize(RelevanceGraph *graph, double normalizeTotal)
{
    double sum=0;
    for(int i=0; i<graph->nodeCount(); i++) {
        sum += graph->personalization(i);
    }
    
    if (sum > 0) {
        for (int i=0; i<graph->nodeCount(); i++) {
            graph->setPersonalization(i, graph->personalization(i) * normalizeTotal / sum);
        }
    }
    return sum;
}

double ActivityPersonalization::personalize(RelevanceGraph *graph, double normalizeTotal, unsigned int referenceTime)
{
    double sumAges = 0.0;
    for (int i=0; i<graph->nodeCount(); i++) {
        uint time = graph->modificationTime(i);
        uint age = (time > referenceTime) ? 0 : referenceTime - time;
        
        double aging = agingFunc(age);
        sumAges += aging;
    }
    
    for (int i=0; i<graph->nodeCount(); i++) {
        uint time = graph->modificationTime(i);
        uint age = (time > referenceTime) ? 0 : referenceTime - time;
        double aging = agingFunc(age);
        double addToPersonalization = aging / sumAges * normalizeTotal;
        
        if (graph->personalization(i) < 0) 
            graph->setPersonalization(i, 0);
        else
            graph->setPersonalization(i, graph->personalization(i) + addToPersonalization);
    }
    
    return sumAges;
}

double ActivityPersonalization::agingFunc(unsigned int ageInSecs)
{
    return pow(2.0, -((double)ageInSecs)/ ((double)(m_ageHalflife)));
}
