#include <QtTest/QtTest>

class RelevanceStorageServiceTest : public QObject
{
    Q_OBJECT
    private slots:
        void constructorAndDestructorTestCase();
        void storageDataUpdatedTestCase();
};
