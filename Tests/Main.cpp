
#include <QtTest/QtTest>

#include "Daemon/RelevanceStorageServiceTest.h"
#include "Harvesters/HarvesterTest.h"
#include "Client/ContentSearchEngineTest.h"

int main(int argc, char** argv)
{
    RelevanceStorageServiceTest test1;
    QTest::qExec(&test1, argc, argv);

    HarvesterTest test2;
    QTest::qExec(&test2, argc, argv);

    ContentSearchEngineTest test3;
    QTest::qExec(&test3, argc, argv);
}

