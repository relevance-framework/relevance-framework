#include <QFile>
#include <QPluginLoader>

#include "HarvesterTest.h"

#include "AbstractHarvester.h"
#include "AbstractHarvesterFactory.h"

QMap<const char*,const char*> harvesters;

void HarvesterTest::initTestCase()
{
    // TODO: We should check here which harvesters are relevant for 
    // current platform and configuration
    harvesters["SimpleHarvester"] = "Harvesters/SimpleHarvester/libsimpleharvester.so";
    harvesters["TrackerHarvester"] = "Harvesters/TrackerHarvester/libtrackerharvester.so";
}

/**
 * This test case will test instantiation of AbstractHarvester plugins.
 **/
void HarvesterTest::pluginCreationTestCase()
{
    QFETCH(QString, filename);
    if(!QFile::exists( filename ))
        QSKIP("File does not exist and is most likely disabled by build configuration.", 
                SkipSingle);
    QPluginLoader loader( filename );
    AbstractHarvesterFactory *harvesterFactory = 
        qobject_cast< AbstractHarvesterFactory* >( loader.instance() );
    QVERIFY(harvesterFactory);
    AbstractHarvester* harvester = 
        harvesterFactory->createHarvester();
    QVERIFY(harvester);
    delete harvester;
    delete harvesterFactory;
}

void HarvesterTest::pluginCreationTestCase_data()
{
    QTest::addColumn<QString>("filename");
    QMapIterator<const char*, const char*> harvester( harvesters );
    while( harvester.hasNext() ) {
        harvester.next();
        QTest::newRow( harvester.key() ) << harvester.value();
    }
}

