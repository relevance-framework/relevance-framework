CONFIG += qtestlib
TEMPLATE = app
TARGET    = relevance-tests
DEPENDPATH += .

INCLUDEPATH += ../Daemon
 
QT += sql
#QMAKE_LIBDIR += ../Daemon
#LIBS += -lrelevancedaemon

QMAKE_LIBDIR += ../Client
LIBS += -lrelevanceclient

SOURCES += \
    Main.cpp \
    Client/ContentSearchEngineTest.cpp \
    Daemon/RelevanceStorageServiceTest.cpp \
    Harvesters/HarvesterTest.cpp 


HEADERS += \
    Client/ContentSearchEngineTest.h \
    Daemon/RelevanceStorageServiceTest.h \
    Harvesters/HarvesterTest.h

